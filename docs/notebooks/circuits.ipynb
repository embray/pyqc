{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `Circuit` class provided by PyQC provides several alternative means of defining quantum circuits which can be simulated (on a small number of qubits) without building up algebraic expressions of operators.  Circuits are a straightforward way to program quantum algorithms, and as instances of the `Circuit` class are also `Gate` objects, they can be used to create new gates for use as subroutines in more complex circuits.  Another feature of `Circuit` objects is that they can be rendered as circuit diagrams.\n",
    "\n",
    "The `Circuit` class can also be used to read in circuits from quantum assembly languages.  Currently only two languages are supported:  The pseudo-assembly language used by [qasm2circ](http://www.media.mit.edu/quanta/qasm2circ/) by Isaac Chuang, as it is also (currently) used to generate PyQC's circuit diagrams.  Within certain limitations this turns out to be an okay language for providing instructions to a circuit.  The other is the simple language used by Scott Aaronson's `chp` program, as part of the experimental support for stabilizer circuits.  There is also support for an extended version of this language that includes instructions for the Pauli operators.  At least partial support for QCL and Quipper's programming language are also planned once a bit more of the necessary machinery is in place.\n",
    "\n",
    "As explained in the introduction to this documentation, the primary format for creating a circuit is a list of instructions, where each instruction is a tuple consisting of an operator and one or more arguments.  Here we can use the gate objects themselves, `H`, `CNOT`, `X`, and so on, as glyphs representing that gate in the instruction list.  For single-qubit gates, the argument is simply an integer specifying which qubit to apply the operator to (numbered from 0 to n - 1).  For multi-qubit gates, including control gates, the arguments are given as a tuple of arguments).\n",
    "\n",
    "A circuit, for example, consisting of a single Hadamard gate is defined like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAFsAAAAcBAMAAAD8Rd5+AAAAMFBMVEX///+qqqoyMjK6urrc3NwQEBBUVFQiIiLu7u52dnbMzMyYmJiIiIhERERmZmYAAAAIOT9MAAAAAXRSTlMAQObYZgAAAAlwSFlzAAAOxAAADsQBlSsOGwAAASJJREFUOMtjYMAO+P8jgw8MBAA/Cm/QKz9AjPIHcFYGMcovwFk2pClnIk05pwG68s74bI7p3w5gV86SgGF6LdBOBXTTz+yagaYTqpzjNwND/wQ05bwH+ArAzN3oyhm/MDDIG6Ap387AluC504GBgRddOV8AA8N7dK9GMrBu0GBQgCiHp0SQHHvlzJmxIAYiZV5g+czg71XAoMzA4Iduev8GBo4/aKZz/WDYyq3AcJuBYRa6cnkHBsZf6I6J4AjjucBwB0vIPAU6/wO6crvdMUDTdRgYN6ArLwM6/wJGrHL84ipgKGZga0BTzgIK9gMYypk/MFQwFDC0oaUZjud/D/Tpa2Io57zAsHLnAobDxCUxjqeRGGmAiOzRMIRLAlSAvdADAJc9jVXqGSNyAAAAAElFTkSuQmCC\n",
      "text/plain": [
       "C_0 = [[ 0.70710678  0.70710678]\n",
       "       [ 0.70710678 -0.70710678]]"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from pyqc import *\n",
    "\n",
    "Circuit([(H, 0)])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Naturally, this is exactly equivalent to just using the H gate directly:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "H == Circuit([(H, 0)])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`Circuit` instances are themselves gates, and share all the same semantics as the `Gate` and `Operator` classes.  For example, we can provide input to a circuit by acting on a `Ket` object with it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left|1\\right>$"
      ],
      "text/plain": [
       "|1>"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Circuit([(X, 0)])|Ket(0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Same applies for circuits of more than one qubit:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAEEAAAA5BAMAAACbqFvaAAAAMFBMVEX///+qqqoyMjK6urrc3NwQEBBUVFQiIiLu7u52dnbMzMyYmJiIiIhERERmZmYAAAAIOT9MAAAAAXRSTlMAQObYZgAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAXZJREFUOMvt1b9Lw0AYx+FvTC3XX8bVrdDFQWj+gKJx1EEXEQsOHTtGdHDM1EUHR+nkKhR6i0itw0kdHTK46eDiHhBEupxJKrm70EtGlx4djvKQ5j2ST4FkMcxfH8muqxF+smvliqVcUbLzRKEjvjWbJ7J4ebhOYWxxvipEja248XYsRJNzV4h7FDs7IweoCcE5D4RoY5muoz4TFk+tAH7hC/u7LhrAnuY+yj+4q9TxBvTlWRrSLEfksOrjXZkFlnwem+Pj8BobMKhOgEzLLk5R9LTCDHAOFz1oRcnH7egGE60gn+30oStCPEGeRmjWQizEvwiW+yR39e/LvBYOn7zsFg6Gz4+ZLTRsi5GrrBZewGKoZrWQRcJwMlpII0Go3EK8Ql+6uIVrB5BrSaMfIn+ti1vo4AxyC1n0qXhSCxGLvjRLKC7lFs6EmMW0IZ9H2MJYyC0c2MqZkmkslBZuTzy1hQhn6en/o8IWtr6p2kLkthC5LRTrFz/7otIf+cpIAAAAAElFTkSuQmCC\n",
      "text/plain": [
       "C_3 = [[1. 0. 0. 0.]\n",
       "       [0. 1. 0. 0.]\n",
       "       [0. 0. 0. 1.]\n",
       "       [0. 0. 1. 0.]]"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Circuit([(CNOT, (0, 1))])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left|11\\right>$"
      ],
      "text/plain": [
       "|1, 1>"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Circuit([(CNOT, (0, 1))])|Ket(1, 0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is a more complete example, implementing a full circuit for a 3-qubit QFT.  To implement this circuit we first also defined a controlled phase gate and a controlled T-gate:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "C_S = ControlledGate(S)\n",
    "C_T = ControlledGate(T)\n",
    "\n",
    "QFT_3 = Circuit([\n",
    "    (H, 0),\n",
    "    (C_S, (1, 0)),\n",
    "    (C_T, (2, 0)),\n",
    "    (H, 1),\n",
    "    (C_S, (2, 1)),\n",
    "    (H, 2),\n",
    "    (SWAP, (0, 2))\n",
    "])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAATcAAABoBAMAAABmo6qXAAAAMFBMVEX///+qqqoyMjK6urrc3NwQEBBUVFQiIiLu7u52dnbMzMyYmJiIiIhERERmZmYAAAAIOT9MAAAAAXRSTlMAQObYZgAAAAlwSFlzAAAOxAAADsQBlSsOGwAABJ9JREFUaN7t201oE0EUB/C3zZpOU7vJKeJBLPZSQaxXQbQ3UcQPkFJBMReloGBE8eMgyUGEIrS9+BH1EA8eFME9KH4UdUHwYg+LeFOkCEUliltbirbaOLvV7O7LpN3NTuyg8y6mZWx+vs7sTP67ArArWfaWxfguayD8nUr6vrKqv5tkDExKnMSFKkM83GjlVZ94OLPyaoPIuCaRcS3r8Pv07z9ELk0ZCKdce7HNj3v6dmz2876G4tRMVRPO0H62484dgsQuP64PmjOwhQ9O1ek/v+jiRh5dRs7f70NmAAaHEK6VDuv04YgOgzqc59S5EkDa7VyboWWdl8MYp0wCrFyHcHHa4CEfTqGjuuEWJ5ymKxkXd5++35aH3QBtGKftBPiCF0R8nLEgDnBcEKW0Z87tgSV6J7TP4ZLeA0bz6UJhn/3Ce+xQc18NPBB6GEeZyGWBqU7Ajq1Z6ADYjjtHZxL5UXUpiV2axZ1TZzh2Lj3jdi7xDe61tsNrgCsYR2eSMo1x9Pc/hnGtk/xwSmZWd3+tvaRnqQlvGKuVIjQL4RIpgGUYF9/JD5cGq+TiNg7vpZ1bA4qOcSfotDMRbimdcJ8wbskoN5w6Cpbmuc6R6UQWjkE8j3D2TBo0EK6tExQT4+6keG5flneHiFlwCrJwDu2t5P1PY6BrNcJpA4Wr+FJyMbc21Shciwk3HxbhmSgbvxdHxvZU7V6C4NzDZl48nICfISSuYThYKI5gJRTW38KF+8tcD+gSJ3ESJ3ES98/i2FF84Ni9wTjmcCtooyXuv8EZ4uHcDzh94uFqpOmi4ZpExvnS9Mg4cmFK788dxrjlI3eNenC+NL12xbrGA3XOzqdW4c7FdbiRCoVjpem1a1O5nAqCs2PR7xj3mM6efBgcM02vXV3lcjYIzo5FJzDuIJ09oeacnabDK0CBdaSyf3AlFvUeAI6kGCeDeQJrJ01fvhv8UX/0zp0sFC6YuHOby2tCrVY7Te+G4+BP0yPPOVIVizp/rs+NhsHZaTo4uCsBV2tHkNXq3P0pIpw6d+MlxJzrJT1zOBPqrmpc5e6PF3edorOhcBuH9zo4X5oeGdeccW46+nEjFG2E2yHItIPzpemRcTv+3P3x4l7mYUXI7StGv6Kr9RxwxA3k3pB3X4cQ7sPHgh4S12LChindn6ZH7hynjZ+ZpouBcw+befFwfEri/gfcQnGEtYi4iAMlTuIkTuIkTuLY+5wouIVOCBL3b+MM8XDuB5w+8XB1pOmLgWtaHFyNZBvhAqfpnDvnT7ZVHSzv49+/K2CazqpY19G6BrKS7RJYnse/Q6bprGKmnQEGspJtTR/PuLiQaTqrmDlxgIHMB35LU545Z6fpt58XeabpoWL3SrJdI01/YCR6g6bpvDtXlWynS27nnGfTLZgImqbznnM42VYymvv4t5OmF8lk0DSdvQg76hrISrbTzv/X+DPnnDRdG4qWHtZ/eUTJtkr3U8/j306a/iRamh4BV51sg3eHINMQM0ikNL1+HCPZ9uFiFtw+ezdSms5l12PiWkxiX1SeiYjjkaY3COceNvPi4Ra1JI4LLikarjxvxP4LR571aZWgP9UAAAAASUVORK5CYII=\n",
      "text/plain": [
       "C_5 = [[ 0.35355339+0.j          0.35355339+0.j\n",
       "         0.35355339+0.j          0.35355339+0.j\n",
       "         0.35355339+0.j          0.35355339+0.j\n",
       "         0.35355339+0.j          0.35355339+0.j        ]\n",
       "       [ 0.35355339+0.j          0.25      +0.25j\n",
       "         0.        +0.35355339j -0.25      +0.25j\n",
       "        -0.35355339+0.j         -0.25      -0.25j\n",
       "         0.        -0.35355339j  0.25      -0.25j      ]\n",
       "       [ 0.35355339+0.j          0.        +0.35355339j\n",
       "        -0.35355339+0.j          0.        -0.35355339j\n",
       "         0.35355339+0.j          0.        +0.35355339j\n",
       "        -0.35355339+0.j          0.        -0.35355339j]\n",
       "       [ 0.35355339+0.j         -0.25      +0.25j\n",
       "         0.        -0.35355339j  0.25      +0.25j\n",
       "        -0.35355339+0.j          0.25      -0.25j\n",
       "         0.        +0.35355339j -0.25      -0.25j      ]\n",
       "       [ 0.35355339+0.j         -0.35355339+0.j\n",
       "         0.35355339+0.j         -0.35355339+0.j\n",
       "         0.35355339+0.j         -0.35355339+0.j\n",
       "         0.35355339+0.j         -0.35355339+0.j        ]\n",
       "       [ 0.35355339+0.j         -0.25      -0.25j\n",
       "         0.        +0.35355339j  0.25      -0.25j\n",
       "        -0.35355339+0.j          0.25      +0.25j\n",
       "         0.        -0.35355339j -0.25      +0.25j      ]\n",
       "       [ 0.35355339+0.j          0.        -0.35355339j\n",
       "        -0.35355339+0.j          0.        +0.35355339j\n",
       "         0.35355339+0.j          0.        -0.35355339j\n",
       "        -0.35355339+0.j          0.        +0.35355339j]\n",
       "       [ 0.35355339+0.j          0.25      -0.25j\n",
       "         0.        -0.35355339j -0.25      -0.25j\n",
       "        -0.35355339+0.j         -0.25      +0.25j\n",
       "         0.        +0.35355339j  0.25      +0.25j      ]]"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "QFT_3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "\\begin{equation}\\frac{1}{\\sqrt{8}}\\left(\\left|000\\right> + \\left|001\\right> + \\left|010\\right> + \\left|011\\right> + \\left|100\\right> + \\left|101\\right> + \\left|110\\right> + \\left|111\\right>\\right)\\end{equation}"
      ],
      "text/plain": [
       "sqrt(1/8)(|0, 0, 0> + |0, 0, 1> + |0, 1, 0> + |0, 1, 1> + |1, 0, 0> + |1, 0, 1> + |1, 1, 0> + |1, 1, 1>)"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "QFT_3|Ket(0, 0, 0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## To/From QASM\n",
    "\n",
    "This circuit can also be dumped to the [qasm2circ](http://www.media.mit.edu/quanta/qasm2circ/) format as a string:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\tdef\tc-S,1,'S'\n",
      "\tdef\tc-T,1,'T'\n",
      "\n",
      "\tqubit\tq0\n",
      "\tqubit\tq1\n",
      "\tqubit\tq2\n",
      "\n",
      "\tH\tq0\n",
      "\tc-S\tq1,q0\n",
      "\tc-T\tq2,q0\n",
      "\tnop\tq1\n",
      "\tH\tq1\n",
      "\tc-S\tq2,q1\n",
      "\tH\tq2\n",
      "\tswap\tq0,q2\n"
     ]
    }
   ],
   "source": [
    "print(QFT_3.to_qasm())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These QASM scripts can also be read in to pyqc using the `Circuit.from_string` classmethod.  This does not yet work perfectly for all cases, but for many common cases it does.  In particular, QASM output by `Circuit.to_qasm` will always round-trip:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "QFT_3_from_qasm = Circuit.from_qasm(QFT_3.to_qasm())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The resultant object is a fully capable PyQC `Circuit`.  Its circuit diagram can be displayed, and it can be evaluated on input states:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAT4AAABoBAMAAACatoFdAAAAMFBMVEX///+qqqoyMjK6urrc3NwQEBBUVFQiIiLu7u52dnbMzMyYmJiIiIhERERmZmYAAAAIOT9MAAAAAXRSTlMAQObYZgAAAAlwSFlzAAAOxAAADsQBlSsOGwAABIlJREFUeNrt3E9IFFEcB/DfuGrjqjN7MvKS4MUgUjpLebWiDEIUlPYSCEJtFFGHyEMEEainyurgtRCaoCgTckDoUoc9dCvCiwRZNJZU2J/pzZgz7/32rfvWfes+9P1A2F3H3Y9v3ps/X38IwC/bp8vjvMrbEDatbOaZl/uqzdnQ1j7t23C5Svrmo0dDSvqy0aNOxX1VivvqOvBHXT85bN767iKfce/VEdY3+37h7+fBcvuq0zlDcZmMagsev2FIHmd9Q7AjDd3yfIfJV3Pse/38NqL+/yjzF8DYOPLVk83aGJ/pwJgDN+T5LAeMdORrdK1M+HAG+4xlgN0dyFdLfnKc8Rlkqy54IHH/LkJTvH+fko/snu4CaMQ+qwfgC14ftUuc9XFK7vqwHqVjXz/UOG3Qsuqz6UuRHZcmJgaDB/QFSvWVry7eEHo5Fz1SCrLV3+DYoQy0AhzF40dmlfk75/iSuPUXj1/1L7njZ5xxovFL/oQn9S3wFuAO9pFZZaxgH5kIC9hXvyzX12Qvxvu3z+xtyMI7zvolDstDvmQKYCf21fZI9Rlp23Ii34GZATJ+e8FwsO8CmYJZ5Gsgk+8T9tXMS/UdJe/0MT7+mSvJDJyD2hHkC2bVmIt8jW1gZLHvYUry+c2mzx8JDy5CBq6hDcwPf9zR9j3IZ41O3MXHl5tX9qXK6KvLwv3pSZhT6PqA9pkL/TmnN3V88fXpiJI+Ne8/tK/MPiiUb/AiD28TfeuUJ/n9tE/7tE/7tE/7+OdHXthfYZ/H+7YtKtC+7edzlfTF90dDSvry5PcK+qoU9zH5vRRflP8zPjb/F/cx+X3+SrQvCY/fWv7P+Nj8X8DHy+/z10HfTwn6ovyf9qH8v7CPm9/nr3bfzwj6ovyf9qH8v7AvyO/hDaB8vNQKfFH+T4fpa/m/aD4e5ve7TgD79wUp4xfl//T4reX/ouMX5PddcB7Y/F7K/Ivyf2Z9sPl/QV+Q30PouyO4fltF12+U/9O+Ljb/Lzz/+szeVV8WNlx8X5T/Uz6c/xf2HZgZCH1Mfi/FF+X/lA/n/wLHP3Ml9DH5vQxfnP9TPpz/C/gS5L3I+r0Gcn1U/k/5cP4v4KvLQud3h83vpexfOdcH3PxeGV98fTqipK9c99Pat7V9ovl/pXzF3R9on/Zpn/Zpn/YV6eOfCNXxebwn2rf9fK6Svvj+aEhJ3wby+wr5qirn4zfSI59wfl+G8WMb6Q+T92vO8Qnm97xKtJ8V33j/jxT2oUZ6y7Gp/vYi83tecTPVfL+L7y9hH26kX7Sp/vYi83tecTPpPFXl+z+wDzfSW7Pp2Bfk91MvJyXn90UF/bmN9Gx+/8xN9onm9+UYP9xIb5yO+9vD/nsPvonm9+WYf7iRvgni/vYwv580l0Xze/76bS1i/X7twD7USE8WL9XfHub31nhpAeXGiwnSV33Bjoz728P8/kVp+X1pvtxGeqDPH+YKJFyzpPy+JB+nkZ7xJTyYuvq4pPy+xP27zq1TmN+bwWFoTlGfjPy+fL74+nRESV/FS/u2ts9f9x8U/AMwXMux8kxOyQAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "C_6 = [[ 0.35355339+0.j          0.35355339+0.j\n",
       "         0.35355339+0.j          0.35355339+0.j\n",
       "         0.35355339+0.j          0.35355339+0.j\n",
       "         0.35355339+0.j          0.35355339+0.j        ]\n",
       "       [ 0.35355339+0.j          0.25      +0.25j\n",
       "         0.        +0.35355339j -0.25      +0.25j\n",
       "        -0.35355339+0.j         -0.25      -0.25j\n",
       "         0.        -0.35355339j  0.25      -0.25j      ]\n",
       "       [ 0.35355339+0.j          0.        +0.35355339j\n",
       "        -0.35355339+0.j          0.        -0.35355339j\n",
       "         0.35355339+0.j          0.        +0.35355339j\n",
       "        -0.35355339+0.j          0.        -0.35355339j]\n",
       "       [ 0.35355339+0.j         -0.25      +0.25j\n",
       "         0.        -0.35355339j  0.25      +0.25j\n",
       "        -0.35355339+0.j          0.25      -0.25j\n",
       "         0.        +0.35355339j -0.25      -0.25j      ]\n",
       "       [ 0.35355339+0.j         -0.35355339+0.j\n",
       "         0.35355339+0.j         -0.35355339+0.j\n",
       "         0.35355339+0.j         -0.35355339+0.j\n",
       "         0.35355339+0.j         -0.35355339+0.j        ]\n",
       "       [ 0.35355339+0.j         -0.25      -0.25j\n",
       "         0.        +0.35355339j  0.25      -0.25j\n",
       "        -0.35355339+0.j          0.25      +0.25j\n",
       "         0.        -0.35355339j -0.25      +0.25j      ]\n",
       "       [ 0.35355339+0.j          0.        -0.35355339j\n",
       "        -0.35355339+0.j          0.        +0.35355339j\n",
       "         0.35355339+0.j          0.        -0.35355339j\n",
       "        -0.35355339+0.j          0.        +0.35355339j]\n",
       "       [ 0.35355339+0.j          0.25      -0.25j\n",
       "         0.        -0.35355339j -0.25      -0.25j\n",
       "        -0.35355339+0.j         -0.25      +0.25j\n",
       "         0.        +0.35355339j  0.25      +0.25j      ]]"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "QFT_3_from_qasm"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "\\begin{equation}\\frac{1}{\\sqrt{8}}\\left(\\left|000\\right> + \\left|001\\right> + \\left|010\\right> + \\left|011\\right> + \\left|100\\right> + \\left|101\\right> + \\left|110\\right> + \\left|111\\right>\\right)\\end{equation}"
      ],
      "text/plain": [
       "sqrt(1/8)(|0, 0, 0> + |0, 0, 1> + |0, 1, 0> + |0, 1, 1> + |1, 0, 0> + |1, 0, 1> + |1, 1, 0> + |1, 1, 1>)"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "QFT_3_from_qasm|Ket(0, 0, 0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, for an arbitrary QASM input, the resultant `Circuit` object will not always be executable.  Take for example the following sample QASM script, with two inputs, that places the inputs in a superposition and then applies a two-qubit gate named \"U\":"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "/home/embray/src/pyqc/pyqc/circuit.py:294: UserWarning: Input QASM defines the following gates unrecognized by pyqc:\n",
      "\n",
      "    U_0\n",
      "\n",
      "Until operations are defined for these gates the circuit cannot be evaluated.  You can register these gates using the register=True argument when creating a new gate object.  For example:\n",
      "\n",
      "    >>> G = Gate(<..matrix elements..>, name=U_0,\n",
      "    ...          register=True)\n",
      "\n",
      "The diagram for this circuit can still be displayed, but the circuit cannot be evaluated until the unknown gates are registered with pyqc.\n",
      "  f\"Input QASM defines the following gates unrecognized by \"\n"
     ]
    }
   ],
   "source": [
    "sample_qasm = \"\"\"\n",
    " defbox    U_0,2,0,'U_0'\n",
    " \n",
    " qubit     q0\n",
    " qubit     q1\n",
    " \n",
    " H         q0\n",
    " H         q1\n",
    " U_0       q0,q1\n",
    " H         q0\n",
    " H         q1\n",
    "\"\"\"\n",
    "\n",
    "C = Circuit.from_qasm(sample_qasm)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As the warning indicates, the `Circuit` instantiation is not quite sure what to do with the custom gate `U_0` defined in this QASM script.  There are no gates of that name known to PyQC.  We can still display this circuit diagram just as though in the input were passed to qasm2circ:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAALUAAABCBAMAAADj6oo7AAAAMFBMVEX///+qqqoyMjK6urrc3NwQEBBUVFQiIiLu7u52dnbMzMyYmJiIiIhERERmZmYAAAAIOT9MAAAAAXRSTlMAQObYZgAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAlZJREFUWMPtmD9II0EUxr+sMdnsmcTWKoFrTjwxIFjJmVYLtTgOBQ8tBZuAIloIqQQbtRC8xMJ0YrcIItFmwfYOtrjurrCRK104CeIf4sxE1jgzkVF3CmEHFmaX7/327ezsm/kWkLd0vbl50qv1+rPili395MyTXvVP5eKQ/TzbCZx95vdmA2e7fm9QI9vQyE7k+LC16Tlzu+aosQVxMzs6I4StkKfJqubNixn75/EP7jYPcvMG2NhUZAtiCkw6qQLrnvBhkUsgk1NkC2LKPkJsZriaB5J8WGocuFB9l4KYsifRbn9CtsFON5e4+HKp9J12+MLH1z+p2IMb/Y+xkQI+AqN8Shs2zFvVvAUxydu6wuGHLP4AZT4sk0fkWpUtiOmYTJjfOlz8lcyTczKKnipbEFPgl5MpkvdnRGw+bJGMoqvKFsQsWfPaKmAesSIXFqUz1lFki2LGbvOwhAJWuTDz352z3tetxpaIGTvhYr9awamGWmWeTwqffNBrQzFci1/JbruodVp9PW9hQ75nowep0TGO+bI9W+unGSI1ul86RG9rFLZLjk1N7N7HiRs0m9ZoU1PetEZHKtGdLQ1suiymYBQNDewkKTtr2IORC54dIzukLRwgaQfPtiaQqOAX2dVomIMDJbILO4Dxena65TfP2h7iTeMdrE+LF+Paaqy1Uw59WujTQp/2jn0afkPipXzrpeKlBLH34NO6vkLiAX3rpZK3IEbDp+WxAF0+DYytx6c12Hp8GmNr8mmMrcungcwTXT5tsGaHPi38HxuYT5P/W2/h0+4B68APoBjArzIAAAAASUVORK5CYII=\n",
      "text/plain": [
       "C_7"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "C"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But any attempt to evaluate the circuit will fail with the previous admonition:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The gate 'U_0' is currently a placeholder for an undefined gate.  In order for this circuit to be evaluated a `Gate` instance with the name 'U_0' must be created and registered in the global gate registry.  For example:\n",
      "\n",
      "    >>> custom_gate = Gate([[...]], name='U_0', register=True)\n"
     ]
    }
   ],
   "source": [
    "try:\n",
    "    C|Ket(0, 0)\n",
    "except Exception as e:\n",
    "    print(e)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to make this `Circuit` useful we need to define a new `Gate` with the name `'U_0'` and add it to a *registry* of known gates.  This is done by passing the `register=True` argument to the `Gate` constructor--currently, gates are not automatically added to the registry (though all the gates built in to PyQC are registered).  For example, we could define:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [],
   "source": [
    "U_0 = Gate(Z * Z, name='U_0', register=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The act of registering this gate with the name `'U_0'` immediately makes the previously defined circuit usable:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left|11\\right>$"
      ],
      "text/plain": [
       "|1, 1>"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "C|Ket(0, 0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(Incidentally, the resultant circuit is equivalent to an $ X $ gate on both qubits:)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "C == Circuit([(X, 0), (X, 1)])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that now that `'U_0'` is registered we are prevented from accidentally re-registering it and possibly overriding the previous definition:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "An Operator with the name 'U_0' has already been defined. Use the replace=True argument to override the existing Operator.  The existing Operator will continue to work in existing circuits but may not be usable in new ones.\n"
     ]
    }
   ],
   "source": [
    "try:\n",
    "    U_0 = Gate(Y * Y, name='U_0', register=True)\n",
    "except Exception as e:\n",
    "    print(e)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That is, unless we force it with the `replace=True` argument:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "/home/iguananaut/src/pyqc/pyqc/operator.py:113: UserWarning: Replacing existing Operator 'U_0'.\n",
      "  \"Replacing existing Operator {0!r}.\".format(name))\n"
     ]
    }
   ],
   "source": [
    "U_0 = Gate(Y * Y, name='U_0', register=True, replace=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that this does *not* change the functionality of the previous circuit that used the `U_0` gate.  It still has a reference to the old gate.  This prevents the circuit's functionality from changing in place.  Still, this is useful, as the same circuit can be used like a template for new circuits, just by swapping out the `U_0` gate.  If we create a new `Circuit` from the same QASM script it will use the new definition of `U_0`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [],
   "source": [
    "C2 = Circuit.from_qasm(sample_qasm)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$-\\left|11\\right>$"
      ],
      "text/plain": [
       "-|1, 1>"
      ]
     },
     "execution_count": 22,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "C2|Ket(0, 0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## From Wire Lists\n",
    "\n",
    "This alternative method for instantiating a `Circuit` was actually the original format designed for PyQC.  The idea was to be able to write out a circuit in valid Python syntax that follows a similar visual flow to that of a circuit *diagram*.\n",
    "\n",
    "In practice, this format turned out be cumbersome for anything but toy cases, but it is still included (and documented) for completeness.\n",
    "\n",
    "The idea is to provide a list of lists, where each list represents a single *wire* in a quantum circuit--that is, the list of instructions performed on each qubit.  These lists can be passed in to the `Circuit.from_wires` method to create a new `Circuit`.  For example, for a single wire:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAKoAAAAcBAMAAAADyznZAAAAMFBMVEX///+qqqoyMjK6urrc3NwQ\nEBAiIiLu7u52dnbMzMyYmJiIiIhmZmZERERUVFQAAADa50CWAAAAAXRSTlMAQObYZgAAAdJJREFU\nSMftlrFLAlEcx7+m+dQ0/Q88aihoyL0GCQKDwKChoKKDoJbShoaGSJdq9Q8IuqUwp4PGhiQwCA50\nruWIploOLcoC6zzt9P3uqiHdetO7H7/3ue/7/n737gH2I/jRPjS76I+Z31C5J80mGvwx859qDrmD\nVNUM3HeQWjIDY12h9nSF6onQNbHlB7Zfkwm1mFCKIk917dWk7eSjHdWlWpRs6DsQqNZZoBAlWt06\nZoDXen56QF7QzGXvQCpNqF4JnifqQEoGe+WoAdkvGtMcpTr01eEIofqAiwylhqNwVDjqCfrVWFbf\nU4BS/TO6i9Zq+SqWahX0ZI2jzqNXGoLQaIJg+4nkXleUpfqEP6fCIdDMVUUplFqZKLkqiE+JGASm\nqdaUZFhLtDrfLJ3F3gxr27R6X3DsE3ADXFNq3a2qhXqUxhahGgXIcA7MsaU+Abc2PUDdavTmIpAm\nVLMALep4bkHXOgKHRKmJRiPy1EkVLE+obtXoba5fWdUrYgWBTUJ1UbfqUaavnggRavyrAG1Up4Y1\niNgl5wC7Kss7o8OEGqgpd8+kWjvJEXZZTvNUTwmH2QzOOnq6sOK85YP9KzVvBqL//9juU/Hrjcj2\nltS8Jn0Cn4wUd4udxG4AAAAASUVORK5CYII=\n",
      "text/plain": [
       "<repr(<pyqc.circuit.Circuit at 0x3786490>) failed: TypeError: format_text() takes at most 2 arguments (3 given)>"
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Circuit.from_wires([H, Z, H])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Multi-qubit gates are more complicated in that they span multiple wires.  To support this, multi-qubit `Gate` bojects have labels representing each of their inputs.  For example `CNOT.target` and `CNOT.control` (or we can use `CNOT.t` and `CNOT.c` for short).  The `.target` and `.control` labels are also provided by default on custom `ControlledGate` objects.  Single qubit gates also have a label for their single input as well, but this is not generally useful.  For example we can implement the 3-qubit QFT from before as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAATIAAABoBAMAAACAimHTAAAAMFBMVEX///+qqqoyMjK6urrc3NwQ\nEBAiIiLu7u52dnbMzMyYmJiIiIhmZmZERERUVFQAAADa50CWAAAAAXRSTlMAQObYZgAABJdJREFU\naN7t209IFFEcB/DfuKOjNrqeqkur5MHAQO8FbZcwCvIQGIi1EdSlWg8dJES99AeK2GsUNIf+StGA\nt4JYAgs3wyE6VYc52MWgxj9QmrjNzI7Tzm/e4tudp77s/U7j+HQ//nxv3vqdEYBc8XxxWYSzpIGw\nARUPfGSFz8YJA+NCJmRrls6ZzPSPpjmTGf7RPm5lVdzKajvxi3SdmlGuruhIJt14cSQoe/5lfPl9\n3/rJZDP041+wO9mCezYDdbNB2TTUmNDFRiZr9s+e8WQvn1xDSO9FlN8AwyNIVm8PawvIFA2GNbjM\nqGc5gITHadDVlHs4imXSAkBzJ5I12q0dCcgk+/eZhNuMZKommZ7sof1iXY+TAA1YpnYDTOEV0Djb\nH14BXxmugFxidZ6dgGqtDVoKizNe/Lah5vzkZJ9zUPxmQk7P6XggnCS8QYlYYMjzcOxwCloBjuKe\n2bPHmWp4bV5dxj2Tlxj2LLHk9azuJzyob4FPABNY1pwEaZFwPRvHsvoFdjLJXNa832aP0retBT4T\n1qYtUC0kk5sAtmNZYzc7WQKsnCfbP9pr96wdJA3LLtpTzUCyenvUOyyrzjKTyVmw1E7vIqYs1qXg\nDDT0I5kze4Z1JGtoA8nAsntNLHcny98DYhacgxQMoX1TeTOnD3bsQTJ14HoOXzVupfc2rYus1oC7\njzMwxsuO7suUqROhzYkHWdY/keRMxuHfAULGWgZrpQekQMHaEFl5X8n0fbaQCZmQCZmQ/cMycjBO\nHYKvp4w43KJtsZBtaZnOmcz0T0xzJiuRbXMlq+JWFsi2I8vkKyvaQPoblu18eV8vWxbItktXLD1L\n1TMnRdqNe9aouekMrYyUbZeuA/l8E41sWAflF5Y9sydNP7WMmG2Xro58PkUjcwLLeSz7bk8a+nnm\nZNvwEQrZNpsQ2PnGfmBZvLP/2EHY8kslyG62vfM49eKk7dnZyclxA/fsUL6dfm062XYSHNlRlvNM\nCQWWhbWZzlLLnGwbXNkE3drsaKVZm+79l0z4eubc/aCdZz1KX0FmQMUVlvn3X4plB21xil62f7TX\nlQWy7ciyGtO91ReU2atf1cvYA5RFVxbItiPLjq3efymWfeiHXeXsTjHLlQ0BQ9lgul15PTeCZG+v\n39TKkdUasG9FC2bbkXvGYkcnZtscyLL+iSRnMjYlZFtYtlZ6YG2WLOJAIRMyIRMyIfsfZORtjAvZ\nWlu/kG09mc6ZzPQ/Mc2ZrIJse8NlVZsjKxE1V5RtM+5ZMGqWNbD8p6O9osy2SRVLn65oIClqzoG1\n+nR0mdk2qYg5JMVAUtSsarOmxykz2yYVMbulGEh8Nja3sjrPnGz7zqsMy2y7rBDcj5pJ2fYjva4n\nyuKM0rNQ1JzIeT1zn9u2YJ4222Y9z3DULJmqVpC52XZGWaDNtolLjpR3UwwkRc0J958XwM+21ZFo\n0V7ll0EUNctZG+M9He1m20+jZdsRZOGoGfw9QFmEmK5EyrYrlxGi5r+ymAV3Lj2JlG0z2dTCslpD\ndv48GONOxiLbXg9Z1j9Kcibb1BKyiLI4V7J86cD7D3Eb6nGknaclAAAAAElFTkSuQmCC\n",
      "text/plain": [
       "<repr(<pyqc.circuit.Circuit at 0x3786d90>) failed: TypeError: format_text() takes at most 2 arguments (3 given)>"
      ]
     },
     "execution_count": 24,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "CS = ControlledGate(S)\n",
    "CT = ControlledGate(T)\n",
    "\n",
    "Circuit.from_wires(\n",
    "    [H, CS.t, CT.t, I, I,    I, SWAP.q0],\n",
    "    [I, CS.c, I   , H, CS.t, I, I      ],\n",
    "    [I, I,    CT.c, I, CS.c, H, SWAP.q1]\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The resultant `Circuit` object is otherwise equivalent in every way to circuits defined from other formats.\n",
    "\n",
    "Note how this required manually inserting all the \"blanks\" on the wire as `I`.  This is just one of the problems that wound up making this format more difficult to work with than it was hoped.  Any ideas for improving it are, however, welcome."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
