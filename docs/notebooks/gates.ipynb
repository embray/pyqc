{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As alluded the in the section on the `Operator` class, the `Gate` class in PyQC is just a special kind of operator.  It has all the same functionality and semantics as the `Operator` class except for one difference--they must represent unitary operators that can be used as gates in quantum circuits.\n",
    "\n",
    "In fact, if you initialize an `Operator` with a matrix that happens to be unitary it is automatically \"promoted\" to a `Gate`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$\\hat{U} = \\left(\\begin{array}{*{11}c}0.0 & 1.0\\\\-1.0 & 0.0\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "U = [[ 0.  1.]\n",
       "     [-1.  0.]]"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from pyqc import *\n",
    "\n",
    "U = Operator([[0, 1],\n",
    "              [-1, 0]], name='U')\n",
    "\n",
    "U"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "pyqc.operator.Gate"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "type(U)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, one can be more explicit by using the `Gate` class directly:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$\\hat{U} = \\left(\\begin{array}{*{11}c}0.0 & 1.0\\\\-1.0 & 0.0\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "U = [[ 0.  1.]\n",
       "     [-1.  0.]]"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "U = Gate([[0, 1],\n",
    "          [-1, 0]], name='U')\n",
    "\n",
    "U"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Trying to instantiate a `Gate` with a non-unitary matrix will result in an error:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Input matrix for Gate must be a 2^n x 2^n unitary:\n",
      "Quantum object: dims = [[2], [2]], shape = (2, 2), type = oper, isherm = False\n",
      "Qobj data =\n",
      "[[1. 2.]\n",
      " [3. 4.]]\n"
     ]
    }
   ],
   "source": [
    "try:\n",
    "    Gate([[1, 2],\n",
    "          [3, 4]])\n",
    "except Exception as e:\n",
    "    print(e)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Other than that, all the same operations described in the section on Operators are the same, such as matrix multiplication, tensor products, etc:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$-\\left|1\\right>$"
      ],
      "text/plain": [
       "-|1>"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "U|Ket(0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "PyQC comes with many gates useful in quantum circuits right out of the box.  When one does `from pyqc import *` the local namespace is populated with all of the included gates.  Many of these are imported as single-character variables and a few with multiple characters.  It is useful to think of these predefined gates almost like special glyphs that can be used both in expressions, and describing intructions for a quantum circuit as we will see in the next section.\n",
    "\n",
    "The currently included gates are the Hadamard gate:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$\\hat{H} = \\left(\\begin{array}{*{11}c}0.707 & 0.707\\\\0.707 & -0.707\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "H = [[ 0.70710678  0.70710678]\n",
       "     [ 0.70710678 -0.70710678]]"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "H"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The phase gate (on some systems this may be displayed with a real component very close to but not quite zero; this is a known issue that will be addressed):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$\\hat{S} = \\left(\\begin{array}{*{11}c}1.0 & 0.0\\\\0.0 & 1.0j\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "S = [[1.000000e+00+0.j 0.000000e+00+0.j]\n",
       "     [0.000000e+00+0.j 6.123234e-17+1.j]]"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "S"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The $ \\pi/8 $ gate:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$\\hat{T} = \\left(\\begin{array}{*{11}c}1.0 & 0.0\\\\0.0 & (0.707+0.707j)\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "T = [[1.        +0.j         0.        +0.j        ]\n",
       "     [0.        +0.j         0.70710678+0.70710678j]]"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "T"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The CNOT gate:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$\\mathrm{CNOT} = \\left(\\begin{array}{*{11}c}1.0 & 0.0 & 0.0 & 0.0\\\\0.0 & 1.0 & 0.0 & 0.0\\\\0.0 & 0.0 & 0.0 & 1.0\\\\0.0 & 0.0 & 1.0 & 0.0\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "CNOT = [[1. 0. 0. 0.]\n",
       "        [0. 1. 0. 0.]\n",
       "        [0. 0. 0. 1.]\n",
       "        [0. 0. 1. 0.]]"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "CNOT"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The swap gate:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$SWAP = \\left(\\begin{array}{*{11}c}1.0 & 0.0 & 0.0 & 0.0\\\\0.0 & 0.0 & 1.0 & 0.0\\\\0.0 & 1.0 & 0.0 & 0.0\\\\0.0 & 0.0 & 0.0 & 1.0\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "SWAP = [[1. 0. 0. 0.]\n",
       "        [0. 0. 1. 0.]\n",
       "        [0. 1. 0. 0.]\n",
       "        [0. 0. 0. 1.]]"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "SWAP"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Toffoli gate:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$\\mathrm{Toffoli} = \\left(\\begin{array}{*{11}c}1.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0\\\\0.0 & 1.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0\\\\0.0 & 0.0 & 1.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0\\\\0.0 & 0.0 & 0.0 & 1.0 & 0.0 & 0.0 & 0.0 & 0.0\\\\0.0 & 0.0 & 0.0 & 0.0 & 1.0 & 0.0 & 0.0 & 0.0\\\\0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 1.0 & 0.0 & 0.0\\\\0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 1.0\\\\0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 1.0 & 0.0\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "Toffoli = [[1. 0. 0. 0. 0. 0. 0. 0.]\n",
       "           [0. 1. 0. 0. 0. 0. 0. 0.]\n",
       "           [0. 0. 1. 0. 0. 0. 0. 0.]\n",
       "           [0. 0. 0. 1. 0. 0. 0. 0.]\n",
       "           [0. 0. 0. 0. 1. 0. 0. 0.]\n",
       "           [0. 0. 0. 0. 0. 1. 0. 0.]\n",
       "           [0. 0. 0. 0. 0. 0. 0. 1.]\n",
       "           [0. 0. 0. 0. 0. 0. 1. 0.]]"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Toffoli"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And the Pauli gates including the identity gate:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$\\hat{I} = \\left(\\begin{array}{*{11}c}1.0 & 0.0\\\\0.0 & 1.0\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "I = [[1. 0.]\n",
       "     [0. 1.]]"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "I"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$\\hat{X} = \\left(\\begin{array}{*{11}c}0.0 & 1.0\\\\1.0 & 0.0\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "X = [[0. 1.]\n",
       "     [1. 0.]]"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "X"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$\\hat{Y} = \\left(\\begin{array}{*{11}c}0.0 & -1.0j\\\\1.0j & 0.0\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "Y = [[0.+0.j 0.-1.j]\n",
       "     [0.+1.j 0.+0.j]]"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Y"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$\\hat{Z} = \\left(\\begin{array}{*{11}c}1.0 & 0.0\\\\0.0 & -1.0\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "Z = [[ 1.  0.]\n",
       "     [ 0. -1.]]"
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Z"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It's worth noting that the identity gate is parameterized in that by default it works on a single qubit, but by *calling* the `I` object with an integer argument we can make $ n $-qubit identity gates:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$\\hat{I}^{\\otimes 2} = \\left(\\begin{array}{*{11}c}1.0 & 0.0 & 0.0 & 0.0\\\\0.0 & 1.0 & 0.0 & 0.0\\\\0.0 & 0.0 & 1.0 & 0.0\\\\0.0 & 0.0 & 0.0 & 1.0\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "I(2) = [[1. 0. 0. 0.]\n",
       "        [0. 1. 0. 0.]\n",
       "        [0. 0. 1. 0.]\n",
       "        [0. 0. 0. 1.]]"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "I(2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$\\hat{I}^{\\otimes 3} = \\left(\\begin{array}{*{11}c}1.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0\\\\0.0 & 1.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0\\\\0.0 & 0.0 & 1.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0\\\\0.0 & 0.0 & 0.0 & 1.0 & 0.0 & 0.0 & 0.0 & 0.0\\\\0.0 & 0.0 & 0.0 & 0.0 & 1.0 & 0.0 & 0.0 & 0.0\\\\0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 1.0 & 0.0 & 0.0\\\\0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 1.0 & 0.0\\\\0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 1.0\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "I(3) = [[1. 0. 0. 0. 0. 0. 0. 0.]\n",
       "        [0. 1. 0. 0. 0. 0. 0. 0.]\n",
       "        [0. 0. 1. 0. 0. 0. 0. 0.]\n",
       "        [0. 0. 0. 1. 0. 0. 0. 0.]\n",
       "        [0. 0. 0. 0. 1. 0. 0. 0.]\n",
       "        [0. 0. 0. 0. 0. 1. 0. 0.]\n",
       "        [0. 0. 0. 0. 0. 0. 1. 0.]\n",
       "        [0. 0. 0. 0. 0. 0. 0. 1.]]"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "I(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To be clear, `I(n)` does not create an $ n \\times n $ identity, but rather a $ 2^n \\times 2^n $ identity where $ n $ is the number of qubits we are working with.  The $ \\otimes n $ superscript is added to the representation to eliminate ambiguity--in a later version it should be possible to customize this for (really the majority of) cases where it is unambiguous.\n",
    "\n",
    "The $ \\text{SWAP} $ gate is similarly parameterized so that it can work in systems of more than two qubits.  For example to swap the first and third qubit in a circuit we can use:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left|110\\right>$"
      ],
      "text/plain": [
       "|1, 1, 0>"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "SWAP(3)|Ket(0, 1, 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Or to swap the second and third qubits in a three qubit circuit we could use the tensor product of $ I $ with $ \\text{SWAP} $ (a future version will have options to make this more straightforward):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left|010\\right>$"
      ],
      "text/plain": [
       "|0, 1, 0>"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(I*SWAP)|Ket(0, 0, 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Controlled gates like $ \\text{CNOT} $ and $ \\text{Toffoli} $ have similar options as we will see in the next section."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Controlled Gates\n",
    "\n",
    "There is also a special class called `ControlledGate` that can be used to implement any $ n $-qubit controlled gate.  For example one can create a controlled $ Y $ gate with one control bit like so:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$C(\\hat{Y}) = \\left(\\begin{array}{*{11}c}1.0 & 0.0 & 0.0 & 0.0\\\\0.0 & 1.0 & 0.0 & 0.0\\\\0.0 & 0.0 & 0.0 & -1.0j\\\\0.0 & 0.0 & 1.0j & 0.0\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "C(Y) = [[1.+0.j 0.+0.j 0.+0.j 0.+0.j]\n",
       "        [0.+0.j 1.+0.j 0.+0.j 0.+0.j]\n",
       "        [0.+0.j 0.+0.j 0.+0.j 0.-1.j]\n",
       "        [0.+0.j 0.+0.j 0.+1.j 0.+0.j]]"
      ]
     },
     "execution_count": 20,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "C_Y = ControlledGate(Y)\n",
    "\n",
    "C_Y"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Or with two or more control bits:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$C^{2}(\\hat{Y}) = \\left(\\begin{array}{*{11}c}1.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0\\\\0.0 & 1.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0\\\\0.0 & 0.0 & 1.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0\\\\0.0 & 0.0 & 0.0 & 1.0 & 0.0 & 0.0 & 0.0 & 0.0\\\\0.0 & 0.0 & 0.0 & 0.0 & 1.0 & 0.0 & 0.0 & 0.0\\\\0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 1.0 & 0.0 & 0.0\\\\0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0 & -1.0j\\\\0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 1.0j & 0.0\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "C2(Y) = [[1.+0.j 0.+0.j 0.+0.j 0.+0.j 0.+0.j 0.+0.j 0.+0.j 0.+0.j]\n",
       "         [0.+0.j 1.+0.j 0.+0.j 0.+0.j 0.+0.j 0.+0.j 0.+0.j 0.+0.j]\n",
       "         [0.+0.j 0.+0.j 1.+0.j 0.+0.j 0.+0.j 0.+0.j 0.+0.j 0.+0.j]\n",
       "         [0.+0.j 0.+0.j 0.+0.j 1.+0.j 0.+0.j 0.+0.j 0.+0.j 0.+0.j]\n",
       "         [0.+0.j 0.+0.j 0.+0.j 0.+0.j 1.+0.j 0.+0.j 0.+0.j 0.+0.j]\n",
       "         [0.+0.j 0.+0.j 0.+0.j 0.+0.j 0.+0.j 1.+0.j 0.+0.j 0.+0.j]\n",
       "         [0.+0.j 0.+0.j 0.+0.j 0.+0.j 0.+0.j 0.+0.j 0.+0.j 0.-1.j]\n",
       "         [0.+0.j 0.+0.j 0.+0.j 0.+0.j 0.+0.j 0.+0.j 0.+1.j 0.+0.j]]"
      ]
     },
     "execution_count": 21,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "C_Y2 = ControlledGate(Y, n_control=2)\n",
    "\n",
    "C_Y2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In fact, the $ \\text{CNOT} $ and $ \\text{Toffoli} $ gates are just special cases of `ControlledGate`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By default a controlled one qubit gate with one control bit works in a 2-qubit state space, as in the case of `CNOT`.  But like with the `SWAP` gate we can also parameterize `ControlledGates` to work in higher-dimensional spaces.  By default the control bit or (bits) are the first qubits, and the target bit (or bits) are the last.  For example we can use a `CNOT` on a 3-qubit state where the first qubit is the control and the third qubit is the target:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left|101\\right>$"
      ],
      "text/plain": [
       "|1, 0, 1>"
      ]
     },
     "execution_count": 22,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "CNOT(3)|Ket(1, 0, 0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But that's not all--when calling `CNOT` it also takes optional arguments of `control` (or `c` for short) and/or `target` (or `t` for short) that can control which qubit in the target state is mapped to which input.  For example make the second qubit the target and the third the control:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left|011\\right>$"
      ],
      "text/plain": [
       "|0, 1, 1>"
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "CNOT(3, c=2, t=1)|Ket(0, 0, 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Multi-qubit gates can be controlled as well:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [],
   "source": [
    "C_XX = ControlledGate(X * X)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By default this works on a three qubit state where the first qubit is the control bit and the other two are the targets:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left|110\\right>$"
      ],
      "text/plain": [
       "|1, 1, 0>"
      ]
     },
     "execution_count": 25,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "C_XX|Ket(1, 0, 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As in the previous example the order of the control and targets can be swapped around, with one caveat: The control qubits must all be contiguous.  That being the case, only the *first* target qubit needs to be specified.  For example to use this `C_XX` operator on a 4 qubit state where the first two qubits are the target and the last is the control:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left|1001\\right>$"
      ],
      "text/plain": [
       "|1, 0, 0, 1>"
      ]
     },
     "execution_count": 26,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "C_XX(4, t=0, c=3)|Ket(0, 1, 0, 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The same concepts go for gates with multiple control bits.  In this case the control bit inputs are labeled `control0`, `control1`, and so on.  The shortened named `c0`, `c1`, etc. can also be used:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left|111\\right>$"
      ],
      "text/plain": [
       "|1, 1, 1>"
      ]
     },
     "execution_count": 27,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Toffoli(t=1, c0=0, c1=2)|Ket(1, 0, 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This scheme can be used for creating arbitrary control gates for use in expressions, but is admittedly a bit cumbersome.  As we will see in the next section, using the `Circuit` class tends to be a more straightforward way to specify mappings of qubits to inputs on quantum gates."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
