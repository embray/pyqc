{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "PyQC is a concise and Pythonic toolbox for constructing and evaluating quantum circuits.  It is built on top of the powerful [QuTiP](http://qutip.org/) (Quantum Toolbox in Python) framework which provides most of the numerical heavy lifting, but adds a high-level user interface that strives to mimic how we write equations with quantum states and operators as closely as is possible in Python.  It also provides results rendered in LaTeX, as well as rendering of quantum circuit diagrams."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# PyQC basics\n",
    "\n",
    "This section provides a crash course on the core features of PyQC.  Later sections of this documentation go into further detail on how to use each of the objects described here.\n",
    "\n",
    "The easiest way to start using PyQC, once it is installed, is to import the entire `pyqc` module into your local namespace:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyqc import *"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One of the most basic objects in PyQC, representing an $ n $-qubit state is the `Ket` (as in a ket vector).  To create a single-qubit state representing the value 0, just instantiate a `Ket` like so:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left|0\\right>$"
      ],
      "text/plain": [
       "|0>"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Ket(0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also create superposition states using the addition operator `+`.  By default all superpositions are normalized automatically:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "\\begin{equation}\\frac{\\left|0\\right> + \\left|1\\right>}{\\sqrt{2}}\\end{equation}"
      ],
      "text/plain": [
       "sqrt(1/2)(|0> + |1>)"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Ket(0) + Ket(1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Multiple qubit states can be created in several ways that will be discussed in full in the section on state vectors.  The simplest, however, is to instantiate a `Ket` with a sequence of bits.  For example, we can create a 3-qubit state representing the value 5 like so:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left|101\\right>$"
      ],
      "text/plain": [
       "|1, 0, 1>"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Ket(1, 0, 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Operators on $n$-qubit states can created by instantiating an `Operator` object with the matrix elements for that operator:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$\\hat{O}_{(0)} = \\left(\\begin{array}{*{11}c}1.0 & 0.0\\\\0.0 & 0.0\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "O_0 = [[1. 0.]\n",
       "       [0. 0.]]"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Operator([[1, 0],\n",
    "          [0, 0]])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the above example the \"(0)\" subscript is used to indicate that this operator is given a default name (the next operator would have a default name of $ \\hat{O}_{(1)} $ and so on).  Operators can also be given custom names and LaTeX representations for use in equation rendering.  The previous example might be rewritten to emphasize that this is a projection operator:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$P_0 = \\left(\\begin{array}{*{11}c}1.0 & 0.0\\\\0.0 & 0.0\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "P_0 = [[1. 0.]\n",
       "       [0. 0.]]"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "P_0 = Operator([[1, 0],\n",
    "                [0, 0]],\n",
    "               name='P_0', latex_name='P_0')\n",
    "P_0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Operators can act on a state using the `|` syntax in Python (this is different from other linear algebra systems you might be familiar with that use `*` to perform matrix multiplication).  The output, as one would expect, is a new state:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left|0\\right>$"
      ],
      "text/plain": [
       "|0>"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "psi = Ket(0) + Ket(1)\n",
    "P_0|psi"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "PyQC includes operators for most common quantum gates out of the box.  For example the Hadamard gate:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$\\hat{H} = \\left(\\begin{array}{*{11}c}0.707 & 0.707\\\\0.707 & -0.707\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "H = [[ 0.70710678  0.70710678]\n",
       "     [ 0.70710678 -0.70710678]]"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "H"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The CNOT gate:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$\\mathrm{CNOT} = \\left(\\begin{array}{*{11}c}1.0 & 0.0 & 0.0 & 0.0\\\\0.0 & 1.0 & 0.0 & 0.0\\\\0.0 & 0.0 & 0.0 & 1.0\\\\0.0 & 0.0 & 1.0 & 0.0\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "CNOT = [[1. 0. 0. 0.]\n",
       "        [0. 1. 0. 0.]\n",
       "        [0. 0. 0. 1.]\n",
       "        [0. 0. 1. 0.]]"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "CNOT"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Toffoli gate:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$\\mathrm{Toffoli} = \\left(\\begin{array}{*{11}c}1.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0\\\\0.0 & 1.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0\\\\0.0 & 0.0 & 1.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0\\\\0.0 & 0.0 & 0.0 & 1.0 & 0.0 & 0.0 & 0.0 & 0.0\\\\0.0 & 0.0 & 0.0 & 0.0 & 1.0 & 0.0 & 0.0 & 0.0\\\\0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 1.0 & 0.0 & 0.0\\\\0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 1.0\\\\0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0 & 1.0 & 0.0\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "Toffoli = [[1. 0. 0. 0. 0. 0. 0. 0.]\n",
       "           [0. 1. 0. 0. 0. 0. 0. 0.]\n",
       "           [0. 0. 1. 0. 0. 0. 0. 0.]\n",
       "           [0. 0. 0. 1. 0. 0. 0. 0.]\n",
       "           [0. 0. 0. 0. 1. 0. 0. 0.]\n",
       "           [0. 0. 0. 0. 0. 1. 0. 0.]\n",
       "           [0. 0. 0. 0. 0. 0. 0. 1.]\n",
       "           [0. 0. 0. 0. 0. 0. 1. 0.]]"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Toffoli"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And the Pauli matrices.  For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$\\hat{Z} = \\left(\\begin{array}{*{11}c}1.0 & 0.0\\\\0.0 & -1.0\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "Z = [[ 1.  0.]\n",
       "     [ 0. -1.]]"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Z"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Operators can be combined using the `|` syntax, just as we used to apply an operator to a state.  For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$\\hat{H}\\hat{Z}\\hat{H} = \\left(\\begin{array}{*{11}c}0.0 & 1.000\\\\1.000 & 0.0\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "H | Z | H = [[0. 1.]\n",
       "             [1. 0.]]"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "H|Z|H"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also use tensor products of operators to create operators on multi-qubit states.  The `*` symbol is reserved for tensor products between matrices and/or states.  For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$\\hat{X} \\otimes \\hat{X} = \\left(\\begin{array}{*{11}c}0.0 & 0.0 & 0.0 & 1.0\\\\0.0 & 0.0 & 1.0 & 0.0\\\\0.0 & 1.0 & 0.0 & 0.0\\\\1.0 & 0.0 & 0.0 & 0.0\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "X * X = [[0. 0. 0. 1.]\n",
       "         [0. 0. 1. 0.]\n",
       "         [0. 1. 0. 0.]\n",
       "         [1. 0. 0. 0.]]"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "O = (X * X)\n",
    "O"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left|11\\right>$"
      ],
      "text/plain": [
       "|1, 1>"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "O|Ket(0, 0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Operator products and tensor products can be combined arbitrarily to build up entire circuits:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$(\\hat{H} \\otimes \\hat{H})\\mathrm{CNOT}(\\hat{H} \\otimes \\hat{H}) = \\left(\\begin{array}{*{11}c}1.000 & 0.0 & 0.0 & 0.0\\\\0.0 & 0.0 & 0.0 & 1.000\\\\0.0 & 0.0 & 1.000 & 0.0\\\\0.0 & 1.000 & 0.0 & 0.0\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "(H * H) | CNOT | (H * H) = \\\n",
       "    [[1. 0. 0. 0.]\n",
       "     [0. 0. 0. 1.]\n",
       "     [0. 0. 1. 0.]\n",
       "     [0. 1. 0. 0.]]"
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "C = (H * H)|CNOT|(H * H)\n",
    "C"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left|01\\right>$"
      ],
      "text/plain": [
       "|0, 1>"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "C|Ket(1, 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also define `Circuit` objects which are just a special class of operator built up from a list of circuit instructions, sort of like a simple assembly language.  The `Circuit` is initialized with a list of tuples, the first element of which is an operation represented by the gate object itself, such as `H`, and the second is either an integer corresponding to a qubit to apply the gate to, or a tuple for multi-qubit gates.  In this circuit description language the above example would be written:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [],
   "source": [
    "C2 = Circuit([\n",
    "    (H, 0),\n",
    "    (H, 1),\n",
    "    (CNOT, (0, 1)),\n",
    "    (H, 0),\n",
    "    (H, 1)\n",
    "])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is equivalent to the gate defined in the previous example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "C == C2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This `Circuit` object is also an `Operator` equivalent to the one we built earlier using mathematical expressions, and can operate on a state in the same way:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left|01\\right>$"
      ],
      "text/plain": [
       "|0, 1>"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "C|Ket(1, 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Though one other advantage of `Circuit` objects is that by default they are rendered as circuit diagrams (this feature currently requires a full-featured LaTeX distribution [TexLive](https://www.tug.org/texlive/)):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAJcAAABCBAMAAACoQlnWAAAAMFBMVEX///+qqqoyMjK6urrc3NwQEBBUVFQiIiLu7u52dnbMzMyYmJiIiIhERERmZmYAAAAIOT9MAAAAAXRSTlMAQObYZgAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAn9JREFUWMPtmD9oE1Ecx79pY/t6vfaEdqggNOiiIBjaxaGUrFqwDiIWFDMWdAgo/hmkcRGC0HYpbZy6ut1SatvBg27icIObHUQojp5WUq3K+e7Fvry8u/Ducm8oNkcgL+/3yye/+929u08OiN4sX9y8RNEwrOmTlyh6jGBOethHPppND3P5aEInrEsnrC8vp1fu3CXLNScaRpZqdmXuXgtYthj67ae03lwjfWz/pBDtpV89E6rs3eaKxP2XTn4BC4s8vdv3vwrRBRvkpwwbcAZLbLglwzLfgdE8T+/y/X0hOlpAZk+GraOneHmjAAzIsMFrwJdGerC8heguTfBk2AxO2OeQq8P49YB15Um1ejsY+PIWTD6qVpdcMerBze5h+koJZ4GrcmVBV36jRc9YQx2pMuMH1vpz+AC8lGFBVw6E9DE/34iyhq7Ku3mT3DBd7EQczVBXLOGdN7QJNrl1i1Z2ARlbhj2sn0zRsN4iOw/l84wcGCXcR09ZgmVDXRFh04cNbYZ1e3iMEp5LC4Z8/uPMXzzfAjY/t0M+fVsMwfpcvNpYxbZ6KVvKhU52Z0KrqU1Y4+JYTg9LdA/472CIFACrPT2IU2/KrQPrwI4wzNK5nDrXM122Hf+GMpselsS2jTcj2mzbeGtNObpse4jOVjTZNrGDWUePbZtsdliPbfez2Rdq28Z7RAgyl8NIQeZeKQgys+1T1xGh7ty2Dyuje/qMR7ltC5UFtl3AAyht22Sv10rbBoMpbJseTRPEUdp2Haay7XEKq6htm8GUtm1sm8IKaGnbDKa27ezpS3FsG/RoxrBtxLLtiZodx7bVf6sT2Hb8RxHl9LDj/WAJ7TyM+wuoo/JM9eUSPgAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "C_0 = [[1. 0. 0. 0.]\n",
       "       [0. 0. 0. 1.]\n",
       "       [0. 0. 1. 0.]\n",
       "       [0. 1. 0. 0.]]"
      ]
     },
     "execution_count": 20,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "C2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is a way also write circuits using an interface that lays them out similar to a circuit diagram, where each row represents the operations performed on each qubit in the circuit.  Multi-qubit operators have labels representing each of their inputs.  For example `CNOT.target` and `CNOT.control` (or we can use `CNOT.t` and `CNOT.c` for short).  This is mostly useful for toy examples:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAJcAAABCBAMAAACoQlnWAAAAMFBMVEX///+qqqoyMjK6urrc3NwQEBBUVFQiIiLu7u52dnbMzMyYmJiIiIhERERmZmYAAAAIOT9MAAAAAXRSTlMAQObYZgAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAn9JREFUWMPtmD9oE1Ecx79pY/t6vfaEdqggNOiiIBjaxaGUrFqwDiIWFDMWdAgo/hmkcRGC0HYpbZy6ut1SatvBg27icIObHUQojp5WUq3K+e7Fvry8u/Ducm8oNkcgL+/3yye/+929u08OiN4sX9y8RNEwrOmTlyh6jGBOethHPppND3P5aEInrEsnrC8vp1fu3CXLNScaRpZqdmXuXgtYthj67ae03lwjfWz/pBDtpV89E6rs3eaKxP2XTn4BC4s8vdv3vwrRBRvkpwwbcAZLbLglwzLfgdE8T+/y/X0hOlpAZk+GraOneHmjAAzIsMFrwJdGerC8heguTfBk2AxO2OeQq8P49YB15Um1ejsY+PIWTD6qVpdcMerBze5h+koJZ4GrcmVBV36jRc9YQx2pMuMH1vpz+AC8lGFBVw6E9DE/34iyhq7Ku3mT3DBd7EQczVBXLOGdN7QJNrl1i1Z2ARlbhj2sn0zRsN4iOw/l84wcGCXcR09ZgmVDXRFh04cNbYZ1e3iMEp5LC4Z8/uPMXzzfAjY/t0M+fVsMwfpcvNpYxbZ6KVvKhU52Z0KrqU1Y4+JYTg9LdA/472CIFACrPT2IU2/KrQPrwI4wzNK5nDrXM122Hf+GMpselsS2jTcj2mzbeGtNObpse4jOVjTZNrGDWUePbZtsdliPbfez2Rdq28Z7RAgyl8NIQeZeKQgys+1T1xGh7ty2Dyuje/qMR7ltC5UFtl3AAyht22Sv10rbBoMpbJseTRPEUdp2Haay7XEKq6htm8GUtm1sm8IKaGnbDKa27ezpS3FsG/RoxrBtxLLtiZodx7bVf6sT2Hb8RxHl9LDj/WAJ7TyM+wuoo/JM9eUSPgAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "C_1 = [[1. 0. 0. 0.]\n",
       "       [0. 0. 0. 1.]\n",
       "       [0. 0. 1. 0.]\n",
       "       [0. 1. 0. 0.]]"
      ]
     },
     "execution_count": 21,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Circuit.from_wires(\n",
    "    [H, CNOT.c, H],\n",
    "    [H, CNOT.t, H]\n",
    ")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
