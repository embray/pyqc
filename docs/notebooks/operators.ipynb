{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "PyQC provides quantum operators via the `Operator` class, and more specialized subclasses thereof such as the `Gate` and `Circuit` classes.  The `Operator` class itself is the most generic, and is not currently used much directly in PyQC, but is provided for completeness.  Instances of `Operator` can be created by providing the matrix elements of the operator, but it can also be subclassed for more specialized purposes.  There are also future plans to make it easier to create specialized and parameterized operators more easily without subclassing.\n",
    "\n",
    "To instantiate an `Operator` from its matrix elements, we can use either a list of Python lists, a Numpy `ndarray`, or an existing PyQC `Operator` or a `qutip.Qobj` operator.  The simplest is to just use Python lists:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyqc import *\n",
    "\n",
    "O = Operator([[1, 2],\n",
    "              [3, 4]])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The default display formatting for operators includes both the operator's *name* and its matrix representation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$\\hat{O}_{(0)} = \\left(\\begin{array}{*{11}c}1.0 & 2.0\\\\3.0 & 4.0\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "O_0 = [[1. 2.]\n",
       "       [3. 4.]]"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "O"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this case the somewhat strange formatting for the name, with the `(0)` subscript, indicates that this name was assigned by default.  Subsequent operators without explicit names will be given subscripts like `(1)` and so on.  Every operator created in PyQC is given a name even if we don't provide one explicitly.  These names are used for more than just display formatting; for example they can be used to map instructions in quantum assembly languages to operators that implement those instructions.  Other uses are anticipated in future releases.\n",
    "\n",
    "It should be made clear that an operator's name is *not* related to the Python variable it is assigned to, as Python has no mechanism to directly link an object to the variable pointing to that object.\n",
    "\n",
    "To give an operator an *explicit* name, use the `name=` argument when creating the operator:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$\\hat{A} = \\left(\\begin{array}{*{11}c}1.0 & 2.0\\\\3.0 & 4.0\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "A = [[1. 2.]\n",
       "     [3. 4.]]"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "A = Operator([[1, 2],\n",
    "              [3, 4]], name='A')\n",
    "\n",
    "A"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are a couple other display formatting options for operators.  Currently these include `'name'` which displays just the name by default, and `'matrix'` which displays just the matrix representation.  The default, `'both'`, displays both.  To set an operator's default formatting use the `representation=` argument at instantiation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$\\hat{A}$$"
      ],
      "text/plain": [
       "A"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Operator([[1, 2],\n",
    "          [3, 4]], name='A', representation='name')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$\\left(\\begin{array}{*{11}c}1.0 & 2.0\\\\3.0 & 4.0\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "[[1. 2.]\n",
       " [3. 4.]]"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Operator([[1, 2],\n",
    "          [3, 4]], name='A', representation='matrix')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As with `Ket` and `Bra` objects this only affects how operators are displayed, and has no impact on its numerical evaluation.\n",
    "\n",
    "If you don't like the default representation of placing hats over the operator names, or want some other custom LaTeX symbol, one can also specify the `latex_name=` argument in addition to `name`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$R_4 = \\left(\\begin{array}{*{11}c}1.0 & 0.0\\\\0.0 & (0.924+0.383j)\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "R_4 = [[1.        +0.j         0.        +0.j        ]\n",
       "       [0.        +0.j         0.92387953+0.38268343j]]"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "import numpy as np\n",
    "\n",
    "R_4 = Operator([[1, 0],\n",
    "                [0, np.exp(1j * 2 * np.pi / (2 ** 4))]], name='R_4', latex_name='R_4')\n",
    "\n",
    "R_4"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Applying Operators\n",
    "\n",
    "Operators can be applied to states using the `|` operator.  As explained in the section on state vectors, this operator is used by PyQC--somewhat unusually--to represent matrix multiplication.  This does map somewhat elegantly to the mathematical notation, however, where applying an operator to a ket might be written like $ \\hat{A}|0\\rangle $.  In PyQC this same expression is written:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "\\begin{equation}0.31622776601683794\\left|0\\right> + 0.9486832980505138\\left|1\\right>\\end{equation}"
      ],
      "text/plain": [
       "0.31622776601683794|0> + 0.9486832980505138|1>"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "A|Ket(0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The return value being the appropriate `Ket` object representing the *normalized* result of applying this operator to the original state:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "pyqc.braket.Ket"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "type(A|Ket(0))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Operator Expressions\n",
    "\n",
    "Operators can be applied to other operators using the same notation to create new operators following the rules of matrix multiplication:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$\\hat{A}\\hat{B} = \\left(\\begin{array}{*{11}c}1.0 & 0.0\\\\3.0 & 0.0\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "A | B = [[1. 0.]\n",
       "         [3. 0.]]"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "B = Operator([[1, 0],\n",
    "              [0, 0]], name='B')\n",
    "              \n",
    "A|B"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This syntax is about the closest we can get, visually, to the common mathematical notation of using concatenation to imply multiplication.  The return value is a new operator, the default name of which is taken from the mathematical expression of existing operators used to create that operator; in this case $ \\hat{A}\\hat{B} $, or just `'A | B'` in plain text:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'A | B'"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(A|B).name"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also give this operator its own name by passing the expression in to the `Operator` constructor:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$\\hat{C} = \\left(\\begin{array}{*{11}c}1.0 & 0.0\\\\3.0 & 0.0\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "C = [[1. 0.]\n",
       "     [3. 0.]]"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "C = Operator(A|B, name='C')\n",
    "\n",
    "C"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Any number of operators can be used in an expression:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$\\hat{B}\\hat{A}\\hat{C} = \\left(\\begin{array}{*{11}c}7.0 & 0.0\\\\0.0 & 0.0\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "B | A | C = [[7. 0.]\n",
       "             [0. 0.]]"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "B|A|C"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The other important binary operator that can be used with two `Operator` instances is `*` which is used in PyQC to represent the tensor product:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$\\hat{A} \\otimes \\hat{B} = \\left(\\begin{array}{*{11}c}1.0 & 0.0 & 2.0 & 0.0\\\\0.0 & 0.0 & 0.0 & 0.0\\\\3.0 & 0.0 & 4.0 & 0.0\\\\0.0 & 0.0 & 0.0 & 0.0\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "A * B = [[1. 0. 2. 0.]\n",
       "         [0. 0. 0. 0.]\n",
       "         [3. 0. 4. 0.]\n",
       "         [0. 0. 0. 0.]]"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "A * B"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As with multiplication, this returns a new operator that is the result of that expression, the default name of which is also taken from the expression:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'A * B'"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(A * B).name"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Tensor products and multiplication can be freely mixed within an expression:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$(\\hat{A} \\otimes \\hat{B})(\\hat{A} \\otimes \\hat{C}) = \\left(\\begin{array}{*{11}c}7.0 & 0.0 & 10.0 & 0.0\\\\0.0 & 0.0 & 0.0 & 0.0\\\\15.0 & 0.0 & 22.0 & 0.0\\\\0.0 & 0.0 & 0.0 & 0.0\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "(A * B) | (A * C) = \\\n",
       "    [[ 7.  0. 10.  0.]\n",
       "     [ 0.  0.  0.  0.]\n",
       "     [15.  0. 22.  0.]\n",
       "     [ 0.  0.  0.  0.]]"
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(A * B)|(A * C)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Such expressions can also be applied directly to a state:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "\\begin{equation}0.42288546533112387\\left|00\\right> + 0.9061831399952655\\left|10\\right>\\end{equation}"
      ],
      "text/plain": [
       "0.42288546533112387|0, 0> + 0.9061831399952655|1, 0>"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(A * B)|(A * C)|Ket(0, 0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One important point to note about operator expressions is that their matrix elements are *not* immediately computed unless the matrix is displayed as in the examples above, or the operator is applied to a state.  That is, if we just assign an operator to an expression like:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [],
   "source": [
    "D = (A * B)|(A * C)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "...the full expression is not actually evaluated until a numerical result is required.  This is especially useful when using combinations of operators to create a custom gate or subroutine for later use in a circuit."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## QuTiP Interface\n",
    "\n",
    "As with `Ket` states, most PyQC operators have an underlying `qutip.Qobj` representation which can be accessed via the `.qobj` attribute:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "Quantum object: dims = [[2], [2]], shape = (2, 2), type = oper, isherm = False\\begin{equation*}\\left(\\begin{array}{*{11}c}1.0 & 2.0\\\\3.0 & 4.0\\\\\\end{array}\\right)\\end{equation*}"
      ],
      "text/plain": [
       "Quantum object: dims = [[2], [2]], shape = (2, 2), type = oper, isherm = False\n",
       "Qobj data =\n",
       "[[1. 2.]\n",
       " [3. 4.]]"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "A.qobj"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consistent with the previous note about operator expressions, the `Qobj` for an operator created from an *expression* of operators is not computed until the `.qobj` attribute is explicitly accessed, saving on time and space when building up complicated expressions.\n",
    "\n",
    "One feature of the underlying `Qobj` is its `.dims` attribute--the value `[[2], [2]]` in the above example.  This is distinct from the `.shape` attribute which just indicates that the operator is represented by a $ 2 \\times 2 $ matrix, in that it also preserves the tensor structure when the tensor product is taken between two operators.  The above example indicates that this operator acts on states in a single two-dimensional vector space.  But if we take the tensor product of two such operators we get:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "Quantum object: dims = [[2, 2], [2, 2]], shape = (4, 4), type = oper, isherm = False\\begin{equation*}\\left(\\begin{array}{*{11}c}1.0 & 2.0 & 2.0 & 4.0\\\\3.0 & 4.0 & 6.0 & 8.0\\\\3.0 & 6.0 & 4.0 & 8.0\\\\9.0 & 12.0 & 12.0 & 16.0\\\\\\end{array}\\right)\\end{equation*}"
      ],
      "text/plain": [
       "Quantum object: dims = [[2, 2], [2, 2]], shape = (4, 4), type = oper, isherm = False\n",
       "Qobj data =\n",
       "[[ 1.  2.  2.  4.]\n",
       " [ 3.  4.  6.  8.]\n",
       " [ 3.  6.  4.  8.]\n",
       " [ 9. 12. 12. 16.]]"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(A * A).qobj"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here the `.dims` attribute is `[[2, 2], [2, 2]]` which indicates that it operates on vectors in a product of two 2-dimensional vector spaces--that is, states representing two qubits.  In fact we can see that `Ket` vectors preserve this information as well:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "Quantum object: dims = [[2, 2], [1, 1]], shape = (4, 1), type = ket\\begin{equation*}\\left(\\begin{array}{*{11}c}1.0\\\\0.0\\\\0.0\\\\0.0\\\\\\end{array}\\right)\\end{equation*}"
      ],
      "text/plain": [
       "Quantum object: dims = [[2, 2], [1, 1]], shape = (4, 1), type = ket\n",
       "Qobj data =\n",
       "[[1.]\n",
       " [0.]\n",
       " [0.]\n",
       " [0.]]"
      ]
     },
     "execution_count": 20,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Ket(0, 0).qobj"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This indicates that this is a vector in a product of two 2-dimensional vector spaces.  The fact that this information is kept track of will be useful, for example, when computing the partial trace of an operator."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
