{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "PyQC includes prototype support for states represented in the stabilizer formalism, and evaluation of stabilizer circuits on those states.  The implementation is based on the [\"CHP\"](http://www.scottaaronson.com/chp/) algorithm by Aaronson and Goettsman ([arXiv:quant-ph/0406196](http://arxiv.org/abs/quant-ph/0406196)).\n",
    "\n",
    "Stabilizer states are represented in PyQC by the `StabilizerState` class, which is used as an alternative to the `Ket` class in operations.  Currently, the `StabilizerState` constructor takes a single argument--the number of qubits represented by the state--and returns a state initialized to the all zeros state:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyqc import *\n",
    "\n",
    "s = StabilizerState(2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "+ZI\n",
       "+IZ"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "s"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`StabilizerState` instances are represented by the elements in the $n$-qubit Pauli group $ \\mathcal{P}_n $ that generate the group of *stabilizers* for the quantum state in question.  In principle this representation could then be converted to the state vector representation--and in fact that capability *will* be added--but the advantage of the stabilizer representation is that it can represent stabilizer states for a large number of qubits efficiently on a classical computer.  Whereas, for example, an arbitrary 1024-qubit state would require an unthinkable amount of space to represent as a vector (at least $ 2^1024 $ bits), the equivalent `StabilizerState` requires only about 12292 bits in the current implementation (and could be made to use even slightly less space).\n",
    "\n",
    "This formalism does come with certain limitations, but it is still useful for a broad class of problems, especially in quantum error correction.\n",
    "\n",
    "The current prototype implementation of `StabilizerState` supports application of the Hadamard, phase, and CNOT gates, as well as the identity (trivially) and Pauli X, Y, and Z gates since they can be easily decomposed into a combination of Hadmard, phase, and CNOT gates.\n",
    "\n",
    "As mentioned earlier, within these limitations, `StabilizerState` objects can be used in PyQC in the same way as `Ket` objects:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left|1\\right>$"
      ],
      "text/plain": [
       "|1>"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "X|Ket(0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "-Z"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "X|StabilizerState(1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is easy to check that $ -Z $ generates the set $ \\{I, -Z\\} $ which clearly stabilizes $ |1\\rangle $.\n",
    "\n",
    "Let's look at a two qubit case:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left|11\\right>$"
      ],
      "text/plain": [
       "|1, 1>"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "CNOT|Ket(1, 0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For the stabilizer we first have to prepare the `StabilizerState` in the state $ |10\\rangle $ by applying $ X $ to the first qubit (in a later version state preparation of this sort will be simplified).  Then we apply the $ \\text{CNOT} $:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "-ZI\n",
       "+ZZ"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "CNOT|(X * I)|StabilizerState(2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The generator $ \\langle-ZI, ZZ\\rangle $ generates the set $ \\{II, -IZ, -ZI, ZZ\\} $, which it is easy to check is the stabilizer group for $ |11\\rangle $.\n",
    "\n",
    "It is also possible to read the `.chp` instruction files used by the original `chp` implementation into a `Circuit` object:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "epr_chp = \"\"\"\\\n",
    "Prepares an EPR pair |00>+|11> across qubits\n",
    "0 and 1, then measures qubit 1.  The outcome\n",
    "should be random.\n",
    "#\n",
    "h 0\n",
    "c 0 1\n",
    "m 1\n",
    "\"\"\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "epr = Circuit.from_chp(epr_chp)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Prepares an EPR pair |00>+|11> across qubits\n",
      "0 and 1, then measures qubit 1.  The outcome\n",
      "should be random.\n"
     ]
    }
   ],
   "source": [
    "print(epr.description)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAJ8AAABHCAIAAACWFAL1AAAABnRSTlMA/wD/AP83WBt9AAAACXBIWXMAAA7EAAAOxAGVKw4bAAAHJ0lEQVR42u1dvW6bXBg+rqJOUQVe2iFqJOhgr2DVawY7QzJVIrmAqsKdshVbVacOFW5vgKJcAY7SpVlsbsAVjrKxgJNGHVIpAlR5qLq4w/uJ8mEbMH+xyXmGCJ8EDpznvD/nxedJaTqdopVBqVRK/Zor9YA5Y2PVbihdMrKYLmuEBwgDs4uB2cUoELvdbheP4Eqz22q1Yp+saZpt23gQV5fd8Xgc+2Se52VZxoNYTM/cbDYVRcGDWNisqtFoDAaDHG7Utu12u02SZKlUOjg4+Pz5s9tI0zQ04jxgDj3TBDAMg+O4aUqAakYAKIqavWGGYRiGiXdBF/1+n2EYhBBFUYqiTAuBOeyKoiiKoiRJmqYJgmBZVvAlEs6P6GRYloUQkiRptlEQhCTs9vt936QvBsF+dhuNhiiKcMzzPEVRoZeQJMk34hmxCzHeMIzZxn6/n4RdsFovojz4mrErSZL3qTiOA69rGIYgCJIkCYLgG9x0zTeYDJ7nCYKYbQw4KyK7c2NWAdj931uETqcDgwVQVVUURYTQ4eGhqqokSSKEWJYdjUZ3UsRXVbVcLrfbbZ8LbTQaWdxDEd5AuGZnGIbXxcFHwzA0TfMaNMMwPjdoGAbP81nbbkDQdUMJjrs+/FsROY6DEKrVaq6hEARB07SmaQRBuH9WLpd9BRBZlr0WnxFUVYW5GNoYb+Hu5swwxQ8ODgq13mVZ1mXRtu1OpwOj5jhOuVz2ngPzwMX5+TnLsjmwC7Ntlt1Uem82m6PRCMy92WwWY7m74XNHnU7HncJgxwRBgAMEeI8RQr1ej+O4HG5UVdVZG53biBFSzQAKNU2bTqe+uEtRlDfuchwXuiBOHnchCVg26C5VzUhyyhrEXd/LH9fjwU/vuyDXcUEj5NLZFSBbrdbu7i5C6OTkBGqN0AhWqyiKL4vGmO+ZF3k8RVFEUaQoajwee18b5JBPkSQJJeXQRoxwz2xZFhQNCIJYVN7LogaZkVe85565xPN8bDuwbTtdt1wqlVL/TuSyF0z9Hu4Qq/UkmN3Ver+LgdnFWKWc+a4wnU7v+e6BIrOL7ve2H+yZMTC7GJhdzC4GZhcDs4uB2cXA7GJgdjG7GJhdDMwuBmYXIyds4CFIiMvLy7dv38Lxnz9/Hj58GO8YIfTkyZObm5u5f/bx40ffN/Uxu3ng4uICIfTy5UuE0GQy2dzchPZljxFCtVoNvmvs+9XFxYUsyzGUBTC7SbG5ufn06dO0Nqcsug7sqcFxd82QqSQUZjcpJpNJktM/fPiQXReY3TvG79+/V9R2sYIQxN3Y545Go62trey6wEqCdxlxr6+vd3d3TdPMynaxkqDXkmDTTa/Xiz5rYwdFkiR3dna+ffvm2/y+KnG3MEqCo9EIogx4MoZhFEXJIe6QJFkul7PbIpt0vQtKgmstRdDr9QiC8G4CpmmapmmQKQzlOEncjegh4sfdhEOz7s7Ztm3LsubOTpIku91uphb8/v37WSG0bG0XnocgiFqtBkoaAa6DpmmfSMp6QZbl4K37FEWZphlQ440eFPf3929vbxFCv379evToEULo9vZ2b28vtIAMXcTYg/NgNpQihNrtdqvVkmX55OTEpbbZbM7N7jiOW9+d8F6xpkWhJ14V0EtqtVqt1+s7OzvD4XA4HOq6DgeiKL57965er1er1VCp3KyUBBVFEQSBIIi5MoIBm/DvydpmkUjB3t5epVJZpGF5enrqHc/nz59XKpVFYmmhOgjhqiidTscrT+SqZ4D0cWjinsp0yxmz+qU+ER3LsoJ1505PT+c+e7Va3dra0nV9Ucp5dXXl9clg09VqdbYC4XrmZfGPXdM0HcdxxVB8HwNgmiZFUWtqdqFJQ6gOJagt+fD48WNd14MD1twqla7rP3782N/fn+0iEyXBKIlJDkqCGSFYNsS2bcdxgtejz549834cDAbb29s/f/6MfUtnZ2cQrRd1ESerWqQkGIp8lASzqydQFDU3o7FtWxTFUDEs32L01atX379/D+33y5cvAWshIPjTp08J17vhSoKhpYB8lASzAyhEdrtdhmEgRpqmqaqq4zhRFrveFVG9Xj8+Po7S6dXV1YsXL4IteHt7+82bN0kqkRu+54THs2379evXPtv1KUS6E6IApWaWZVmWNU2z1+shhMbj8eHh4bIFQnAAEct2UV4NHR8f1+v14XCYciXSqyQI931+fu44DrhrN6nLQUkwT0ABctmzXLd5dHSk63q6TuXo6Cg1zzy7FvLa9GwEWut8KnVEpzY46Ma7ZqRaFQhsyrKsaVpoQqGqamGkjGMjRlD0rXSz6OI/2/UtVUFgM2JlEf8jsXiIEnRTwYMkJeLCRNwkSPIGMOsu8Lfm8sZgMMj0rV94VoWxVFD8+vXr9fU1irajxLvJIOJOk5ubm0qlEuPeSljbLSEuLy/dUnNGO00mk0lw6WMR/gKtxwmMwyeADQAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "C_2 = [[ 0.70710678  0.          0.70710678  0.        ]\n",
       "       [ 0.         -0.70710678  0.         -0.70710678]\n",
       "       [ 0.          0.70710678  0.         -0.70710678]\n",
       "       [-0.70710678  0.          0.70710678  0.        ]]"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "epr"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "+IZ\n",
       "+ZZ"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "epr|StabilizerState(2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Because the outcome of the measurement is random, this may have a different result each time you run it (either $ \\langle IZ, ZZ \\rangle $ or $ \\langle -IZ, ZZ \\rangle $).\n",
    "\n",
    "Measurements on stabilizer states *are* supported, as the above example shows.  But there is currently no side-channel provided by the API to obtain the actual measurement results!  Clearly, this is on the to do list."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
