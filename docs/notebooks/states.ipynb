{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The basic units of information used in PyQC are state vectors representing one or more qubits in the computational basis.  That is, the basis states for a single qubit are $ |0\\rangle = \\bigl(\\begin{smallmatrix} 1 \\\\ 0 \\end{smallmatrix}\\bigr) $ and $ |1\\rangle = \\bigl(\\begin{smallmatrix} 0 \\\\ 1 \\end{smallmatrix}\\bigr) $.  $ n $-qubit states are represented by $ 2^n $ dimensional vectors.  PyQC includes a class called `Ket` used to represent these state vectors.\n",
    "\n",
    "To create a single-qubit state representing the value 0 simply call:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left|0\\right>$"
      ],
      "text/plain": [
       "|0>"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from pyqc import Ket\n",
    "\n",
    "Ket(0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Likewise a single-qubit state representing the value 1 can be created like so:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left|1\\right>$"
      ],
      "text/plain": [
       "|1>"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Ket(1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Multiple qubit states can be created in one of three ways.  The `Ket` constructor can take an arbitrary number of positional arguments.  If all arguments are $ 0 $ or $ 1 $ it treats those arguments as bits and creates the appropriate state to represent that bit string.  For example the state representing the value 5 can be created with:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left|101\\right>$"
      ],
      "text/plain": [
       "|1, 0, 1>"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Ket(1, 0, 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Alternatively, one can provide a single integer, and a state is created that represents that integer in the smallest required number of qubits:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left|5\\right>$"
      ],
      "text/plain": [
       "|5>"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Ket(5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It should be noted that when a `Ket` is instantiated this way, it is represented by the given integer, rather than the corresponding bit string.  There are options to change this, and it is still equivalent to the previous `Ket` instantiated with bits:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Ket(5) == Ket(1, 0, 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To represent an integer (say, 5) in a larger space of more than 3 qubits one can manually specify the n_qubits= argument. When using this option the bit string representation is always used as it is less ambiguous:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left|0101\\right>$"
      ],
      "text/plain": [
       "|0, 1, 0, 1>"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Ket(5, n_qubits=4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That said, you can manually specify the representation format using the `representation=` argument to the constructor.  Currently the only valid formats are `'decimal'` and `'binary'`.  It should be made clear that this only affects display formatting, and has no effect on numerical results.  Note that even in decimal extra qubits are represented with leading zeros:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left|05\\right>$"
      ],
      "text/plain": [
       "|5>"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Ket(5, n_qubits=4, representation='decimal')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For a given `Ket` object we can always introspect it for what decimal value it represents:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "5"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "s = Ket(5, n_qubits=4)\n",
    "s.value"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also check the number of qubits it represents, as well as the dimensionality of its vector space:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "4"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "s.n_qubits"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "16"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "s.dimension"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Superpositions\n",
    "\n",
    "So far we have only seen basis states with definite values.  But PyQC also allows creating superpositions of states using the familiar sum notation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "\\begin{equation}\\frac{\\left|0\\right> + \\left|1\\right>}{\\sqrt{2}}\\end{equation}"
      ],
      "text/plain": [
       "sqrt(1/2)(|0> + |1>)"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Ket(0) + Ket(1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note thate the superposition is normalized automatically so that the magnitudes of the probability amplitudes sum to one.  Therefore any sum of `Ket`s without leading coefficients represents and equal superposition:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "\\begin{equation}\\frac{\\left|00\\right> + \\left|01\\right> + \\left|10\\right>}{\\sqrt{3}}\\end{equation}"
      ],
      "text/plain": [
       "sqrt(1/3)(|0, 0> + |0, 1> + |1, 0>)"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Ket(0, 0) + Ket(0, 1) + Ket(1, 0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But we can also weight the probabilities of individual states using multiplication by scalar amplitudes.  Even in this case the resulting state is always normalized.  For example to give one state twice the probability *amplitude* of another:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "\\begin{equation}0.8944271909999159\\left|01\\right> + 0.4472135954999579\\left|10\\right>\\end{equation}"
      ],
      "text/plain": [
       "0.8944271909999159|0, 1> + 0.4472135954999579|1, 0>"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "2 * Ket(0, 1) + Ket(1, 0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The individual probability amplitudes for each state can be accessed using the `[]` notation similar to Python lists.  In this case the index corresponds to decimal values represented by each state in a $ 2^n $-dimensional vector space where $ n $ in the number of qubits in the state.  The above example, being a superposition of 2-qubit states, supports indicies ranging from 0 to 3:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0.0 0.8944271909999159 0.4472135954999579 0.0\n"
     ]
    }
   ],
   "source": [
    "s = 2 * Ket(0, 1) + Ket(1, 0)\n",
    "print(s[0], s[1], s[2], s[3])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can use this to check that the probabilities are weighted as expected.  Here, the state $ |01\\rangle $ has twice the probability amplitude, so it should be four times more probable than $ |10\\rangle $:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0.7999999999999999\n",
      "0.19999999999999998\n"
     ]
    }
   ],
   "source": [
    "print(s[1] ** 2)\n",
    "print(s[2] ** 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It should also be noted superpositions the `.value` property returns a tuple of all values with non-zero probabilities in that state:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(1, 2)"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "s.value"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can always check if a state is a superposition or basis state using the `.superposition` attribute:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "False"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Ket(0).superposition"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(Ket(0) + Ket(1)).superposition"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Bra Vectors\n",
    "\n",
    "The adjoints of \"ket\" vectors, known in Dirac notation as \"bra\" vectors are also fully supported with the `Bra` class.  They have all the exact same properties and semantics as the `Ket` class:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left<0\\right|$"
      ],
      "text/plain": [
       "<0|"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from pyqc import Bra\n",
    "\n",
    "Bra(0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left<1\\right|$"
      ],
      "text/plain": [
       "<1|"
      ]
     },
     "execution_count": 20,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Bra(1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left<0101\\right|$"
      ],
      "text/plain": [
       "<0, 1, 0, 1|"
      ]
     },
     "execution_count": 21,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Bra(5, n_qubits=4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "...and so on.\n",
    "\n",
    "We can also obtain the corresponding bra vector from a ket vector using the `.adjoint` attribute:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left<0\\right|$"
      ],
      "text/plain": [
       "<0|"
      ]
     },
     "execution_count": 22,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Ket(0).adjoint"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "\\begin{equation}\\frac{\\left<0\\right| + \\left<1\\right|}{\\sqrt{2}}\\end{equation}"
      ],
      "text/plain": [
       "sqrt(1/2)(<0| + <1|)"
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(Ket(0) + Ket(1)).adjoint"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "...and vice-versa:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left|0\\right>$"
      ],
      "text/plain": [
       "|0>"
      ]
     },
     "execution_count": 24,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Bra(0).adjoint"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left<0\\right|$"
      ],
      "text/plain": [
       "<0|"
      ]
     },
     "execution_count": 25,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Bra(0).adjoint.adjoint"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Operators between `Bra` and `Ket` objects are also defined.  As noted in the introduction, PyQC is somewhat unusual in that it uses the `|` operator (normally reserved for the boolean \"or\") rather than the `*` to implement matrix multiplication.  (Note, [PEP-465](http://legacy.python.org/dev/peps/pep-0465/) adding support for a `@` operator in Python for use for matrix multiplication was recently approved, but will not be available until Python version 3.5).\n",
    "\n",
    "This may take some getting used to depending on the previous experience of the user.  That said, it fits in somewhat naturally with Dirac notation, where the inner product is written like $ \\langle 0|0 \\rangle $--two state vectors with a $ | $ between them.  So analogously, in PyQC we can write:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1.0"
      ]
     },
     "execution_count": 26,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Bra(0)|Ket(0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As expected the inner product of a ket vector and its adjoint is 1.  And if we take the inner product of two orthogonal states we get zero:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.0"
      ]
     },
     "execution_count": 27,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Bra(1)|Ket(0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also take the outer product of a bra and ket vector, like $ |0\\rangle\\langle 0| $, the result of which is an *operator*:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$\\hat{O}_{(0)} = \\left(\\begin{array}{*{11}c}1.0 & 0.0\\\\0.0 & 0.0\\\\\\end{array}\\right)$$"
      ],
      "text/plain": [
       "O_0 = [[1. 0.]\n",
       "       [0. 0.]]"
      ]
     },
     "execution_count": 28,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Ket(0)|Bra(0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Operators are discussed in more detail in the next chapter."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Tensor Products\n",
    "\n",
    "As mentioned in the introduction, PyQC currently reserves the `*` operator for the tensor products of states and operators.  While not as directly useful with state vectors as it is with operators, the `*` operator can be used to create higher-dimensional vectors:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left|01\\right>$"
      ],
      "text/plain": [
       "|0, 1>"
      ]
     },
     "execution_count": 29,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Ket(0) * Ket(1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left<01\\right|$"
      ],
      "text/plain": [
       "<0, 1|"
      ]
     },
     "execution_count": 30,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Bra(0) * Bra(1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## QuTiP Interface\n",
    "\n",
    "All `Bra` and `Ket` objects in PyQC have an underlying [`qutip.Qobj`](http://qutip.org/docs/2.2.0/apidoc/classes.html#the-qobj-class) object which provides the numerical implementation of a state.  Most high-level objects in PyQC, be they state vectors, operators, circuits, etc. have an underling `Qobj` that can be accessed via that object's `.qobj` attribute:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "Quantum object: dims = [[2], [1]], shape = (2, 1), type = ket\\begin{equation*}\\left(\\begin{array}{*{11}c}1.0\\\\0.0\\\\\\end{array}\\right)\\end{equation*}"
      ],
      "text/plain": [
       "Quantum object: dims = [[2], [1]], shape = (2, 1), type = ket\n",
       "Qobj data =\n",
       "[[1.]\n",
       " [0.]]"
      ]
     },
     "execution_count": 31,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Ket(0).qobj"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This documentation will not go into the details of working directly with `Qobj`s.  For that, please refer to the excellent [QuTiP documentation](http://qutip.org/docs/2.2.0/index.html).  One point to note, however, is that `Qobj`s make use of SciPy's [sparse matrices](http://docs.scipy.org/doc/scipy/reference/sparse.html) for *their* underlying representation, which can allow for significant memory savings when working in larger Hilbert spaces.  That said, for perfoming actual calculations one is still severly limited by their computer's memory limits."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
