Abstract for Initial Goals of PyQC
==================================

PyQC (name tentative) aims to be a simple, easy to use, and concise Pythonic
interface for constructing and evaluating quantum circuits.

The initial version, at any rate, will be built on top of the powerful
QuTiP_ (Quantum Toolbox in Python) framework.  Although one of the primary
purposes of QuTiP is efficient numerical simulation of the time evolution
of arbitrary Hamiltonians, it is a great starting point for many projects
related to quantum mechanics, including quantum circuit emulation.

This is because QuTiP is built upon a core set of data structures (taking
full advantage of NumPy_) for representing state vectors in Hilbert spaces
(and/or Fock space) and arbitrary quantum operators.  That is, most of the
mathematical details are already carefully implemented in QuTiP--the goal
of PyQC is just to provide a better interface on top of it for defining
quantum circuits.

There are other Python packages that provide basic quantum mechanics
frameworks, but QuTiP is by far one of the best supported and best documented,
and has strong institutional support backing it.  It can be expected to stay
around and be supported for some time to come.


Shortcomings of QuTiP for quantum circuits
------------------------------------------

QuTiP already has the tools needed to represent single-qubit gates and to build
up multi-qubit gates by using tensor products of operators.  However, as it was
not designed explicitly for this purpose the interface is a bit clunky, and
takes a bit of time to get up and running by defining common quantum gates--
the Pauli spin matrices are included out of the box, but not things like SWAP
gates and CNOT gates, or the Hadamard gate.  It's also not straightforward to
define parameterized gates, such as rotation gates that depend on a specific
angle.  There are many other features one would desire for an easy to use
quantum circuit simulator that do not come out of the box with QuTiP.  Below
I will list more of the specific requirements and desirements that PyQC will
attempt to provide.

For illustration purposes, here is an example of how one might use QuTiP to
implement the Deutsch algorithm for a single qubit with an oracle for the
function :math:`f(x) = x`.  That is, the following circuit:

.. image:: latex/figure1.png
    :align: center

First we define operators for the Hadamard gate and the CNOT gate:

.. sourcecode:: ipython

    In [1]: from qutip import *

    In [2]: H = (2 ** -0.5) * Qobj([[1, 1], [1, -1]])

    In [3]: CNOT = tensor(fock_dm(2, 0), qeye(2)) + tensor(fock_dm(2, 1), sigmax())

    In [4]: H
    Out[4]:
    Quantum object: dims = [[2], [2]], shape = [2, 2], type = oper, isherm = True
    Qobj data =
    [[ 0.70710678  0.70710678]
     [ 0.70710678 -0.70710678]]

    In [5]: CNOT
    Out[5]:
    Quantum object: dims = [[2, 2], [2, 2]], shape = [4, 4], type = oper, isherm = True
    Qobj data =
    [[ 1.  0.  0.  0.]
     [ 0.  1.  0.  0.]
     [ 0.  0.  0.  1.]
     [ 0.  0.  1.  0.]]

Then evaluate the circuit on the input :math:`\left|0\right>\left|1\right>`
a la operator notation:

.. sourcecode:: ipython

    In [7]: result = tensor(H, qeye(2)) * CNOT * tensor(H, H) * tensor(basis(2, 0), basis(2, 1))

Then use the `~qutip.expect` function to measure the first qubit in the
computational basis:

.. sourcecode:: ipython

    In [9]: expect(tensor(sigmaz(), qeye(2)), result)
    Out[9]: -1.0000000000000004

This clearly gives the correct answer, and it wasn't terribly difficult, but
still a bit cumbersome.


PyQC outline
------------

The core object around which the initial version of PyQC will be built is a
class called simply `Gate`.  An N-qubit `Gate` instance is built on top of
QuTiP's `~qutip.Qobj` class, but is always a :math:`2^N \times 2^N` unitary
operator.  Many common single qubit, two qubit, and perhaps three qubit gates
(such as the Toffoli gate) will come out of the box.

As these `Gate` objects are built on `~qutip.Qobj` they can be applied to each
other to form new gates, and the tensor product operator can be used to create
higher-dimensional gates.  There is one syntactic difference with `Gate`
objects that I want to experiment with, however.  Rather than using the ``*``
operator for performing a matrix multiplcation between two gates, I instead
want to use pipe operator ``|`` for this purpose.  This is inspired vaguely by
the *bra-ket* notation:
:math:`\left<\psi\right|\hat{A}\hat{B}\left|\psi\right>` except that an
additional bar is inserted between operators applied in succession:
:math:`\left<\psi\right|\hat{A}|\hat{B}\left|\psi\right>`.  In Python code this
would look something like::

    H | X | H

which is equivalent to :math:`\hat{H}\hat{X}\hat{H}`.  I personally find this
more readable, as it's closer to what one writes in an actual equation.  This
frees up the star operator ``*`` to be used for the tensor product operator
[#f1]_.  If this proves too confusing I will try to come up with something else.

There will also be classes called `Bra` and `Ket` which by default create
computational basis vectors.  There will probably be a few different options
for how to use these, time permitting.  But the basic idea is that can write
something like :math:`\left<0\right|\hat{H}\left|0\right>` in Python as::

    Bra(0)|H|Ket(0)

and get back an expectation value.  I want these `Bra` and `Ket` classes to
also allow easy creation of superpositions, though I haven't determined the
syntax of that yet.

Using these constructs, the previous example of the Deutsch algorithm will
look something like:

.. sourcecode:: ipython

    In [1]: circuit = (Z * I)|(H * I)|CNOT|(H * H)

    In [2]: result = circuit|Ket(0, 1)  # Evaluate the circuit on input |01>

    In [3]: result.dag|(Z * I)|result  # Get the expectation value
    Out[3]: -1

This is a bit nicer, but I would like to implement an additional means of
building new gates from a circuit in a way that reads more naturally like a
quantum circuit diagram.  This would be done with a `Gate.from_circuit` method,
where in this case a "circuit" is defined by a list of lists.  Each list
represents a single quantum wire, and each element of the wire lists is a gate
to apply at a given time step.  Currently, each wire must contain the same
number of elements ( otherwise there may be too many ambiguities).  So it is
necessary to fill in identity gates at time steps that don't modify a given
qubit.  The same example using the `Gate.from_circuit` method would look
something like this:

.. sourcecode:: ipython

    In [1]: circuit = Gate.from_circuit([
       ...:     [H, CNOT, H, Z],
       ...:     [H, CNOT, I, I]])

This will build up the circuit by automatically applying the correct
multiplication and tensor product operators.  Note that `CNOT` was listed
twice--once for each input qubit.  By default this will always require that the
first qubit is the control, and the second qubit is the target.  One last
feature worth mentioning is that gates can have labels for qubits.  By default
the labels are just "qubit1", "qubit2", etc.  But when defining `Gate` objects
one can provide custom labels such as "control" and "target" or "ctrl" and
"targ" for short.  Individual labels can be referenced by the syntax
`CNOT.ctrl` and `CNOT.targ`.

Labels can be used in circuit definition to change which qubits are used as
which inputs to the gate:

.. sourcecode:: ipython

    In [1]: circuit = Gate.from_circuit([
       ...:     [H, CNOT.targ, I, I],
       ...:     [H, CNOT.ctrl, H, Z]])

This is the same as the previous example circuit, but with the order of the
qubits reversed.  In this case the same CNOT gate is used, but the appropriate
SWAP gates are automatically inserted before and after the CNOT gate so that
can take inputs in any order.  

There would be a shortcut, then, to evalute the circuit with a given input like
so:

.. sourcecode:: ipython

    In [2]: circuit.evaluate(0, 1)
    Out[2]:
    [[ 0.        ]
     [ 0.        ]
     [ 0.70710678]
     [-0.70710678]]

It will also be possible to define default ancilla qubit values.

An important feature of `Gate` objects will also be that when they are created
from multiple gates (as in the case of a circuit) they retain a memory of what
gates they were composed of.  This may be useful for implementing features
such as automatic generation of LaTeX Qcircuit diagrams.

For the intial version (for my final project) I hope to implement all or most
of what is described here.  Or at least enough that quantum Fourier transforms
can be built up iteratively, and I would like to include a demonstration of
this.

.. [#f1] Of course, it would be great if Python supported custom operator
         characters and allowed us to actually use the unicode :math:`\otimes`
         symbol as an operator.  That's a subject that comes up perenially on
         Python mailing lists and is always shot down, though in large part
         because nobody ever provides an implementation.  I've actually had my
         own idea that I've been kicking around for how to do this, but that
         effort is outside the scope of this project.

.. _QuTiP: http://qutip.org/
.. _NumPy: http://www.numpy.org/
