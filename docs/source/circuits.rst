Quantum Circuits
================

.. notebook:: ../notebooks/circuits.ipynb

Reference/API
-------------

.. currentmodule:: pyqc

Circuit
^^^^^^^

.. autoclass:: Circuit
    :exclude-members: display_latex, from_binary_operation, registry
