Class Demo
==========

What follows is the IPython notebook for an early demo I gave at the end of my
Fall 2013 Quantum Information class.  The examples have been updated to run
under the current version of the code, though the prose has been left intact
some of it may be out of date.

.. notebook:: ../notebooks/other/demo.ipynb
