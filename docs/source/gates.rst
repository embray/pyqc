Quantum Gates
=============

.. notebook:: ../notebooks/gates.ipynb


Reference/API
-------------

.. currentmodule:: pyqc

Built-in Gates
^^^^^^^^^^^^^^

.. autodata:: I
    :annotation:

.. autodata:: X
    :annotation:

.. autodata:: Y
    :annotation:

.. autodata:: Z
    :annotation:

.. autodata:: H
    :annotation:

.. autodata:: S
    :annotation:

.. autodata:: T
    :annotation:

.. autodata:: SWAP
    :annotation:

.. autodata:: CNOT
    :annotation:

.. autodata:: Toffoli
    :annotation:

.. autoclass:: Gate
    :exclude-members: display_latex, from_binary_operation, registry

.. autoclass:: ControlledGate
    :exclude-members: display_latex, from_binary_operation, registry
