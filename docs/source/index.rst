.. PyQC documentation master file, created by
   sphinx-quickstart on Wed Nov 13 13:40:21 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PyQC's documentation!
================================


Introduction
------------

.. notebook:: ../notebooks/intro.ipynb


.. include:: ../../README
    :start-line: 10


Detailed documentation
----------------------

.. toctree::
    :maxdepth: 2

    states
    operators
    gates
    circuits
    stabilizers

Annex
-----

This section links to documents from earlier in PyQC's development, included
here just for completeness.

.. toctree::
    :maxdepth: 1

    abstract
    demo


Indices and tables
------------------

* :ref:`genindex`
* :ref:`search`

