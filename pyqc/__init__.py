import warnings

# Squelch warning from this module which imports the imp module which in
# turn raises a deprecation warning
with warnings.catch_warnings():
    warnings.simplefilter('ignore', 'the imp module is deprecated')
    import qutip.cy.pyxbuilder

from .braket import *
from .gates import *
from .operator import *
from .circuit import *
from .chp import StabilizerState

try:
    import pkg_resources
    __version__ = pkg_resources.get_distribution('PyQC').version
except Exception:
    __version__ = ''
