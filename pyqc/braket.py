from __future__ import unicode_literals

import abc
import copy
import math
import numbers

import qutip
import numpy as np

try:
    from IPython.display import display_latex
    HAVE_IPYTHON = True
except ImportError:
    HAVE_IPYTHON = False

from .operator import OperatorBase, Operator
from .utils import (repr_bin, bit_width, round_exactly_one_or_zero,
                    lazyproperty)


__all__ = ['Bra', 'Ket']


class BraKet(object):
    __metaclass__ = abc.ABCMeta

    # The dual attribute should be filled in by subclasses, and should
    # reference the class representing objects in the dual space to that
    # represented by objects in this class
    #
    # Mainly, Bra.dual = Ket and Ket.dual = Bra, but this allows subclasses to
    # implement similar relationships
    dual = None

    # The abstract BraKet is not associated with any specific type of qobj
    qobj_type = None

    @abc.abstractmethod
    def __init__(self, *args, **kwargs):
        self._n_qubits, self._values, self._representation = \
            self._process_args(*args, **kwargs)
        self._dimension = 2**self._n_qubits
        self._eigenstates = set(self._values)

        # For now the only allowed amplitudes will be 1 and -1, but eventually
        # we can generalize this
        self._amplitudes = [1]
        self._normalized = True

    @classmethod
    def from_qobj(cls, qobj):
        """
        Instantiate a new `Bra` or `Ket` instance from an existing `qutip.Qobj`
        representing a bra/ket vector.

        Parameters
        ----------
        qobj : `~qutip.Qobj`
            An existing `~qutip.Qobj` with ``.type`` either ``'bra'`` or
            ``'ket'`` as appropriate for the class this method is being
            called on.

        Returns
        -------
        braket : `Bra`, `Ket`
            Either a `Bra` or `Ket` depending on the type of the original
            `~qutip.Qobj`.
        """
        if cls.qobj_type is None and qobj.type not in ('bra', 'ket'):
            raise ValueError('A {0} may only be created from a Qobj of '
                             'type=bra or type=ket (got type={1})'.format(
                                 cls.__name__), qobj.type)
        elif cls.qobj_type != qobj.type:
            raise ValueError('A {0} may only be created from a Qobj of '
                             'type={1} (got type={2})'.format(
                                 cls.__name__, cls.qobj_type, qobj.type))

        # Ensure that the input qobj's dimensions are a power of 2
        ndim = max(qobj.shape)

        if not (ndim >= 2 and (ndim & ndim - 1) == 0):
            raise ValueError('A {0} must be created from a Qobj with 2^n '
                             'dimensions for n >= 1 (got n={1})'.format(
                                 cls.__name__, ndim))

        # This is to handle the BraKet.from_qobj() case since we can't
        # instantiate an abstract BraKet directly
        if qobj.type == 'bra' and not issubclass(cls, Bra):
            cls = Bra
        elif qobj.type == 'ket' and not issubclass(cls, Ket):
            cls = Ket

        # Normalize the input Qobj
        qobj = qobj.unit()

        braket = None

        for idx, amplitude in enumerate(qobj.full().flat):
            if amplitude == 0:
                continue

            if braket is None:
                # Note: Don't use numpy.log2; depending on how Numpy was
                # compiled it may use single-precision floating point
                # arithmetic and return an inexact result
                braket = cls(idx, n_qubits=int(math.log(ndim, 2)))
                braket._amplitudes = [amplitude]
                braket._representation = 'decimal'
            else:
                braket._values.append(idx)
                braket._eigenstates.add(idx)
                braket._amplitudes.append(amplitude)

        return braket

    def __eq__(self, other):
        """
        Equality between two states is defined as having the same set of
        eigenstates with the same amplitude on each eigenstate.
        """

        if isinstance(other, type(self)):
            other = other.qobj
        elif isinstance(other, qutip.Qobj):
            pass
        else:
            return False

        return self.qobj == other

    def __neg__(self):
        new_state = copy.copy(self)
        new_state._amplitudes = [-a for a in new_state._amplitudes]
        return new_state

    def __add__(self, other):
        if not ((isinstance(self, type(other)) and
                 isinstance(other, type(self))) or other == 0):
            # Basically, both objects have to be the same type, or at least
            # subclasses of each other's types
            raise TypeError(
                'unsupported operand type(s) for +: {0!r} and {1!r}'.format(
                    self.__class__.__name__, other.__class__.__name__))

        # Adding a state to itself should have no effect; addition with the
        # scalar zero is considered equivalent to addition with the zero-vector
        if self == other or other == 0:
            return self

        new_state = copy.deepcopy(self)
        for val, amp in zip(other._values, other._amplitudes):
            if val not in new_state._eigenstates:
                new_state._eigenstates.add(val)
                new_state._values.append(val)
                new_state._amplitudes.append(amp)
            elif amp == -1:
                # Negating an eigenstate in the existing superposition
                new_state._eigenstates.remove(val)
                new_state._values.remove(val)

        # After a subraction this can result in an empty, or zero state; for
        # now just represent that with the integer zero.
        if not new_state._eigenstates:
            return 0

        # Depending on what coefficients were provided the state *might* be
        # normalized, but we don't actually check.  This flag indicates that we
        # have not explicitly normalized the state
        new_state._normalized = False

        return new_state

    def __radd__(self, other):
        """Allow left-addition by zero."""

        if other == 0:
            return self

        if isinstance(other, numbers.Number):
            raise TypeError(
                "unsupported operand for type(s): {0!r} (other than zero) "
                "and {1!r}".format(other.__class__.__name__,
                                   self.__class__.__name__))
        else:
            raise TypeError(
                "unsupported operand for type(s): {0!r} and "
                "{1!r}".format(other.__class__.__name__,
                               self.__class__.__name__))

    def __sub__(self, other):
        return self.__add__(-other)

    def __mul__(self, other):
        """
        The ``*`` operator is used for tensor products between two Bras or two
        Kets, producing vectors in extended spaces.

        This could also be used between a Bra and a Ket to produce an operator
        but this usage is currently unimplemented.  Use the ``|`` operator
        instead for matrix multiplication between Bras and Kets.
        """

        if isinstance(self, type(other)) and isinstance(other, type(self)):
            new_value = int(repr_bin(self.value, self.n_qubits) +
                            repr_bin(other.value, other.n_qubits), 2)
            n_qubits = self.n_qubits + other.n_qubits
            new_state = self.__class__(new_value, n_qubits=n_qubits)
            new_state._representation = self._representation
            return new_state
        raise TypeError(
            'unsupported operand type(s) for *: {0!r} and {1!r}'.format(
                self.__class__.__name__, other.__class__.__name__))

    def __or__(self, other):
        # At least for now (until and unless Operator|Ket operations return a
        # new Ket object), allow direct multiplication with Qobjs (where
        # applicable)
        if isinstance(other, qutip.Qobj):
            pass
        elif isinstance(other, BraKet):
            if not (isinstance(self, other.dual) and
                    isinstance(other, self.dual)):
                if (isinstance(self, type(other)) and
                        isinstance(other, type(self))):
                    addendum = (' (use * to create a tensor product of two '
                                '{0}s)'.format(self.__class__.__name__))
                else:
                    addendum = ''
                raise TypeError(
                    'unsupported operand type(s) for |: {0!r} and {1!r}'
                    '{2}'.format(self.__class__.__name__,
                                 other.__class__.__name__, addendum))

            other = other.qobj
        else:
            raise TypeError(
                'unsupported operand type(s) for |: {0!r} and {1!r}'.format(
                    self.__class__.__name__, other.__class__.__name__))

        result = self.qobj * other

        if result.shape == (1, 1):
            # This will just return the dirac delta of the two vectors
            return round_exactly_one_or_zero(result.tr())
        elif result.shape[0] == result.shape[1]:
            # Otherwise return a non-scalar operator
            return Operator(result)
        else:
            # This could be a bra times an operator which would return a bra
            # Qobj
            return result

    def __ror__(self, other):
        if isinstance(other, (int, float, complex, np.number)):
            return self._scale(other)
        elif isinstance(other, qutip.Qobj):
            raise TypeError(
                'unsupported operand type(s) for |: {0!r} of type {1!r} and '
                '{2!r}'.format(other.__class__.__name__, other.type,
                               self.__class__.__name__))
        else:
            raise TypeError(
                'unsupported operand type(s) for |: {0!r} and {1!r}'.format(
                    other.__class__.__name__, self.__class__.__name__))

    def __rmul__(self, other):
        # We do allow right-hand "tensor products" with scalars, which is
        # equivalent to simply multiplcation by those scalars.  So 0.5|Ket(0)
        # is the same as 0.5*Ket(0).  The latter works better with Python's
        # order of operations.
        if isinstance(other, (int, float, complex, np.number)):
            return self._scale(other)

        raise TypeError(
            'unsupported operand type(s) for *: {0!r} and {1!r}'.format(
                other.__class__.__name__, self.__class__.__name__))

    def __getitem__(self, idx):
        """
        Used to return the probability amplitude for the n-th value that can
        be represented by this state.  For example, for a single qubit,
        ``ket[0]`` returns the probability amplitude for a value of 0, and
        ``ket[1]`` returns the probability amplitude for a value of 1.  This
        is a shortcut for going through the ``.qobj`` attribute for the same
        result.
        """

        amp = self.qobj[idx].item()
        # Don't bother returning a complex number unless the imaginary
        # component is non-zero
        if not amp.imag:
            return amp.real

        return amp

    def __deepcopy__(self, memo=[]):
        # When deep-copying a state, don't copy the qobj property.

        new_state = self.__class__()

        for key, value in self.__dict__.items():
            if (hasattr(self.__class__, key) and
                    isinstance(getattr(self.__class__, key), lazyproperty)):
                continue

            new_state.__dict__[key] = copy.deepcopy(value, memo)

        return new_state


    def __repr__(self):
        if not self.superposition:
            # For single states do not display any ampltude but the sign; this
            # might get confusing though so maybe consider displaying the
            # amplitude after all...not sure how I feel about that yet
            return self._repr_state_with_amplitude(
                self.value, np.sign(self._amplitudes[0]),
                self._repr_eigenstate)

        summ = self._repr_superposition(self._repr_eigenstate)

        if self._equal_superposition:
            return 'sqrt(1/{0})({1})'.format(len(self._values), summ)
        else:
            # Inequal superpositions have their amplitudes attached to each
            # basis state by _repr_superposition
            return summ

    def _repr_latex_(self):
        """Returns the LaTeX representation of input bits only; the Bra and Ket
        subclasses should wrap this in the appropriate symbols.
        """

        if not self.superposition:
            return '${0}$'.format(
                self._repr_state_with_amplitude(
                    self.value, np.sign(self._amplitudes[0]),
                    self._repr_latex_eigenstate))

        summ = self._repr_superposition(self._repr_latex_eigenstate)

        if self._equal_superposition:
            if len(self.value) > 4:
                # Put the fraction out front; too many values in the numerator
                # look silly otherwise
                frac_format = r'\frac{{1}}{{\sqrt{{{1}}}}}\left({0}\right)'
            else:
                # Combine the kets and the normalization in a single fraction
                frac_format = r'\frac{{{0}}}{{\sqrt{{{1}}}}}'

            frac = frac_format.format(summ, len(self._values))
        else:
            frac = summ
        return r'\begin{{equation}}{0}\end{{equation}}'.format(frac)

    @lazyproperty
    def qobj(self):
        """The representation of this state as a `qutip.Qobj`."""

        if not self.superposition:
            qobj = self._amplitudes[0] * self._qobj_eigenstate(self.value)
        else:
            qobj = sum(amp * self._qobj_eigenstate(val)
                       for val, amp in zip(self._values, self._amplitudes))

        return qobj.unit()

    @property
    def value(self):
        """
        The integer value(s) represented by this state.

        For basis states this is a single integer value.  For superpositions
        this is a tuple consisting of the values of all the states in the
        superposition with non-zero amplitude.
        """

        if len(self._values) == 1:
            return self._values[0]
        return tuple(self._values)

    @property
    def dimension(self):
        """
        The dimension of this state's Hilbert space.

        Equivalent to :math:`2^n` where :math:`n` is the number of qubits
        represented by this state.
        """

        return self._dimension

    @property
    def n_qubits(self):
        """The number of qubits represented by this state."""

        return self._n_qubits

    @property
    def superposition(self):
        """
        Boolean value representing whether or not the state is in
        superposition.
        """

        return len(self._values) > 1

    @property
    def adjoint(self):
        """
        Returns the adjoint of a bra or ket.
        """

        if not self.superposition:
            return self.dual(self.value, n_qubits=self.n_qubits,
                             representation=self._representation)

        return sum(amp.conjugate() *
                   self.dual(value, n_qubits=self.n_qubits,
                             representation=self._representation)
                   for amp, value in zip(self._amplitudes, self.value))

    @property
    def _equal_superposition(self):
        """
        Note: This doesn't imply an equal superposition of all possible values
        of n qubits.  Rather it just implies that all states in the
        superposition have the same amplitude.
        """

        if not self.superposition:
            return False

        return all(abs(amp) == abs(self._amplitudes[0])
                   for amp in self._amplitudes[1:])

    if HAVE_IPYTHON:
        def display_latex(self):
            display_latex(self)

    def _process_args(self, *args, **kwargs):
        n_qubits = kwargs.pop('n_qubits', None)
        representation = kwargs.pop('representation', None)

        if len(args) == 0:
            # Default to zero-state
            args = [0]

        if len(args) == 1:
            n = args[0]
            n_bits = bit_width(n)
            value = [n]
            if n in (0, 1):
                default_representation = 'binary'
            else:
                default_representation = 'decimal'
        else:
            # If given a list of binary digits create a tensor
            # product of qubits
            try:
                n = int(''.join(str(a) for a in args), 2)
            except ValueError:
                raise ValueError(
                    '{0} arguments must be a single decimal value '
                    'or 1 or more binary digits'.format(
                        self.__class__.__name__))
            n_bits = len(args)
            value = [n]
            default_representation = 'binary'

        if n_qubits is not None:
            if not isinstance(n_qubits, int):
                raise TypeError('n_qubits must be a positive integer')
            elif n_qubits < 1:
                raise ValueError('n_qubits must be a positive integer')
            if n_qubits < n_bits:
                raise ValueError(
                    'Input value {0} requires more qubits than the specified '
                    'n_qubits={1}'.format(value[0], n_qubits))
            elif n_qubits > n_bits:
                n_bits = n_qubits
                # If the requested number of qubits is greater than the minimum
                # required for an integer's representation, switch to binary
                # representation (since otherwise the leading zeros will be
                # obscured)
                default_representation = 'binary'

        if representation is None:
            representation = default_representation
        else:
            representation = representation.lower()
            if representation not in ('binary', 'decimal'):
                raise ValueError(
                    'Unknown representation format {0!r}; the valid formats '
                    'are "decimal" or "binary"'.format(representation))

        return n_bits, value, representation

    def _scale(self, coeff):
        """
        Return a new BraKet with an internal scalar coefficient--the bra/ket is
        still treated as normalized, but this can be used to create
        superpositions.
        """

        if coeff == 0:
            return 0

        new_state = copy.copy(self)
        new_state._amplitudes = [a * coeff for a in new_state._amplitudes]
        return new_state

    def _normalize(self):
        """Normalizes the total probability to 1 if not already normalized."""

        if self._normalized:
            return

        norm = np.linalg.norm(self._amplitudes)
        self._amplitudes = list(np.array(self._amplitudes) / norm)
        self._normalized = True

    def _repr_state_with_amplitude(self, value, amplitude, repr_eigenstate):
        """
        Represent a single state with an amplitude.  The ``repr_eigenstate``
        method given is used to represent the state vector itself.
        """

        sign = np.sign(amplitude).real
        amplitude = amplitude * sign
        sign = '-' if sign < 0 else ''
        ramp = amplitude.real
        iamp = amplitude.imag
        rsign = np.sign(ramp)
        isign = np.sign(iamp)
        isign = '-' if isign < 0 else '+'

        if iamp == 0:
            if ramp == 1:
                amplitude = ''
            else:
                amplitude = ramp
        elif ramp == 0:
            if iamp == 1:
                amplitude = 'i'
            else:
                amplitude = f'{iamp}i'
        elif iamp == 1:
            amplitude = f'({ramp}{iamp}i)'
        else:
            amplitude = f'({ramp}{iamp}i)'
        return '{0}{1}{2}'.format(sign, amplitude, repr_eigenstate(value))

    def _repr_superposition(self, repr_eigenstate):
        """
        Represent a superposition of states, using the method passed in as
        ``repr_eigenstate`` for representing individual eigenstates in the
        superposition.
        """

        # Ensure the probability amplitudes are normalized before displaying
        self._normalize()

        equal = self._equal_superposition

        if equal:
            amp = abs(self._amplitudes[0])
            amplitudes = [a / amp for a in self._amplitudes]
        else:
            amplitudes = self._amplitudes

        summ = [self._repr_state_with_amplitude(self._values[0], amplitudes[0],
                                                repr_eigenstate)]
        for val, amp in zip(self._values[1:], amplitudes[1:]):
            oper = '-' if np.sign(amp) < 0 else '+'
            state = self._repr_state_with_amplitude(val, amp * np.sign(amp),
                                                    repr_eigenstate)
            summ.append(' {0} {1}'.format(oper, state))

        return ''.join(summ)

    def _repr_eigenstate(self, value):
        if self._representation == 'binary':
            args_repr = ', '.join(repr_bin(value, self.n_qubits))
        elif self._representation == 'decimal':
            args_repr = str(value)

        # TODO: Use _repr_format or _repr_format_unicode depending on whether
        # unicode support is enabled
        return self._repr_format.format(args_repr)

    def _repr_latex_eigenstate(self, value):
        """
        Returns the LaTeX representation of input bits only; the Bra and Ket
        subclasses should wrap this in the appropriate symbols.
        """
        if self._representation == 'binary':
            return repr_bin(value, self.n_qubits)
        elif self._representation == 'decimal':
            n = bit_width(value)
            prefix = '0' * (self._n_qubits -n)
            return prefix + str(value)


class Ket(BraKet):
    """
    Class representing a ket vector of 1 or more qubits in the computational
    basis.

    With no arguments this returns a 1-qubit ket representing the value
    zero::

        >>> Ket()
        |0>

    By default the first argument represents a decimal value that is
    represented by this `Ket`.  If the value is 0 or 1 this could also be
    seen as the binary value of a single qubit::

        >>> Ket(0)
        |0>
        >>> Ket(1)
        |1>
        >>> Ket(5)
        |5>

    A `Ket` can also be instantiated with an arbitrary number of arguments,
    representing its value as a bit string::

        >>> Ket(1, 0, 1)
        |1, 0, 1>

    This is equivalent to the previous example of ``Ket(5)``, but with the
    value given in binary instead::

        >>> Ket(1, 0, 1) == Ket(5)
        True

    Superpositions can be created by adding `Ket` objects.  Superpositions are
    always automatically normalized::

        >>> Ket(0) + Ket(1)
        sqrt(1/2)(|0> + |1>)

    The individual probability amplitudes of states in a superposition can be
    accessed using Python's square bracket notation::

        >>> s = Ket(0) + Ket(1)
        >>> s[0]
        0.7071067811865475

    See the PyQC documentation for more details on using `Ket` objects.  In
    addition to the above positional arguments, the following keyword arguments
    are supported:

    Parameters
    ----------
    n_qubits : `int`
        The number of qubits represented by this `Ket`.  Must be at least the
        minimum number of bits required to represent the integer value
        represented by this `Ket`.

    representation : `str`
        The print representation to use when displaying this `Ket`.  Must be
        either ``'decimal'`` or ``'binary'``.
    """

    qobj_type = 'ket'

    _repr_format = '|{0}>'
    _repr_format_unicode = \
        '\N{LIGHT VERTICAL BAR}{0}\N{MATHEMATICAL RIGHT ANGLE BRACKET}'

    def __init__(self, *args, **kwargs):
        super(Ket, self).__init__(*args, **kwargs)

    def __ror__(self, other):
        # Allow a Ket to be operated on from right right by a Qobj
        # TODO: This is a little hacky; operations like this should be factored
        # out into a class method, perhaps, around which BraKet.__or__ as well
        # as this method could just be a wrapper
        if isinstance(other, OperatorBase):
            result = other.qobj * self.qobj
        elif isinstance(other, qutip.Qobj):
            try:
                result = other * self.qobj
            except TypeError:
                # We will reraise a TypeError below if the operation didn't
                # succeed due to incompatible types
                pass
            else:
                if result.shape == (1, 1):
                    result = round_exactly_one_or_zero(result.tr())
        else:
            result = super(Ket, self).__ror__(other)

        if isinstance(result, qutip.Qobj):
            result = self.__class__.from_qobj(result)

        if isinstance(result, BraKet):
            result._representation = self._representation

        return result

    def _qobj_eigenstate(self, value):
        state =  qutip.basis(self.dimension, value)
        # Set the correct tensor dims; this is equivalent to the result of
        # tensor(basis(2, q_0), basis(2, q_1), ..., basis(2, q_n)) for n
        # qubits
        state.dims = [[2] * self.n_qubits, [1] * self.n_qubits]
        return state

    def _repr_latex_eigenstate(self, value):
        args_repr = super(Ket, self)._repr_latex_eigenstate(value)
        return r'\left|{0}\right>'.format(args_repr)


class Bra(BraKet):
    """
    A member of the dual space of `Ket` vectors of the same number of qubits.

    All the semantics of `Bra` are the same as the `Ket` class.  See the PyQC
    documentation for more details.
    """

    qobj_type = 'bra'

    _repr_format = '<{0}|'
    _repr_format_unicode = \
        '\N{MATHEMATICAL LEFT ANGLE BRACKET}{0}\N{LIGHT VERTICAL BAR}'

    def __init__(self, *args, **kwargs):
        super(Bra, self).__init__(*args, **kwargs)

    def __or__(self, other):
        # Allow operation on an operator by a bra vector
        if isinstance(other, OperatorBase):
            other = other.qobj

        return super(Bra, self).__or__(other)

    def _qobj_eigenstate(self, value):
        state = qutip.basis(self.dimension, value).dag()
        state.dims = [[1] * self.n_qubits, [2] * self.n_qubits]
        return state

    def _repr_latex_eigenstate(self, value):
        args_repr = super(Bra, self)._repr_latex_eigenstate(value)
        return r'\left<{0}\right|'.format(args_repr)

# Define the Bra and Ket classes as duals of each other
Ket.dual = Bra
Bra.dual = Ket
