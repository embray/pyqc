import numpy as np
cimport numpy as np
cimport cython

from numpy.random import randint

from .circuit import Circuit
from .operator import GateBase
from .gates import H, S, CNOT, I, X, Y, Z, M


ctypedef np.uint64_t uint64
ctypedef np.uint_t uint
ctypedef np.uint8_t uint8


cdef class StabilizerState:
    cdef uint       n          # number of qubits
    cdef np.ndarray x          # the array of x bits
    cdef np.ndarray z          # the array of z bits
    cdef np.ndarray r          # the array of phase bits


    def __init__(object self, uint n):
        assert n >= 1

        cdef uint row
        cdef uint64 f = ((n - 1) // 64) + 1
        cdef np.ndarray[uint64, ndim=2] x = np.zeros((2 * n + 1, f),
                                                     dtype=np.uint64)
        cdef np.ndarray[uint64, ndim=2] z = np.zeros((2 * n + 1, f),
                                                     dtype=np.uint64)
        cdef np.ndarray[uint8] r = np.zeros(2 * n + 1, dtype=np.uint8)

        for row in range(n):
            x[row, row >> 6] = <uint64>1 << (row & 63)
            z[row + n, row >> 6] = <uint64>1 << (row & 63)

        self.n = n
        self.x = x
        self.z = z
        self.r = r

    property x:
        def __get__(self):
            return self.x

    property z:
        def __get__(self):
            return self.z

    property r:
        def __get__(self):
            return self.r

    property n_qubits:
        def __get__(self):
            return self.n

    def __repr__(self):
        cdef uint64 mask, seg
        cdef uint i, j

        codes = []
        for i in range(self.n, self.n * 2):
            code = []
            r = self.r[i]
            if r == 0 or r == 1:
                code.append('+')
            elif r == 2 or r == 3:
                code.append('-')

            for j in range(self.n):
                mask = <uint64>1 << (j & 63)
                seg = j >> 6
                x_ij = <uint64>self.x[i, seg] & mask
                z_ij = <uint64>self.z[i, seg] & mask

                if x_ij and z_ij:
                    code.append('Y')
                elif x_ij and not z_ij:
                    code.append('X')
                elif not x_ij and z_ij:
                    code.append('Z')
                else:
                    code.append('I')

            codes.append(''.join(code))

        return '\n'.join(codes)

    def __or__(object a, object b):
        # Note: Cython doesn't support a separate __ror__ special method;
        # instead the __or__ on this class is called if the __or__ on the
        # left-hand object does not apply; it cannot be guaranteed that the
        # first argument is self
        if not isinstance(b, StabilizerState):
            raise TypeError(
                'unsupported operand type(s) for |: {0!r} and {1!r}'.format(
                    a.__class__.__name__, b.__class__.__name__))

        if isinstance(a, Circuit):
            instructions = a._instructions
        elif isinstance(a, GateBase):
            instructions = Circuit.from_gate(a)._instructions
        else:
            raise TypeError(
                'unsupported operand type(s) for |: {0!r} and {1!r}'.format(
                    a.__class__.__name__, b.__class__.__name__))

        # Ensure that all single-argument instructions give their args as an
        # int and not a tuple
        for idx, instruction in enumerate(instructions):
            opcode, args = instruction
            if isinstance(args, tuple) and len(args) == 1:
                instructions[idx] = (opcode, args[0])

        # Applying an operator returns a new StabilizerState (rather than
        # modifying the current one in-place)
        cdef StabilizerState new_state
        new_state = StabilizerState(b.n_qubits)
        new_state.x = b.x.copy()
        new_state.z = b.z.copy()
        new_state.r = b.r.copy()
        new_state._apply_instructions(instructions)
        return new_state

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def _apply_instructions(object self, instructions):
        cdef uint64[:, ::1] x = self.x
        cdef uint64[:, ::1] z = self.z
        cdef uint8[::1] r = self.r
        cdef uint n = self.n

        for opcode, args in instructions:
            if opcode is S:
                apply_phase(x, z, r, n, args)
            elif opcode is H:
                apply_hadamard(x, z, r, n, args)
            elif opcode is M:
                apply_measurement(x, z, r, n, args)
            elif opcode is X:
                apply_x(x, z, r, n, args)
            elif opcode is Y:
                apply_y(x, z, r, n, args)
            elif opcode is Z:
                apply_z(x, z, r, n, args)
            elif opcode is CNOT:
                apply_cnot(x, z, r, n, args[0], args[1])


@cython.boundscheck(False)
@cython.wraparound(False)
cdef inline apply_phase(uint64[:, ::1] x, uint64[:, ::1] z,
                        uint8[::1] r, uint n, uint64 a):
    cdef uint row
    cdef uint64 x_ia, z_ia
    cdef uint64 mask = <uint64>1 << (a & 63)
    cdef uint64 seg = a >> 6

    for row in range(2 * n):
        x_ia = x[row, seg] & mask
        z_ia = z[row, seg] & mask
        if x_ia and z_ia:
            r[row] = (r[row] + 2) % 4
        z[row, seg] ^= x_ia


@cython.boundscheck(False)
@cython.wraparound(False)
cdef inline apply_hadamard(uint64[:, ::1] x, uint64[:, ::1] z,
                           uint8[::1] r, uint n, uint64 a):
    cdef uint64 x_ia, z_ia
    cdef uint row
    cdef uint64 mask = <uint64>1 << (a & 63)
    cdef uint64 seg = a >> 6

    for row in range(2 * n):
        x_ia = x[row, seg] & mask
        z_ia = z[row, seg] & mask
        x[row, seg] ^= z_ia
        z[row, seg] ^= x[row, seg] & mask
        x[row, seg] ^= z[row, seg] & mask
        if x_ia and z_ia:
            r[row] = (r[row] + 2) % 4


@cython.boundscheck(False)
@cython.wraparound(False)
cdef inline apply_cnot(uint64[:, ::1]x, uint64[:, ::1] z, uint8[::1] r,
                       uint n, uint64 a, uint64 b):
    cdef uint64 x_ia, x_ib, z_ia, z_ib, c
    # 'c' is used for x_ib ^ z_ia
    cdef uint row
    cdef uint64 amask = <uint64>1 << (a & 63)
    cdef uint64 bmask = <uint64>1 << (b & 63)
    cdef uint64 aseg = a >> 6
    cdef uint64 bseg = b >> 6

    for row in range(2 * n):
        x_ia = x[row, aseg] & amask
        z_ib = z[row, bseg] & bmask

        if x_ia:
            x[row, aseg] ^= bmask

        if z_ib:
            z[row, bseg] ^= amask

        c = (x[row, bseg] & bmask) ^ (z[row, aseg] & amask)

        if x_ia and z_ib and not c:
            r[row] ^= <uint64>1


@cython.boundscheck(False)
@cython.wraparound(False)
cdef inline apply_measurement(uint64[:, ::1]x, uint64[:, ::1] z,
                              uint8[::1] r, uint n, uint64 a):
    cdef uint row, rand, p
    cdef uint8 result
    cdef uint64 mask = <uint64>1 << (a & 63)
    cdef uint64 seg = a >> 6

    for row in range(n, 2 * n):
        if x[row, seg] & mask:
            rand = True
            p = row
            break
    else:
        rand = False

    if rand:
        for row in range(2 * n):
            if row != p and (x[row, seg] & mask):
                rowsum(row, p, x, z, r, n)
        x[p - n] = x[p]
        z[p - n] = z[p]
        r[p - n] = r[p]

        x[p] = 0
        z[p] = 0
        z[p, seg] ^= mask
        result = <uint8>randint(0, 2)
        r[p] = result * 2
        return False, result
    else:
        x[2 * n] = 0
        z[2 * n] = 0
        r[2 * n] = 0
        for row in range(n):
            if x[row, seg] & mask:
                rowsum(2 * n, row + n, x, z, r, n)
        result = r[2 * n] // 2
        return True, result


@cython.boundscheck(False)
@cython.wraparound(False)
cdef inline uint rowmult(uint h, uint i, uint64[:, ::1] x,
                         uint64[:, ::1] z, uint n):
    #"""
    #This computes the function referred to as g(x_1, z_1, x_2, z_2) that
    #computes the phase that results from multiplying the Pauli operators
    #represented by (x_ij, z_ij) and (x_hj, z_hj), summed over all bits in
    #rows i and h.
    #"""

    cdef uint j
    cdef uint64 mask, seg
    cdef int g = 0

    for j in range(n):
        seg = j >> 6
        mask = <uint64>1 << (j & 63)
        x_1 = x[i, seg] & mask
        z_1 = z[i, seg] & mask

        if not (x_1 and z_1):
            continue

        x_2 = x[h, seg] & mask
        z_2 = z[h, seg] & mask

        if x_1 and z_1:
            if not x_2 and z_2:
                g += 1
            elif x_2 and not z_2:
                g -= 1
        elif x_1 and not z_1:
            if x_2 and z_2:
                g += 1
            elif not x_2 and z_2:
                g -= 1
        elif not x_1 and z_1:
            if x_2 and z_2:
                g -= 1
            elif x_2 and not z_2:
                g += 1

    return g


@cython.boundscheck(False)
@cython.wraparound(False)
cdef inline void rowsum(uint h, uint i, uint64[:, ::1] x,
                        uint64[:, ::1] z, uint8[::1] r, uint n):
    cdef int g, j

    g = r[h] + r[i] + rowmult(h, i, x, z, n)

    if g % 4:
        r[h] = 2
    else:
        r[h] = 0

    for j in range(x.shape[1]):
        x[h, j] ^= x[i, j]
        z[h, j] ^= z[i, j]


@cython.boundscheck(False)
@cython.wraparound(False)
cdef inline apply_x(uint64[:, ::1] x, uint64[:, ::1] z,
                    uint8[::1] r, uint n, uint64 a):
    """Apply a Pauli X gate as HZH"""

    apply_hadamard(x, z, r, n, a)
    apply_z(x, z, r, n, a)
    apply_hadamard(x, z, r, n, a)


@cython.boundscheck(False)
@cython.wraparound(False)
cdef inline apply_y(uint64[:, ::1] x, uint64[:, ::1] z,
                    uint8[::1] r, uint n, uint64 a):
    """Apply a Pauli Y gate as PXZP"""

    apply_phase(x, z, r, n, a)
    apply_x(x, z, r, n, a)
    apply_z(x, z, r, n, a)
    apply_phase(x, z, r, n, a)


@cython.boundscheck(False)
@cython.wraparound(False)
cdef inline apply_z(uint64[:, ::1] x, uint64[:, ::1] z,
                    uint8[::1] r, uint n, uint64 a):
    """Apply a Pauli Z gate as PP"""

    apply_phase(x, z, r, n, a)
    apply_phase(x, z, r, n, a)
