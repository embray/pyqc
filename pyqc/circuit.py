from __future__ import absolute_import

import abc
import itertools
import operator
import re
import warnings

import subprocess as sp

from collections import OrderedDict, namedtuple
from functools import reduce

from .operator import _CompoundGate, _InputLabel, GateBase
from .gates import *
from .gates import (IdentityGate, SwapGate, CnotGate, ToffoliGate,
                    MeasurementGate, _get_default_gate_inputs)
from .utils import (ExpressionTree, latex_to_png_dvipng, FindCmdError,
                    lazyproperty)
from .vendor import qasm2circ
from .vendor.qasm2circ.qasm2tex import qgate as _QGate
from .vendor.qasm2circ.qasm2tex import qcircuit as _QCircuit
from .vendor.qasm2circ.qasm2tex import qasm_parser as _QasmParser
from .vendor.qasm2circ.qasm2tex import GateMasterDef as _qgatedefs
# NOTE: GateMasterDef sounds like some kind of hip-hop artist IMO...


__all__ = ['Circuit']


class CircuitDefinitionError(Exception):
    """Raised when an illegal circuit definition is given."""


# TODO: I did not originally want to have a Circuit class separate from the
# Gate class; unfortunately the way the Gate class is currently structured made
# that difficult.  If I were to do it over again I would design Gate to be more
# inherently circuit-oriented rather than expression-oriented.  But there's not
# enough time right now for a redesign.
class Circuit(GateBase):
    """
    Special type of `Gate` representing a quantum circuit.

    As this is a subclass of `Gate` it has all the same capabilities as the
    `Gate` and `Operator` classes.  It can even be used to define complex gates
    that can be used as subroutines in larger circuits.

    The default input to a `Circuit` is a `list` (or other iterable) of
    instructions performed by the circuit, given as tuples.  See the full PyQC
    documentation for more details on this format.

    A few other alternative instantiation methods are provided, including:

        * `Circuit.from_qasm`
        * `Circuit.from_chp`
        * `Circuit.from_wires`
        * `Circuit.from_gate`

    Others will be provided in future releases.

    The other feature that sets the `Circuit` class apart from the basic `Gate`
    class is that it can display its component instructions as a circuit
    diagram.

    Parameters
    ----------
    instructions : iterable
        The list of instructions to build this circuit out of.  Each
        instruction is a tuple consisting of a `Gate`, and the argument or
        arguments to that gate given as qubit numbers.

    The remaining parameters are the same as those for `Operator`.

    Examples
    --------

    The following circuit implements a SWAP gate (already provided by PyQC as
    `pyqc.SWAP`)::

        >>> SWAP = Circuit([
        ...     (CNOT, (0, 1)),
        ...     (CNOT, (1, 0)),
        ...     (CNOT, (0, 1))], name='SWAP')
        ...
        >>> SWAP|Ket(0, 1)
        |1, 0>

    """

    _default_name_base = 'C'

    def __init__(self, instructions, name=None, latex_name=None,
                 description='', representation='circuit', labels=None,
                 register=False, replace=False):
        self._instructions, self._n_qubits = \
                self._normalize_instructions(instructions)
        self._description = description

        # Initialize with an empty expression tree--the expression tree will be
        # generated from the instructions on an as-needed basis
        super(Circuit, self).__init__(name=name, latex_name=latex_name,
                                      representation=representation,
                                      labels=labels, register=register,
                                      replace=replace)

    def __repr__(self):
        # If the circuit contains placeholder gates we can't actually implement
        # the usual text represention since we can't compute the matrix
        # elements.  So in that case just display the name
        if any(isinstance(inst[0], _PlaceholderGateBase)
               for inst in self._instructions):
            return self.format_text(representation='name')
        else:
            return super(Circuit, self).__repr__()

    def _repr_latex_(self):
        # Raise NotImplementedError to skip LaTeX repr for Circuits, which
        # isn't terribly useful; if one wants to display the circuit as an
        # expression they can still call format_latex directly
        raise NotImplementedError

    def _repr_png_(self):
        return self.format_png()

    @property
    def n_qubits(self):
        """The number of qubits this operator acts on."""

        return self._n_qubits

    @property
    def qobj(self):
        """Returns the `qutip.Qobj` used to evaluate this circuit as a quantum
        operator.

        This ensures that concrete instances of all placeholder gates have been
        defined and registered in the `Gate` registry before evaluating the
        circuit.  Otherwise a `NotImplementedError` is raised.
        """

        # Create a _CompoundGate with an expression tree generated from the
        # instruction list, and return its Qobj
        self._resolve_placeholders()
        tree = self._tree_from_instructions(self._instructions)
        return _CompoundGate(tree).qobj

    @property
    def description(self):
        """
        Text description of this circuit and its purpose.  Currently used only
        for informational purposes.
        """

        return self._description

    def format_text(self, representation=None): #  , unicode=False):
        """
        Return a plain-text expression for this circuit.

        Used primarily for displaying circuits in the Python command-line.
        The current default when displaying circuits in plain-text is to
        convert the circuit to an algebraic expression.

        Other, possibly more useful formats are planned.
        """

        # TODO: Unicode support
        if representation is None:
            representation = self._representation

        if representation == 'circuit':
            representation = 'expression'

        return super(Circuit, self).format_text(representation=representation)
                                                # ,unicode=unicode)

    def format_latex(self, representation=None):
        """
        Return a LaTeX expression for this circuit.

        Parameters
        ----------
        representation : `str`
            Same as the ``representation`` argument to the `Operator`
            constructor.  This can be used to output the LaTeX formatting
            for this operator in different representations.

        Notes
        -----
        The default LaTeX representation for `Circuit` objects is to generate
        the LaTeX for its circuit diagram representation.  Currently the only
        other supported LaTeX outputs are equivalent to those supported by
        `Operator`, where the circuit is treated as an algebraic operator
        expression.
        """

        if representation is None:
            representation = self._representation

        if representation == 'circuit':
            return self._format_latex_qasm2circ()

        return super(Circuit, self).format_latex(representation=representation)

    # TODO: Add a test for this; this is currently not covered by the test
    # suite
    # Could use matplotlib to check output image against known good output
    # TODO: When displaying Circuits in the notebook, ensure required programs
    # like latex and dvipng are usable; if not display a fallback, and perhaps
    # a warning
    def format_png(self):
        """
        Returns the raw bytes of the PNG image for a circuit diagram.

        Currently the image is created by running the ``dvi2png`` program
        after running ``latex`` on the output from
        ``Circuit.format_latex(representation='circuit')``.
        """

        try:
            png_bytes = latex_to_png_dvipng(self._format_latex_qasm2circ(),
                                            qasm2circ.get_package_data())
        except (sp.CalledProcessError, FindCmdError) as e:
            raise NotImplementedError(
                '{0}\nA modern LaTeX distribution including the dvipng '
                'program and packages like Xy-pic are required for rendering '
                'circuit diagrams')

        return png_bytes

    @classmethod
    def from_qasm(cls, qasm):
        """
        Creates a new `Circuit` object from the instructions read in from a
        QASM file as used by the `qasm2circ
        <http://www.media.mit.edu/quanta/qasm2circ/>`_ program.

        Parameters
        ----------

        qasm : `str`
            A string containing the entire contents of a QASM file.

        Examples
        --------
        To create a `Circuit` from a QASM file on disk, simply open the file
        and read its entire contents into this method::

            >>> with open('example.qasm', 'r') as f:
            ...     circ = Circuit.from_qasm(f.read())

        Notes
        -----
        Not all arbitrary QASM files will currently work seamlessly.  QASM
        files that define custom operators require those operators to be
        implemented in PyQC in order for the resultant `Circuit` to be
        evaluatable.  See the full PyQC documentation for more details.
        """

        orig_gatemasterdef = dict(_qgatedefs)

        try:
            qp = _QasmParser(qasm.splitlines())
        finally:
            _qgatedefs.clear()
            _qgatedefs.update(orig_gatemasterdef)

        # This maps the gate names in qasm (mapped in GateMasterDef) to
        # Gate objects in pyqc; we have to sort of figure out which gates to
        # map to as we go
        gate_mapping = {}
        qubit_mapping = {}
        instructions = []
        placeholder_gates = []

        for idx, qubit in enumerate(qp.nametab):
            qubit_mapping[qubit] = idx

        for qgate in qp.gatetab:
            if qgate.name in gate_mapping:
                gate = gate_mapping[qgate.name]
            else:
                gate = _qgate_to_gate(qgate)
                gate_mapping[qgate.name] = gate
                if isinstance(gate, _PlaceholderGateBase):
                    placeholder_gates.append(gate.name)

            inputs = tuple(qubit_mapping[qbit] for qbit in qgate.qubits)
            instructions.append((gate, inputs))

        if placeholder_gates:
            placeholders = '\n    '.join(placeholder_gates)
            name = placeholder_gates[0]
            warnings.warn(
                f"Input QASM defines the following gates unrecognized by "
                f"pyqc:\n\n"
                f"    {placeholders}\n\n"
                f"Until operations are defined for these gates the circuit "
                f"cannot be evaluated.  You can register these gates using "
                f"the register=True argument when creating "
                f"a new gate object.  For example:\n\n"
                f"    >>> G = Gate(<..matrix elements..>, name={name},\n"
                f"    ...          register=True)\n\n"
                f"The diagram for this circuit can still be displayed, but "
                f"the circuit cannot be evaluated until the unknown gates "
                f"are registered with pyqc.")

        inst = cls(instructions)
        return inst

    def to_qasm(self, instruction_map={}):
        """
        Outputs the text of a valid QASM file as used by the `qasm2circ
        <http://www.media.mit.edu/quanta/qasm2circ/>`_ program, generated from
        the instructions in this `Circuit`.

        Parameters
        ----------
        instruction_map : `dict` (optional)
            When a circuit contains gates that do not have corresponding
            pre-defined instructions in QASM, new instructions need to be
            defined for those gates using either the ``def`` or ``defbox``
            commands.  By default, this method will auto-generate names for
            those new instructions, but argument can be used to map specific
            `Gate` objects used in this circuit to new instruction names that
            should be used for those gates in the output QASM.

        Examples
        --------
        Because this method returns a string, to write a QASM file simply
        open a file for writing and write the return value of this method::

            >>> circ = Circuit([(H, 0), (H, 1), (CNOT, (0, 1)),
            ...                 (H, 0), (H, 1)])
            >>> with open('example.qasm', 'w') as f:
            ...     f.write(circ.to_qasm())
        """

        lines = []
        qubits, gate_defs, qgates = \
            self._to_qasm2circ(instruction_map=instruction_map)

        if gate_defs:
            # Group gate definitions so that single-qubit gates (controlled or
            # otherwise) are defined before multi-qubit "box" gates
            key = lambda d: d[1] - d[2]
            def_groups = itertools.groupby(sorted(gate_defs.values(),
                                                  key=key), key)
            for key, defs in def_groups:
                if key == 1:
                    format_str = "\tdef\t{inst},{n_control},'{label}'"
                else:
                    format_str = \
                        "\tdefbox\t{inst},{n_qubits},{n_control},'{label}'"
                for inst, n_qubits, n_control, label in defs:
                    args = {'inst': inst, 'n_qubits': n_qubits,
                            'n_control': n_control, 'label': label}
                    lines.append(format_str.format(**args))
                lines.append('')

        for qubit in qubits:
            lines.append('\tqubit\t' + qubit)

        lines.append('')

        for instruction, args in qgates:
            lines.append('\t{0}\t{1}'.format(instruction, args))

        return '\n'.join(lines)

    # TODO: to_chp
    @classmethod
    def from_chp(cls, chp):
        """
        Creates a new `Circuit` object from the instructions read in from the
        CHP file format used by Scott Aaronson's ``chp`` program:
        http://www.scottaaronson.com/chp/

        This is a very simple format that only supports Hadamard, phase, and
        CNOT gates.

        Parameters
        ----------
        chp : `str`
            The CHP program as a string.
        """

        description, instructions = _parse_chp(chp)
        return cls(instructions, description=description)

    @classmethod
    def from_chp_extended(cls, chp):
        """
        Creates a new `Circuit` object from the instructions read in from an
        *extended* version of the format understood by `Circuit.from_chp`.

        This extended format also supports Pauli X, Y, and Z gates as well
        as identity using the ``x``, ``y``, ``z``, and ``i`` instructions
        respectively.
        """

        description, instructions = _parse_chp(chp, extended=True)
        return cls(instructions, description=description)

    @classmethod
    def from_wires(cls, *wires):
        """
        Create a new `Circuit` instance from lists of "wires" listing the
        operations performed on each qubit at each time step.

        See the full PyQC documentation for more details of this format.
        """

        # TODO: Provide a Circuit.to_wires() as well; probably not that useful
        # but nice to have the symmetry I guess
        if not wires:
            raise CircuitDefinitionError(
                "A circuit must have at least one 'wire' with gates on it.")

        def process_step(step, gates):
            """Given a list of gates at a single time step (including labels
            for multi-qubit gates) return an expression tree for the tensor
            product of all the gates at that time step.
            """

            instructions = []
            seen_multi_gates = set()

            for idx, gate in enumerate(gates):
                if isinstance(gate, _InputLabel) and gate.gate.n_qubits > 1:
                    # If any multi_qubit gates were found go through a
                    # pre-processing step that converts labels for multi-qubit
                    # gates to the gate itself and ensures all the inputs for
                    # that gate are provided and in a valid order
                    gate = gate.gate
                    if id(gate) in seen_multi_gates:
                        continue
                    instructions.append(process_multi_gate(step, gate, gates))
                    seen_multi_gates.add(id(gate))
                elif isinstance(gate, IdentityGate):
                    # Skip identity gates which are essentially used as blank
                    continue
                else:
                    instructions.append(_Instruction(gate, idx))

            return instructions

        def process_multi_gate(step, gate, gates):
            all_inputs = set(gate.labels)
            given_inputs = {}

            for idx, label in enumerate(gates):
                if not (isinstance(label, _InputLabel) and label.gate is gate):
                    continue

                if label.name in given_inputs:
                    raise CircuitDefinitionError(
                        "Multiple instances of the same multi-qubit gate at "
                        "the same time step are not supported due to "
                        "ambiguity as to which input labels are associated "
                        "with which gate.")
                given_inputs[label.name] = idx

            given_inputs_set = set(given_inputs)
            if given_inputs_set != all_inputs:
                missing = all_inputs.difference(given_inputs_set)
                raise CircuitDefinitionError(
                    "All inputs to a multi-qubit gate must be on the same "
                    "step.  The following inputs to the {0} gate at step {1} "
                    "were not specified:\n    {2}".format(
                        gate.name, step, list(missing)))

            # For general multi-qubit gates all inputs need to be in order and
            # contiguous.  For controlled gates that is only true for the
            # target inputs, while the control inputs can be in any order.  For
            # swap gates the order is irrelevant
            check_labels = []
            if isinstance(gate, ControlledGate):
                if gate._controlled_gate.n_qubits > 1:
                    for n in range(gate._controlled_gate.n_qubits):
                        check_labels.append('target{0}'.format(n))

                msg = ("All inputs to the target of a controlled multi-qubit "
                       "gate must be in consecutive order.")
            elif not isinstance(gate, SwapGate):
                check_labels[:] = gate.labels
                msg = ("All inputs to a multi-qubit gate must be in "
                       "consecutive order.")

            if check_labels:
                previous = given_inputs[check_labels[0]]
                for label in check_labels[1:]:
                    _next = given_inputs[label]
                    if _next != previous + 1:
                        raise CircuitDefinitionError(msg)
                    previous = _next

            args = tuple(given_inputs[label] for label in gate.labels)
            return _Instruction(gate, args)

        instructions = []
        for step, gates in enumerate(zip(*wires)):
            instructions.extend(process_step(step, gates))

        return cls(instructions)

    @classmethod
    def from_gate(cls, gate):
        """
        Creates a new `Circuit` from an existing `Gate`.

        If the input is another `Circuit` class this just copies that circuit.
        If the input is a simple `Gate` instance this creates a trivial
        single-gate circuit.  However, the input was a gate created from an
        algebraic expression of simpler gates, that expression will be
        converted to the corresponding circuit.

        Parameters
        ----------
        gate : `Gate`
            The existing `Gate` object to convert to a `Circuit`.

        Examples
        --------

        >>> G = (H * H)|CNOT(H * H)
        >>> C = Circuit.from_gate(G)
        >>> print C.to_qasm()

            qubit   q0
            qubit   q1

            H   q0
            H   q1
            cnot    q0,q1
            H   q0
            H   q1

        """

        # First get the simple cases out of the way
        if isinstance(gate, Circuit):
            return cls(gate._instructions)
        elif not isinstance(gate, _CompoundGate):
            # A trivial Circuit with a single gate
            return cls([(gate, _get_default_gate_inputs(gate))])
        elif not isinstance(gate, GateBase):
            raise ValueError(
                "Circuit.from_gate only accepts pyqc.Gate objects.")

        # Now the more interesting case--converting an expression tree to a
        # list of instructions
        def leaf_tensor(left, right):
            # Given a left-hand side consisting of a tensor product of
            # operators, and a right-hand side consisting of the tensor product
            # of one or more operators, return concatenate the two together,
            # but updating the input qubit numbers on the right-hand side so
            # that they follow the inputs on the left-hand side
            left_args = left[-1][1]
            if isinstance(left_args, tuple):
                max_left = max(left_args) + 1
            else:
                max_left = left_args + 1

            right = [
                _Instruction(op, tuple(arg + max_left for arg in args))
                             for op, args in right
            ]

            return left + right

        def leaf_product(left, right):
            # Simply ensure that the right-most operators come before the
            # left-most operators in the instruction list
            return right + left

        def leaf_getter(value):
            # Convert a single gate to an instruction code
            # We need to handle a couple special cases
            args = _get_default_gate_inputs(value)
            return [_Instruction(value, args)]

        operators = {
            'tensor': leaf_tensor,
            'product': leaf_product
        }

        instructions = gate._tree.evaluate(operators, leaf_getter)

        # Filter out no-ops
        instructions = [inst for inst in instructions
                        if not isinstance(inst[0], IdentityGate)]

        return cls(instructions)


    @classmethod
    def _validate_instruction(cls, instruction):
        """
        Validate a single instruction passed to Circuit.from_instructions.
        """

        if not isinstance(instruction, tuple) or len(instruction) != 2:
            raise ValueError(
                "All instructions must be input as a 2-tuple of the "
                "gate and its input qubit(s) given as either a single "
                "integer or a tuple of integers; "
                "got {0!r}.".format(instruction))

        gate, inputs = instruction

        if not (isinstance(inputs, (int, tuple)) or
                (isinstance(inputs, tuple) and len(inputs) < 1)):
            raise ValueError(
                "Input qubits must be given as a single integer (for a "
                "single-qubit gate) or a tuple if one or more integers; "
                "got {0!r}.".format(inputs))

        if isinstance(inputs, int):
            inputs = (inputs,)

        if len(inputs) != len(_get_default_gate_inputs(gate)):
            raise CircuitDefinitionError(
                "Gate {0!r} takes {1} input qubits; got {2!r}.".format(
                    gate.name, gate.n_qubits, inputs))

        return gate, inputs

    @classmethod
    def _tree_from_instructions(cls, instructions):
        # To start out we don't know the max number of qubits required for the
        # circuit--that will be determined as we read through the instructions
        n_qubits = 0

        # List of "time steps" consisting of sets of gates that can be applied
        # to the state vector simultaneously (as a tensor product)
        steps = [[]]

        def can_occupy(step, inputs):
            min_input = min(inputs)
            max_input = max(inputs)
            if len(step) < max_input + 1:
                step.extend([I] * (max_input - len(step) + 1))

            return all(step[idx] is I
                       for idx in range(min_input, max_input + 1))

        for instruction in instructions:
            gate, inputs = cls._validate_instruction(instruction)
            n_qubits = max(n_qubits, max(inputs) + 1)

            last_step = None
            for step in reversed(steps):
                if not can_occupy(step, inputs):
                    break
                last_step = step

            if last_step is None:
                last_step = [I] * n_qubits
                steps.append(last_step)

            if len(inputs) == 1:
                # A single qubit gate is easy to deal with
                last_step[inputs[0]] = gate
                continue

            # Multi-qubit gates take a little more to figure out
            first_qubit = min(inputs)
            last_qubit = max(inputs)
            n_qubit = last_qubit - first_qubit + 1

            if isinstance(gate, SwapGate):
                new_gate = gate(n_qubit)
            elif isinstance(gate, ControlledGate):
                controls = inputs[:gate.n_control]
                target = inputs[gate.n_control]

                # If the first qubit in the gate is *not* 0 then we have to
                # subtract the number of the first qubit from the input
                # numberings
                target -= first_qubit
                if gate.n_control == 1:
                    new_gate = gate(n_qubit, target=target,
                                    control=controls[0] - first_qubit)
                else:
                    controls = dict(('control{0}'.format(idx), c - first_qubit)
                                    for idx, c in enumerate(controls))
                    new_gate = gate(n_qubit, target=target, **controls)
            else:
                new_gate = gate

            last_step[first_qubit] = new_gate
            # Just fill the rest of the "step" occupied by this gate with Nones
            # in order to mask it out
            last_step[first_qubit + 1:last_qubit + 1] = [None] * (n_qubit - 1)

        def prepare_step(step):
            step.extend([I] * (n_qubits - len(step)))
            return filter(None, step)

        # TODO: Include a regression test that ensures the generated tree
        # represents the right order of evaluation for operators
        compound = reduce(operator.or_,
                          (reduce(operator.mul, prepare_step(step))
                           for step in reversed(steps)))

        # This can happen in case of a single-instruction "circuit"
        if not isinstance(compound, _CompoundGate):
            tree = ExpressionTree(compound)
        else:
            tree = compound._tree

        return tree

    @staticmethod
    def _normalize_instructions(instructions):
        """
        Normalizes instruction list such that the arguments for single-qubit
        operators are always given as an integer (and not a single-element
        tuple).  This simplifies later processing.

        Also returns the max number of qubits used by the instruction list.
        """

        normalized = []
        n_qubits = 1

        for op, args in instructions:
            if isinstance(args, tuple) and len(args) == 1:
                n_qubits = max(n_qubits, args[0] + 1)
                normalized.append(_Instruction(op, args[0]))
            elif isinstance(args, int):
                n_qubits = max(n_qubits, args + 1)
                normalized.append(_Instruction(op, args))
            else:
                n_qubits = max(n_qubits, max(args) + 1)
                normalized.append(_Instruction(op, args))

        return normalized, n_qubits

    def _resolve_placeholders(self):
        """
        Replace any placeholder gates in the circuit's instruction list with
        concrete gates from the registry, if possible.
        """

        def replace_instr(instr):
            # Replace placeholder gates with a real gate if possible (i.e. a
            # real gate with the same name has been registered)
            gate, args = instr
            if isinstance(gate, _PlaceholderGateBase):
                real_gate = self.registry.get(gate.name)
                if real_gate is not None:
                    if isinstance(gate, _PlaceholderControlledGate):
                        # Wrap the registered real gate as a controlled gate
                        real_gate = ControlledGate(
                                real_gate, n_control=gate.n_control,
                                n_qubits=gate.n_qubits)

                    # Now return the real gate instead of the placeholder
                    gate = real_gate

                instr = _Instruction(gate, args)

            return instr

        self._instructions = list(map(replace_instr, self._instructions))

    def _to_qasm2circ(self, instruction_map={}):
        """
        The core of `Circuit.to_qasm` and `Circuit._format_latex_qasm2circ`.

        Converts the `Circuit` instruction list to gate definitions understood
        by qasm2circ.
        """

        qubits = ['q{0}'.format(idx) for idx in range(self.n_qubits)]
        gate_defs = OrderedDict()
        qgates = []

        def def_gate(gate):
            # First we have to come up with an instruction name for this gate,
            # fortunately the version of QASM we are working off of (for now)
            # basically allows any non-whitespace character except comma for
            # instruction names
            orig_gate = gate

            if isinstance(orig_gate, ControlledGate):
                gate = gate._controlled_gate
                n_control = orig_gate.n_control
            else:
                n_control = 0

            if id(orig_gate) in instruction_map:
                inst = instruction_map[id(orig_gate)]
            else:
                inst = re.sub(r'\s|,', '', gate.name)

                if isinstance(orig_gate, ControlledGate):
                    inst = 'c-' + inst

            if gate.latex_name == r'\hat{{{0}}}'.format(gate.name):
                label = gate.name
            else:
                label = gate.latex_name

            gate_defs[id(orig_gate)] = \
                    (inst, orig_gate.n_qubits, n_control, label)
            return inst

        def overlap_lookahead(idx, args):
            """
            This is a workaround to prevent the lines connecting SWAP and
            controlled gates from overlapping other instructions.
            """

            args = set(args)
            possible_overlaps = [a for a in range(min(args), max(args) + 1)
                                 if a not in args]
            for gate, inputs in self._instructions[idx + 1:]:
                if not possible_overlaps:
                    break

                if isinstance(inputs, int):
                    if inputs in possible_overlaps:
                        # Append nop to prevent overlap
                        qgates.append(('nop', qubits[inputs]))
                        possible_overlaps.remove(inputs)
                    continue

                # Multi-qubit gates
                # Check the full range of wires covered by this gate
                inputs = set(range(min(inputs), max(inputs) + 1))
                overlap = inputs.intersection(args)
                possible_overlaps = set(possible_overlaps).difference(overlap)


        for idx, instruction in enumerate(self._instructions):
            gate, inputs = instruction
            if isinstance(inputs, int):
                inputs = (inputs,)

            if gate is I:
                instruction = 'nop'
            elif isinstance(gate, MeasurementGate):
                instruction = 'measure'
            elif len(inputs) == 1 and gate.name in _qgatedefs:
                instruction = gate.name
            elif isinstance(gate, SwapGate):
                instruction = 'swap'
            elif isinstance(gate, CnotGate):
                instruction = 'cnot'
            elif isinstance(gate, ToffoliGate):
                instruction = 'toffoli'
            elif (isinstance(gate, ControlledGate) and
                    gate._controlled_gate is X and gate.n_control == 1):
                instruction = 'c-x'
            elif (isinstance(gate, ControlledGate) and
                    gate._controlled_gate is Z and gate.n_control == 1):
                instruction = 'c-z'
            elif id(gate) in gate_defs:
                instruction = gate_defs[id(gate)][0]
            else:
                instruction = def_gate(gate)

            args = ','.join(qubits[idx] for idx in inputs)
            qgates.append((instruction, args))

            if isinstance(gate, (SwapGate, ControlledGate)):
                overlap_lookahead(idx, inputs)

        return qubits, gate_defs, qgates

    def _format_latex_qasm2circ(self):
        """
        Converts the circuit to LaTeX using qasm2circ.

        This skips writing the actual qasm code and uses qasm2circ's "API"
        directly.
        """

        qubits, gate_defs, qgates = self._to_qasm2circ()

        # qasm2circ supports classical bits as well as qubits; for now we only
        # support qubits
        types = [0] * self.n_qubits

        # This is all that's needed to instatiate a 'qcircuit':
        qc = _QCircuit(qubits, types)

        # qasm2circ uses a global table of gate definitions which is modified
        # at runtime to support custom gates; we should restore this to its
        # original values after running in case we need to define any custom
        # gates as well
        orig_gatemasterdef = dict(_qgatedefs)

        for inst, n_qubits, n_control, label in gate_defs.values():
            if n_qubits - n_control == 1:
                # A silly quirk about how qasm2circ formats single-qubit gates
                label = r'\op{{{0}}}'.format(label)
            _qgatedefs[inst] = (n_qubits, n_control, label)

        # This refers to a line of (non-existent) qasm code, and not a line of
        # the circuit
        qasm_line = 1
        latex_lines = []
        try:
            for opcode, args in qgates:
                qc.add_op(_QGate(opcode, args, qasm_line))
                qasm_line += 1

            for line in qc.output_matrix():
                latex_lines.append(line)

            for line in qc.output_latex():
                latex_lines.append(line)
        finally:
            _qgatedefs.clear()
            _qgatedefs.update(orig_gatemasterdef)

        return '\n'.join(latex_lines)


class _Instruction(namedtuple('Instruction', ['gate', 'args'])):
    """
    Tuple representing a single instruction in the instruction listing returned
    by `Circuit.to_instructions`.
    """

    __slots__ = ()

    def __repr__(self):
        return '({0}, {1})'.format(self.gate.format_text('name'), self.args)


class _PlaceholderGateBase(GateBase):
    """
    This is a placeholder for a Gate that has not yet been defined--it
    has the same name and possibly LaTeX representation of some real
    Gate, but it cannot be evaluated--the Qobj which defines its operation
    on a state is undefined.

    It is used as a temporary marker in circuits read in from assembly
    code for gates named in the assembly (such as in QASM) that don't have
    a known definition already in PyQC.
    """

    @abc.abstractmethod
    def __init__(self, name, latex_name=None, labels=None, *args, **kwargs):
        super(_PlaceholderGateBase, self).__init__(
                name=name, latex_name=latex_name, labels=labels,
                register=False)

    @property
    def n_qubits(self):
        """The number of qubits this operator acts on."""

        return self._n_qubits

    @property
    def dimension(self):
        """The dimension of the Hilbert space this operator acts on."""

        return 2 ** self.n_qubits

    @property
    def qobj(self):
        """
        Normally this would return a `qutip.Qobj` instance defining the matrix
        elements of this operator.

        However, the point of placeholder operators is that they are undefined,
        and as such returning its qobj and trying to perform any operations
        on it that require it to have a qobj will fail.
        """

        raise NotImplementedError(
            "The gate {0!r} is currently a placeholder for an undefined gate."
            "  In order for this circuit to be evaluated a `Gate` instance "
            "with the name {0!r} must be created and registered in the global "
            "gate registry.  For example:\n\n"
            "    >>> custom_gate = Gate([[...]], name={0!r}, register=True)"
            "".format(self.name))

    def __repr__(self):
        return '{0}(n_qubits={1}, name={2!r})'.format(
            self.__class__.__name__, self.n_qubits, self.name)


class _PlaceholderGate(_PlaceholderGateBase):
    """
    Class for non-controlled, generic placeholder gates.
    """

    def __init__(self, n_qubits, name, latex_name=None):
        super(_PlaceholderGate, self).__init__(n_qubits=n_qubits, name=name,
                                               latex_name=latex_name)
        self._n_qubits = n_qubits


class _PlaceholderControlledGate(ControlledGate, _PlaceholderGateBase):
    """
    Marks a controlled gate whose target gate is a placeholder.

    Like a normal _PlaceholderGate this cannot be evaluated, but is motly
    useful as a bookkeeping to distinguish controlled gates that use a
    placeholder.
    """

    def __init__(self, n_qubits, n_control, name, latex_name=None, target=None,
                 **kwargs):
        placeholder_gate = _PlaceholderGate(n_qubits - n_control, name,
                                            latex_name=latex_name)
        super(_PlaceholderControlledGate, self).__init__(
            gate=placeholder_gate, n_control=n_control, n_qubits=n_qubits,
            name=name, latex_name=latex_name, target=target, **kwargs)

    def __repr__(self):
        return '{0}(n_qubits={1}, n_control={2}, name={3!r})'.format(
            self.__class__.__name__, self.n_qubits, self._n_control,
            self.name)

    def __call__(self, n_qubits=None, *args, **kwargs):
        if n_qubits is None:
            n_qubits = self.n_qubits

        return self.__class__(n_qubits, self._n_control, self._name, *args,
                              **kwargs)


# The pre-defined instructions in qasm are easily mapped to existing Gate
# instances; it's the custom gates where things get tricky
_QASM_INSTR_TO_GATE = {
    'cnot':    CNOT,
    'c-z':     ControlledGate(Z),
    'c-x':     ControlledGate(X),
    'h':       H,
    'H':       H,
    'X':       X,
    'Y':       Y,
    'Z':       Z,
    'S':       S,
    'T':       T,
    'nop':     I,
    'swap':    SWAP,
    'toffoli': Toffoli,
    'measure': M
}


# The way qasm stores gate information we unfortunately have to parse a small
# snippet of LaTeX to get the gate name--for whatever reason this only applies
# to single-qubit gates... :(
_QASM_TEX_RE = re.compile(r'\\op\{(?P<gatename>[^}]+)\}')


def _qgate_to_gate(qgate):
    """
    Utility function to convert one of qasm2circ's qgate objects to the
    appropriate pyqc gate.
    """

    if qgate.name in _QASM_INSTR_TO_GATE:
        return _QASM_INSTR_TO_GATE[qgate.name]

    if qgate.nbits - qgate.nctrl == 1:
        gate_name = _QASM_TEX_RE.match(qgate.texsym).group('gatename')
    else:
        gate_name = qgate.name

    if gate_name not in Circuit.registry:
        if qgate.nctrl > 0:
            gate = _PlaceholderControlledGate(qgate.nbits, qgate.nctrl,
                                              gate_name)
        else:
            gate = _PlaceholderGate(qgate.nbits, gate_name)

        return gate

    gate = Circuit.registry[gate_name]

    if qgate.nctrl > 0:
        gate = ControlledGate(gate, qgate.nctrl)

    return gate


# Mappings of .chp format opcodes to the gate objects they represent
_CHP_INSTR_TO_GATE = {
    'c': CNOT,
    'h': H,
    'p': S,
    'm': M
}

# Mappings for extended .chp format that includes support for Pauli operators
_CHP_EXT_INSTR_TO_GATE = _CHP_INSTR_TO_GATE.copy()
_CHP_EXT_INSTR_TO_GATE.update({
    'i': I,
    'x': X,
    'y': Y,
    'z': Z
})

_CHP_INSTR_RE = re.compile(r'\s*(?P<opcode>[a-z])\s*'
                           r'(?P<args>[0-9]+(\s+[0-9]+)*)')


def _parse_chp(chp, extended=False):
    """
    Parses the .chp assembly format used by Scott Aaronson's CHP program that
    implements the algorithm described in Aaronson & Gotteman (Phys. Rev. A 70,
    052328 (2004)).

    This is a very simple format consisting of some leading commentary text,
    followed by a set of either c (CNOT), h (Hadamard), p (Phase), or m
    (measure) instructions, each followed by whitespace and then the qubit
    operated on (or in the case of CNOT the control qubit followed by
    whitespace followed by the target qubit).  The number of qubits in the
    circuit is determined by the highest qubit number referenced in any
    instructions (0 being the first qubit).

    This parse also supports an extended version of the format with support for
    i (identity), and x, y, and z for the Pauli X, Y, and Z gates respectively.
    """

    if extended:
        gate_map = _CHP_EXT_INSTR_TO_GATE
    else:
        gate_map = _CHP_INSTR_TO_GATE

    description = []
    instructions = []
    begin_instructions = False

    for idx, line in enumerate(chp.splitlines()):
        if not begin_instructions:
            if line and line[0] == '#':
                begin_instructions = True
            else:
                description.append(line)
            continue

        line = line.strip()

        if not line:
            continue

        match = _CHP_INSTR_RE.match(line)
        if match is None:
            raise CircuitDefinitionError(
                "Invalid CHP instruction: {0!r} on line {1}".format(
                    line, idx + 1))
        opcode = match.group('opcode')
        args = tuple(map(int, match.group('args').split()))

        if opcode not in gate_map:
            raise CircuitDefinitionError(
                "Unrecognized CHP instruction opcode: {0!r} "
                "on line {1}".format(opcode, idx + 1))

        if opcode != 'c' and len(args) > 1:
            raise CircuitDefinionError(
                "Invalid number of arguments to CHP instruction {0!r}: "
                "{1} on line {2} ({0!r} only takes one input)".format(
                    opcode, len(args), idx + 1))
        elif opcode == 'c' and len(args) > 2:
            raise CircuitDefinionError(
                "Invalid number of arguments to CHP instruction {0!r}: "
                "{1} on line {2} ({0!r} only takes two inputs)".format(
                    opcode, len(args), idx + 1))

        gate = gate_map[opcode]

        if opcode != 'c':
            # Take only the first argument
            args = args[0]

        instructions.append((gate, args))

    return '\n'.join(description), instructions
