# -*- coding: utf-8 -*-
"""
Defines built-in gates and other specialized gate classes such as
`ConrolledGate`.
"""

from __future__ import absolute_import

import abc
import itertools

import numpy as np
import qutip
from qutip.qip import operations as ops

from .operator import GateBase
from .operator import Gate as _Gate


# TODO: It might be useful if gates had a method that can return a controlled
# version of itself
__all__ = ['I', 'X', 'Y', 'Z', 'H', 'S', 'T', 'CNOT', 'Toffoli', 'SWAP',
           'R_k', 'ControlledGate', 'M']


class Gate(_Gate):
    """
    This is required as a hack to make Sphinx document `Gate` instances in this
    module properly.
    """
Gate.__doc__ = _Gate.__doc__


X = Gate(qutip.sigmax(), name='X', register=True)
"""Pauli X gate."""

Y = Gate(qutip.sigmay(), name='Y', register=True)
"""Pauli Y gate."""

Z = Gate(qutip.sigmaz(), name='Z', register=True)
"""Pauli X gate."""

S = Gate(ops.phasegate(np.pi / 2), name='S', register=True)
r"""
Phase gate.

Equivalent to:

:math:`\begin{pmatrix}1 & 0\\0 & e^{i\pi/2}\end{pmatrix}`

::

    >>> S
    S = [[  1.0...+0.j   0.0...+0.j]
         [  0.0...+0.j   ...+1.j]]
"""

T = Gate(ops.phasegate(np.pi / 4), name='T', register=True)
r"""
π/8 gate.

Equivalent to:

:math:`\begin{pmatrix}1 & 0\\0 & e^{i\pi/4}\end{pmatrix}`

::

    >>> T
    T = [[ 1.0...+0.j          0.0...+0.j            ]
         [ 0.0...+0.j          0.70710678+0.70710678j]]
"""

H = Gate(ops.snot(), name='H', register=True)
"""Hadamard gate."""


class GateDefinitionError(Exception):
    """Exception raised when initializing gates with invalid properties."""


class _VariableQubitGate(GateBase):
    """
    This is a base class for classes of Gates that have a common purpose but
    can be applied to any number of qubits.  The simplest example is the
    `IdentityGate`--it can operate on any number of qubits but regardless its
    purpose is the same: it is a no-op.

    Another example is the `CnotGate`--although it only uses two qubits in its
    operation the `CnotGate` class allows creating a CNOT that spans multiple
    qubits between its control and target qubits, for use in > 2 qubit
    circuits.

    _VariableQubitGates generally have a default number of input qubits, but
    instances of _VariableQubitGates can be *called* to return a new instance
    that operatos on a different number of qubits.  Returning to the example of
    `IdentityGate`, this module defines an instance of `IdentityGate` called
    simply 'I':

        >>> I = IdentityGate()

    By default it operates on a single qubit, but multi-qubit identity gates
    can be created simply by calling single-qubit instance.  For example a
    4-qubit identity can be created like:

        >>> I(4)

    This is essentially identical to calling ``IdentityGate(4)`` and is simply
    a shortcut.  The same concept applies to all gates that subclass
    _VariableQubitGate.
    """

    @abc.abstractmethod
    def __init__(self, min_qubits, n_qubits=None, name=None, latex_name=None,
                 representation='both', labels=None, register=False,
                 replace=False):
        if n_qubits is None:
            # The general case where the gate operates on a state of exactly
            # the total qubits operated on by all the component gates as
            # though they were "stacked horizontally"
            n_qubits = min_qubits

        if n_qubits < min_qubits:
            raise GateDefinitionError(
                "A {0} must operate on at least a {1}-qubit "
                "state space".format(self.__class__.__name__, min_qubits))

        self._n_qubits = n_qubits

        super().__init__(
            name=name, latex_name=latex_name, representation=representation,
            labels=labels, register=register, replace=replace)

    @property
    def n_qubits(self):
        """The number of qubits this operator acts on."""

        return self._n_qubits

    def __call__(self, n_qubits=None, *args, **kwargs):
        if n_qubits is None:
            n_qubits = self.n_qubits
        kwargs['n_qubits'] = n_qubits
        return self.__class__(*args, **kwargs)


class _NamedGate(GateBase):
    """
    Special mix-in class for Gate classes that have a particular name, such as
    'I' for `IdentityGate` or 'CNOT' for `CnotGate`.  This ensures that only
    one instance of that class is kept in the Operator registry.
    """

    def __new__(cls, *args, **kwargs):
        name = cls.base_name

        inst = super().__new__(cls)

        if not hasattr(cls, '_registered') or not cls._registered:
            cls.registry[name] = inst
            cls._registered = True

        return inst


class IdentityGate(_VariableQubitGate, _NamedGate):
    """
    Creates an n-qubit identity operator.

    Parameters
    ----------
    n_qubits : `int`
        Create an identity operator for states of n qubits.  Defaults to 1.
    """

    base_name = 'I'

    def __init__(self, n_qubits=1):
        # TODO: Also provide an unambiguous plain-text representation once
        # plain text repr for Gates is reworked.
        # Use an unambiguous name and LaTeX name.
        name = self.base_name
        latex_name = r'\hat{I}'

        if n_qubits > 1:
            name += '({0})'.format(n_qubits)
            latex_name += r'^{{\otimes {0}}}'.format(n_qubits)

        super().__init__(
            1, n_qubits=n_qubits, name=name, latex_name=latex_name,
            representation='name')

    @property
    def qobj(self):
        """The representation of this operator as a QuTiP `~qutip.Qobj`."""

        qobj = qutip.identity(2**self._n_qubits)
        # Update the dimensions to compatible with n-qubit states
        qobj.dims = [[2] * self._n_qubits, [2] * self._n_qubits]
        return qobj


# Shortcut for 1-qubit identity:
I = IdentityGate()
"""
Single qubit identity gate.

Parameterized so that calling it creates a new n-qubit identity gate::

    >>> I(2)
    I(2) = [[ 1.  0.  0.  0.]
            [ 0.  1.  0.  0.]
            [ 0.  0.  1.  0.]
            [ 0.  0.  0.  1.]]
"""


class SwapGate(_VariableQubitGate, _NamedGate):
    """
    Creates an ``n_qubits`` SWAP gate that swaps the first qubit with the last
    qubit.

    For example, for ``n_qubits=3`` this creates a SWAP gate like::

        --x--
        --|--
        --x--

    This gate can then be embedded in higher-dimensional Hilbert spaces by
    taking tensor products with identity.

    Parameters
    ----------
    n_qubits : `int`
        Create a SWAP gate for state spaces of n qubits.  Defaults to 2.
    """

    base_name = 'SWAP'

    def __init__(self, n_qubits=2):
        name = self.base_name
        if n_qubits > 2:
            name += '({0})'.format(n_qubits)

        super().__init__(
            2, n_qubits=n_qubits, name=name, latex_name=name,
            representation='name')

    @property
    def qobj(self):
        """The representation of this operator as a QuTiP `~qutip.Qobj`."""

        return ops.swap(N=self._n_qubits, targets=[0, self._n_qubits - 1])


# Shortcut for basic 2-qubit SWAP gate
SWAP = SwapGate()
"""
A two qubit SWAP gate.

Parameterized so that calling it creates a new n-qubit SWAP gate that works
in an n-qubit state space (swapping the first and last qubits by default)::

    >>> SWAP(3)|Ket(1, 0, 0)
    |0, 0, 1>
"""


class ControlledGate(_VariableQubitGate):
    """
    Creates an n-qubit gate that is a controlled m-qubit gate operating on an
    n-qubit state.  The number of control bits is 1 by default, but may be as
    many as n - m.

    The total of the control bits (n_control) and the target gate qubits
    (n_target) do not need to add up to n_qubits--this is the case when we want
    to embed a controlled gate in a circuit where not all qubits in the circuit
    are used by that gate.

    For example, for ``n_qubits=3`` this can create a CNOT gate like::

        --•--
        --|--
        --⊕--

    This gate can then be embedded in higher-dimension Hilbert spaces by
    taking tensor products with identity.

    If n_qubits = n_control + n_target, the default layout places all the
    control qubits first, followed by the target gate.  If n_qubits > n_control
    + n_target (that is, the controlled gate is going to be used in a
    higher-dimensional space) the control qubits are first, followed by enough
    blank wires to fill out the space, followed by the target gate.  For
    example if n_qubits = 6, n_control=2, and n_target = 1 this would look in a
    circuit diagram like::

        --•--
        --•--
        --|--
        --|--
        --U--
        --U--

    where the 'U' represent the inputs to the target 2-qubit gate.
    """

    def __init__(self, gate, n_control=1, n_qubits=None, name=None,
                 latex_name=None, representation=None, register=False,
                 replace=False, target=None, **kwargs):
        if n_control < 1:
            raise GateDefinitionError(
                'Controlled gates must have at least 1 control qubit')

        min_qubits = gate.n_qubits + n_control

        if n_qubits is None:
            n_qubits = min_qubits

        if n_qubits < min_qubits:
            raise GateDefinitionError(
                "Controlled {0} gates must operate on at least a {1}-qubit "
                "state space".format((gate.name + ' ' if gate.name else ''),
                                     min_qubits))

        labels = self._generate_labels(n_control, gate.n_qubits)

        # If not given, take the name from the controlled gate's name
        if name is None:
            # TODO: What to do if the controlled gate does not have a specific
            # name?
            name = 'C{0}({1})'.format((n_control if n_control > 1 else ''),
                                      gate.name)

        if latex_name is None:
            if n_control > 1:
                cntl = 'C^{{{0}}}'.format(n_control)
            else:
                cntl = 'C'

            if gate.name is not None:
                latex_name = '{0}({1})'.format(cntl, gate.latex_name)
            else:
                latex_name = r'{0}\left[{1}\right]'.format(cntl,
                                                           gate.latex_name)

        super().__init__(
            min_qubits, n_qubits=n_qubits, name=name, latex_name=latex_name,
            representation=representation, labels=labels, register=register,
            replace=replace)

        self._n_control = n_control
        self._controlled_gate = gate
        self._process_inputs(target, **kwargs)

    def __call__(self, n_qubits=None, *args, **kwargs):
        if n_qubits is None:
            n_qubits = self.n_qubits
        return self.__class__(self._controlled_gate, self._n_control, n_qubits,
                              *args, **kwargs)

    @property
    def n_control(self):
        """The number of control qubits used by this gate."""

        return self._n_control

    @property
    def qobj(self):
        """The representation of this operator as a QuTiP `~qutip.Qobj`."""

        # else build up out of simpler operators
        control_off = qutip.fock_dm(2, 0)
        control_on = qutip.fock_dm(2, 1)
        target_off = I(self._controlled_gate.n_qubits).qobj
        target_on = self._controlled_gate.qobj

        controls = (control_off, control_on)

        template = []
        controls = []
        # Read the input map into a "template" determining where all the
        # "spacers" are, and also determining the position of the target, and
        # the positions of each controls in that template
        idx = 0
        while idx < len(self._input_map):
            code = self._input_map[idx]
            if code is None:
                n = 1
                idx += 1
                while (idx < len(self._input_map) and
                        self._input_map[idx] is None):
                    n += 1
                    idx += 1
                template.append(I(n).qobj)
            elif code == 't':
                target_pos = len(template)
                template.append(None)
                idx += 1
                while (idx < len(self._input_map) and
                        self._input_map[idx] == 't'):
                    idx += 1
            elif code == 'c':
                controls.append(len(template))
                template.append(None)
                idx += 1

        series = []
        for cts in itertools.product(*([(0, 1)] * self._n_control)):
            product = template[:]

            for pos, val in zip(controls, cts):
                if val == 0:
                    product[pos] = control_off
                else:
                    product[pos] = control_on

            if sum(cts) == self._n_control:
                product[target_pos] = target_on
            else:
                product[target_pos] = target_off

            series.append(qutip.tensor(*product))

        return sum(series)

    @staticmethod
    def _generate_labels(n_control, n_target):
        """
        Determine the labels to enable on this gate, given the number of
        control bits ``n_control`` and the number of qubits operated on by the
        target gate ``n_target``.
        """

        if n_control == 1:
            labels = [('control', 'c')]
        else:
            labels = []
            for idx in range(n_control):
                labels.append(('control{0}'.format(idx), 'c{0}'.format(idx)))

        if n_target == 1:
            labels.append(('target', 't'))
        else:
            for idx in range(n_target):
                labels.append(('target{0}'.format(idx), 't{0}'.format(idx)))

        return labels

    def _process_inputs(self, target, **kwargs):
        """
        Determine which input qubits should be mapped to which control qubits
        and to the target qubit.
        """

        self._input_map = [None] * self._n_qubits
        n_target = self._controlled_gate.n_qubits

        if 't' in kwargs:
            # Alias for 'target'
            if target is None:
                target = kwargs.pop('t')
            else:
                raise TypeError(
                    "Duplicate position specification for target qubit.")

        # In the input map the inputs to the target gate are labeled 't', and
        # the control qubits are labeled 'c'

        if target is None:
            self._input_map[-n_target:] = ['t'] * n_target
        else:
            if target > self._n_qubits - n_target:
                raise GateDefinitionError(
                    "Given position of the target gate {0} does not leave "
                    "enough space for the {1}-qubit target gate.".format(
                        target, n_target))
            self._input_map[target:target + n_target] = ['t'] * n_target

        # Now process the kwargs for all the control qubit positions, if any
        # This maps control qubit positions to the control qubit at that
        # position
        control_positions = {}
        for idx in range(self._n_control):
            # Each control can be specified like either c0 or control0 just
            # like the labels
            if self._n_control == 1:
                label1 = 'c'
                label2 = 'control'
            else:
                label1 = 'c{0}'.format(idx)
                label2 = 'control{0}'.format(idx)

            if label1 in kwargs:
                if label2 in kwargs:
                    raise TypeError(
                        "Duplicate position specification for control qubit "
                        "{0}".format(idx))
                position = kwargs.pop(label1)
            elif label2 in kwargs:
                position = kwargs.pop(label2)
            else:
                continue

            if position < 0 or position > self._n_qubits:
                raise GateDefinitionError(
                    "Invalid position for control qubit {0}: {1}; must be "
                    "between 0 and n_qubits - 1".format(idx, position))

            if position in control_positions:
                raise GateDefinitionError(
                    "Given position for control qubit {0} overlaps with the "
                    "position for control qubit {1}".format(
                        idx, control_positions[position]))
            elif self._input_map[position] is not None:
                raise GateDefinitionError(
                    "Given position for control qubit {0} overlaps with the "
                    "position of the target gate".format(idx))
            else:
                control_positions[position] = idx

        for position in control_positions:
            self._input_map[position] = 'c'

        # Finally, any control qubits whose positions were unspecified will be
        # filled in from top to bottom, floating around any already specified
        # inputs
        if len(control_positions) < self._n_control:
            idx = 0
            for _ in range(self._n_control - len(control_positions)):
                while (self._input_map[idx] is not None and
                        idx < len(self._input_map)):
                    idx += 1
                self._input_map[idx] = 'c'
                idx += 1

        # Currently any other kwargs are undefined
        if kwargs:
            raise TypeError(
                "{0}.__init__ got an unexpected keyword argument "
                "{1!r}".format(self.__class__.__name__, list(kwargs)[0]))



class CnotGate(ControlledGate, _NamedGate):
    """
    CNOT gate class.  Not strictly necessary but useful for explicitly
    identifying CNOT gates in a circuit.
    """

    base_name = 'CNOT'
    latex_name = r'\mathrm{CNOT}'

    def __init__(self, n_qubits=2, target=None, **kwargs):
        super().__init__(
            X, n_control=1, n_qubits=n_qubits, name=self.base_name,
            latex_name=self.latex_name, representation='name', target=target,
            **kwargs)

    # TODO: The argument signature for ControlledGate.__init__ does not match
    # the argument signature for most subclasses of ControlledGate like this
    # one, so we have to copy this __call__ implementation to override
    # ControlledGate.__call__.  This suggests to me the need for reworking how
    # _VariableQubitGate creates new instances
    def __call__(self, n_qubits=None, *args, **kwargs):
        if n_qubits is None:
            n_qubits = self.n_qubits
        kwargs['n_qubits'] = n_qubits
        return self.__class__(*args, **kwargs)


CNOT = CnotGate()
"""
A two qubit CNOT gate.

Parameterized so that calling it creates a new n-qubit CNOT gate that works
in an n-qubit state space (where by default the first qubit is input as the
control and the last qubit is input as the target)::

    >>> CNOT(3)|Ket(1, 0, 0)
    |1, 0, 1>

However, the control and target inputs can also be mapped to different
qubits::

    >>> CNOT(3, c=1, t=2)|Ket(0, 1, 0)
    |0, 1, 1>
"""


class ToffoliGate(ControlledGate, _NamedGate):

    base_name = 'Toffoli'
    latex_name = r'\mathrm{Toffoli}'

    def __init__(self, n_qubits=3, target=None, **kwargs):
        super().__init__(
            X, n_control=2, n_qubits=n_qubits, name=self.base_name,
            latex_name=self.latex_name, representation='name', target=target,
            **kwargs)

    def __call__(self, n_qubits=None, *args, **kwargs):
        if n_qubits is None:
            n_qubits = self.n_qubits
        kwargs['n_qubits'] = n_qubits
        return self.__class__(*args, **kwargs)


Toffoli = ToffoliGate()
"""
Toffoli gate.

Has the same parameterization capabilities as `CNOT`.  In his case the two
control qubit inputs are labeled ``control0`` and ``control1`` or ``c0`` and
``c1`` for short.
"""


class MeasurementGate(_NamedGate):
    """
    Implements a point in a circuit where a measurement in the computational
    basis is performed.  Not generally a quantum gate as such, but in a
    circuit it acts as a Z gate.
    """

    base_name = 'M'

    def __init__(self):
        super().__init__(name=self.base_name, latex_name=self.base_name)

    @property
    def qobj(self):
        """The representation of this operator as a QuTiP `~qutip.Qobj`."""

        return Z.qobj

    @property
    def n_qubits(self):
        """The number of qubits this operator acts on."""

        return 1


M = MeasurementGate()



# TODO: Once arbitrary custom parameterized gates are implemented this should
# be converted to one
class RkGate(GateBase):
    """
    Implements a 'k-th root of unity' rotation used in implementing a QFT.
    """

    def __init__(self, k=0):
        name = 'R_{0}'.format(k)
        latex_name = 'R_{{{0}}}'.format(k)
        super().__init__(name=name, latex_name=latex_name)
        self._k = k

    @property
    def qobj(self):
        """The representation of this operator as a QuTiP `~qutip.Qobj`."""

        return ops.phasegate(2 * np.pi / 2**self._k)

    def __call__(self, k):
        return self.__class__(k)

R_k = RkGate()


def _get_default_gate_inputs(gate):
    """
    Given a gate, determine its *default* inputs.  For now this just handles a
    couple special cases.  Later it would be better to have a general way for a
    multi-qubit gate to communicate how many input qubits it actually takes.
    """

    if isinstance(gate, SwapGate):
        return (0, gate.n_qubits - 1)
    elif isinstance(gate, ControlledGate):
        # TODO: This part *really* needs to be refactored :/
        controls = []
        target = None
        for idx, inp in enumerate(gate._input_map):
            if inp == 'c':
                controls.append(idx)
            elif inp == 't' and target is None:
                target = idx

        n_target = gate._controlled_gate.n_qubits
        return tuple(controls) + tuple(range(target, target + n_target))
    else:
        return tuple(range(gate.n_qubits))
