from __future__ import unicode_literals, absolute_import

import abc
import collections
import operator
import random
import warnings

import qutip
import numpy as np

try:
    from IPython.display import display_latex
    HAVE_IPYTHON = True
except ImportError:
    HAVE_IPYTHON = False

from .utils import ExpressionTree, lazyproperty


__all__ = ['Operator', 'Gate']


BINARY_OPERATOR_ASCII = {
    'tensor': '*',
    'product': '|'
}


BINARY_OPERATOR_UNICODE = {
    'tensor': '\N{CIRCLED TIMES}',
    'product': ''  # No operator is displayed for multiplication
}


BINARY_OPERATOR_LATEX = {
    'tensor': r'\otimes',
    'product': ''  # No operator is displayed for multiplication
}


BINARY_OPERATOR_FUNCS = {
    'tensor': qutip.tensor,
    'product': operator.mul
}


class OperatorNameError(Exception):
    """
    Exception used when an Operator is created with a name already used by
    another Operator.
    """


class OperatorMeta(abc.ABCMeta):
    """
    This metaclass is necessary to override argument handling on the Operator
    initialization.

    In order to check whether to return a Gate, we have to convert the input to
    a Qobj.  But the default behavior of __new__ does not allow us to override
    the input arguments to class's __init__, so we would have to unnecessarily
    convert the input to a Qobj a second time.
    """

    def __call__(cls, *args, **kwargs):
        qobj = args[0]

        if isinstance(qobj, OperatorBase):
            qobj = qobj.qobj
        elif not isinstance(qobj, qutip.Qobj):
            qobj = qutip.Qobj(qobj)

        if qobj.type != 'oper':
            raise ValueError(
                'Input matrix does not represent a quantum operator:\n'
                '{0}'.format(qobj))

        args = (qobj,) + args[1:]
        inst = cls.__new__(cls, *args, **kwargs)
        inst.__init__(*args, **kwargs)
        return inst


class OperatorBase(metaclass=abc.ABCMeta):
    """
    An abstract base class for all operator types.
    """

    registry = {}

    _unnamed_count = 0
    _default_name_base = 'O'

    @abc.abstractmethod
    def __init__(self, name=None, latex_name=None, representation='both',
                 register=False, replace=False):
        super().__init__()

        if register and name is not None:
            if name in OperatorBase.registry:
                if not replace:
                    raise OperatorNameError(
                        "An Operator with the name {0!r} has already been "
                        "defined. Use the replace=True argument to override "
                        "the existing Operator.  The existing Operator will "
                        "continue to work in existing circuits but may not "
                        "be usable in new ones.".format(name))
                else:
                    warnings.warn(
                        "Replacing existing Operator {0!r}.".format(name))
            OperatorBase.registry[name] = self

        if name is None:
            # Generate a name for this operator
            # Make sure the default name is not already used in the
            # registry--this is unlikely, but just in case...
            while name is None or name in OperatorBase.registry:
                subscr = self._unnamed_count
                name = '{0}_{1}'.format(self._default_name_base, subscr)
                self.__class__._unnamed_count = subscr + 1

            if latex_name is None:
                latex_name = r'\hat{{{0}}}_{{({1})}}'.format(
                    self._default_name_base, subscr)

        if latex_name is None:
            if len(name) == 1:
                latex_name = r'\hat{{{0}}}'.format(name)
            else:
                latex_name = name

        self._name = name
        self._latex_name = latex_name
        self._representation = representation

    @classmethod
    def from_binary_operation(cls, binary_op, left, right, name=None,
                              latex_name=None, representation='both',
                              register=False, replace=False):

        # TODO: Implement a means of checking whether an operation between two
        # operators is implemented

        if isinstance(left, _CompoundOperator):
            left_tree = left._tree
        else:
            left_tree = ExpressionTree(left)

        if isinstance(right, _CompoundOperator):
            right_tree = right._tree
        else:
            right_tree = ExpressionTree(right)

        # TODO: Come up with some intelligent defaults for name, latex_name,
        # etc.
        tree = ExpressionTree(binary_op, left_tree, right_tree)
        if isinstance(left, GateBase) and isinstance(right, GateBase):
            cls = _CompoundGate
        else:
            cls = _CompoundOperator

        return cls(tree, name, latex_name, representation,
                   register=register, replace=replace)

    qobj = abc.abstractproperty

    @property
    def name(self):
        """The plain-text name of this operator."""

        return self._name

    # TODO: Maybe rename latex_name to just 'latex'; some gates will have latex
    # representations but not necessarily any 'name'
    @property
    def latex_name(self):
        """
        The LaTeX-formatted name for this operator.

        Used for display purposes only.
        """

        if self._latex_name is not None:
            return self._latex_name

        return self.format_latex('matrix')

    @lazyproperty
    def n_qubits(self):
        return int(np.log2(self.qobj.shape[0]))

    @property
    def dimension(self):
        """The dimension of the Hilbert space this operator acts on."""

        return 2 ** self.n_qubits

    def __eq__(self, other):
        """
        Operator equivalence is determined by equivalence of their matrix
        elements.
        """

        if isinstance(other, OperatorBase):
            return self.qobj == other.qobj
        elif isinstance(other, qutip.Qobj):
            return self.qobj == other
        else:
            return False

    def __or__(self, other):
        if not isinstance(other, OperatorBase):
            return NotImplemented

        if self.dimension != other.dimension:
            raise ValueError('operator dimensions incompatible: {0}; '
                             '{1}'.format(self.dimension, other.dimension))
        return self.from_binary_operation('product', self, other)

    def __mul__(self, other):
        return self.from_binary_operation('tensor', self, other)

    def __repr__(self):
        if not hasattr(self, '_name'):
            # Has not been initialized yet; this is primarily an escape gasket
            # for debugging
            return super().__repr__()
        return self.format_text()

    def _repr_latex_(self):
        return '$${0}$$'.format(self.format_latex())

    def format_text(self, representation=None):  # unicode=False):
        """
        Return a plain-text expression for this operator.

        Used primarily for displaying operators in the Python command-line.

        Parameters
        ----------
        representation : `str`
            Same as the ``representation`` argument to the `Operator`
            constructor.  This can be used to output a plain text format for
            this operator in different representations.
        """
        # TODO: Unicode support
        if representation is None:
            representation = self._representation

        if representation == 'name':
            return self.name
        elif representation == 'matrix':
            return self._format_text_matrix()
        elif representation == 'both':
            # There's a point at which if the operator's name is too long (such
            # as a long expression) where the Numpy array formatting gets
            # squashed into too little horizontal space.  The thrshold of 10 is
            # somewhat arbitrary here but it helps with such cases
            if len(self.name) > 10:
                return '{0} = \\\n{1}'.format(
                    self.name, self._format_text_matrix(prefix='    '))
            return self._format_text_matrix(prefix=self.name + ' = ')

        raise ValueError('Unknown text representation format {0!r}'.format(
            representation))

    def format_latex(self, representation=None):
        """
        Return a LaTeX expression for this operator.

        Parameters
        ----------
        representation : `str`
            Same as the ``representation`` argument to the `Operator`
            constructor.  This can be used to output the LaTeX formatting
            for this operator in different representations.

        Notes
        -----
        This can be used to display the operator in different formats in the
        IPython Notebook like so::

            >>> from IPython.display import Latex
            >>> A = Operator([[1, 0], [0, 1]], name='A')
            >>> Latex(A.format_latex(representation='name'))
            ...
        """

        if representation is None:
            representation = self._representation

        if representation == 'name':
            return self.latex_name
        elif representation == 'matrix':
            return self._format_latex_matrix()
        elif representation == 'both':
            return '{0} = {1}'.format(self.latex_name,
                                      self._format_latex_matrix())

        raise ValueError('Unknown LaTeX representation format {0!r}'.format(
            representation))

    if HAVE_IPYTHON:
        def display_latex(self, representation=None):
            latex = self.format_latex(representation=representation)
            display_latex('$${0}$$'.format(latex), raw=True)

    def _format_text_matrix(self, prefix=''):
        """
        Implement plain text representation of an Operator's matrix elements;
        currently this simply displays them as a Numpy array.
        """

        # TODO: But with unicode support we may be able to draw this as ASCII
        # art

        if self.qobj.shape[0] > 10000 or self.qobj.shape[1] > 10000:
            array = self.qobj.data
        elif np.all(np.imag(self.qobj.data.data) == 0):
            array = np.real(self.qobj.full())
        else:
            array = self.qobj.full()

        return prefix + np.array2string(array, prefix=prefix)

    def _format_latex_matrix(self):
        """
        Returns the LaTeX for a matrix representation of this operator.

        This reuses the work already done in Qobj._repr_latex_ but strips
        off the heading to expose just the pmatrix array.
        """

        r = self.qobj._repr_latex_()
        # Older version sof QuTiP used \begin{pmatrix} but it seems newer
        # versions do not
        # TODO: Using a regexp might make this more robust.
        r = r[r.find(r'\left'):].rstrip('$')
        return r.rsplit(r'\end{equation*}')[0]


class Operator(OperatorBase, metaclass=OperatorMeta):
    """
    Generic quantum operator type.

    In general, `Operator` instances are created by providing the elements of a
    2^n x 2^b matrix used to evaluate the result of applying that operator to a
    state in the computational basis.  However, as PyQC `Ket` vectors are
    always normalized automatically, the result of an `Operator` applied to a
    `Ket` is normalized regardless of the matrix elements.

    The matrix can be input either as a `list` of Python lists (the elements of
    which are any numeric type including `complex`), a Numpy `~numpy.ndarray`,
    or a QuTiP `~qutip.Qobj` (of type ``'operator'``).  An `Operator` can also
    be instantiated from an existing `Operator` (this can be used, for example,
    to create an `Operator` with the same action but a different name).

    See the full PyQC documentation for more details.

    Parameters
    ----------
    qobj : `list`, `Operator`, `~qutip.Qobj`, `~numpy.ndarray`
        The elements of a 2^n x 2^n matrix, where n is the number of qubits
        the operator acts on.
    name : `str`
        The plain-text display name for this operator.  Should generally be
        unique within a script or interactive session, but uniqueness is not
        required.
    latex_name : `str`
        An alternative named used for this operator, used exclusively when
        generating a LaTeX representation of the operator (such as in the
        IPython Notebook).
    representation : `str`
        The default display format for this operator.  The default, `'both'`,
        displays both the operator's name and its matrix elements.  `'name'`
        displays just the name.  `'matrix'` displays just the matrix elements.
    register : `bool`
        Register this operator in PyQC's internal registry of "known"
        operators, which maps operator names to their implementations.  Every
        operator in the registry must have a unique name.
    replace : `bool`
        If `True`, when used in conjunction with ``register=True``, replace
        any existing operator of the same name in the registry with this
        operator.  This can be used to override operators already in the
        registry.

    Examples
    --------

    Instantiate an operator named 'A' with the matrix elements A_11 = 1,
    A_12 = 0, A_21 = 0, and A_22 = 1 (in other words, equivalent to the
    Pauli X operator)::

        >>> A = Operator([[0, 1], [1, 0]], name='A')

    Apply an operator to a ket vector::

        >>> A|Ket(0)
        |1>

    Apply an operator to itself (or another operator) to create a new
    operator::

        >>> A | A
        A | A = [[ 1.  0.]
                 [ 0.  1.]]

    Create a new operator from the tensor product of two operators::

        >>> A * A
        A * A = [[ 0.  0.  0.  1.]
                 [ 0.  0.  1.  0.]
                 [ 0.  1.  0.  0.]
                 [ 1.  0.  0.  0.]]
    """

    def __new__(cls, *args, **kwargs):
        qobj = args[0]
        rows, cols = qobj.shape

        if rows >= 2 and (rows & (rows - 1) == 0) and is_unitary(qobj):
            # For now, always return a Gate for unitaries; this actually
            # provides an interesting way to check if a matrix can represent a
            # quantum gate--just create an Operator from it and see if you get
            # a Gate
            # Gates should also be a power of 2 on both sides
            if not issubclass(cls, Gate):
                # If the class is already Gate or a subclass thereof use that
                # class
                cls = Gate
        elif issubclass(cls, Gate):
            # The Gate class or a subclass of it was used directly, but the
            # input was not unitary--this is not allowed
            raise ValueError(
                'Input matrix for {0} must be a 2^n x 2^n unitary:\n'
                '{1}'.format(cls.__name__, qobj))

        return super().__new__(cls)

    def __init__(self, qobj, name=None, latex_name=None,
                 representation='both', **kwargs):
        super().__init__(
            name=name, latex_name=latex_name, representation=representation,
            **kwargs)
        self._qobj = qobj

    @property
    def qobj(self):
        """The representation of this operator as a QuTiP `~qutip.Qobj`."""

        return self._qobj


class _CompoundOperator(OperatorBase):

    # TODO: This is the default for now, but enable unicode by default wherever
    # possible
    _use_unicode = False

    def __init__(self, tree, name=None, latex_name=None,
                 representation='both', register=False, replace=False,
                 **kwargs):
        self._tree = tree

        # Temporary defaults
        if name is None:
            name = ''

        if latex_name is None:
            latex_name = ''

        super().__init__(
            name=name, latex_name=latex_name, representation=representation,
            register=register, replace=replace, **kwargs)

    @property
    def name(self):
        if not self._name:
            self._name = self._format_expression()

        return self._name

    @property
    def latex_name(self):
        if not self._latex_name:
            self._latex_name = self._format_expression(latex=True)

        return self._latex_name

    @lazyproperty
    def qobj(self):
        return self._tree.evaluate(BINARY_OPERATOR_FUNCS, lambda op: op.qobj)

    @property
    def n_qubits(self):
        return int(np.log2(self.dimension))

    @property
    def dimension(self):
        """The dimension of the Hilbert space this operator acts on."""

        # Walk the tree taking the products of the dimensions of both sides of
        # each tensor product encountered.  When a normal product is
        # encountered just walk (without loss of generality) the left subtree
        # under the product.
        #
        # For an expression like (A * B)|(C * D), that means this will return
        # the dimension of (A * B), but that must be the same as the right side
        # of the | operator (C * D), so we're done.
        stack = collections.deque([self._tree])
        dimension = 1
        while stack:
            tree = stack.pop()
            if tree.value == 'tensor':
                # We have to determine the dimensions of both sides
                stack.append(tree.left)
                stack.append(tree.right)
            elif tree.isleaf:
                dimension *= tree.value.dimension
            else:
                # A product--only need the dimension of one side; so without
                # loss of generality take the left
                stack.append(tree.left)

        return dimension

    # TODO: Unicode support
    def format_text(self, representation=None): #, unicode=False):
        if representation is None:
            representation = self._representation

        if representation == 'expression':
            return self._format_expression()

        return super().format_text(representation) #unicode)

    def format_latex(self, representation=None):
        if representation is None:
            representation = self._representation

        if representation == 'expression':
            return self._format_expression(latex=True)

        return super().format_latex(representation=representation)

    def _format_expression(self, latex=False):
        if latex:
            chars = BINARY_OPERATOR_LATEX
        else:
            if self._use_unicode:
                chars = BINARY_OPERATOR_UNICODE
            else:
                chars = BINARY_OPERATOR_ASCII

        output = []

        def append_node(node):
            if node.isleaf:
                if latex:

                    output.append(
                        node.value.format_latex(representation='name'))
                else:
                    output.append(node.value.name)
                return

            oper = node.value

            if node.left is not None:
                if oper == 'product' and node.left.value == 'tensor':
                    output.append('(')
                    append_node(node.left)
                    output.append(')')
                else:
                    append_node(node.left)

            char = chars[oper]
            if char:
                # Account for multiplication, which usually doesn't display
                # an operator at all (its char is '')
                output.append(' {0} '.format(char))

            if node.right is not None:
                if oper == 'product' and node.right.value == 'tensor':
                    output.append('(')
                    append_node(node.right)
                    output.append(')')
                else:
                    append_node(node.right)

        append_node(self._tree)

        return ''.join(output)


class GateBase(OperatorBase):
    """
    A Gate is a special class of Operator that must be unitary, and that can be
    built up from a circuit of other Gates.
    """

    _default_name_base = 'U'

    @abc.abstractmethod
    def __init__(self, name=None, latex_name=None, representation='both',
                 labels=None, register=False, replace=False):
        super().__init__(
            name=name, latex_name=latex_name, representation='both',
            register=register, replace=replace)
        # TODO: Format checking on labels; we also need to disallow any labels
        # that would overshadow built-in attributes; or we could consider an
        # alternative interface to getting to qubit labels, but direct
        # attribute access is the least verbose and simplest for now
        self._custom_labels = []
        self._label_aliases = {}
        if labels is not None:
            for label in labels:
                if isinstance(label, tuple):
                    label, aliases = label[0], label[1:]
                    for alias in aliases:
                        self._label_aliases[alias] = label
                self._custom_labels.append(label)

    @lazyproperty
    def labels(self):
        """
        List of the labels for inputs to this gate.

        This is currently only used by `Circuit.from_wires` to specify inputs
        to multi-qubit gates.
        """

        # If the number of labels in self._custom_labels is fewer than
        # n_qubits, use default names qubitN, qubitN+1, etc.
        # TODO: Provide an API for labeling only qubits, say, in the middle;
        # currently we requiring labels to start from the first qubit and only
        # subsequent ones are filled in by default
        labels = self._custom_labels
        n_qubits = self.n_qubits
        if len(labels) < n_qubits:
            labels.extend(['qubit{0}'.format(n)
                           for n in range(len(labels), n_qubits)])
        return labels

    @lazyproperty
    def label_aliases(self):
        """
        A `dict` mapping aliases for `labels`.

        Each key is an alias, while the value is the `label` for which the key
        is an allowed alias.
        """

        # All labels automatically get an alias of qn
        for idx, label in enumerate(self.labels):
            self._label_aliases['q{0}'.format(idx)] = label

        return self._label_aliases

    def __getattr__(self, attr):
        """Support attribute access to labels."""

        if attr in self.labels:
            return _InputLabel(attr, self)
        elif attr in self.label_aliases:
            return _InputLabel(self.label_aliases[attr], self)

        raise AttributeError(attr)

    def evaluate(self, *args, **kwargs):
        """
        Evaluate the gate on the given input qubit(s) and return an output
        state, possibly at random if some of the qubits were left in a
        superposition.

        Accepts the same arguments as the `Ket` constructor.
        """

        # TODO: Refactor so that this in-line import is not necessary
        from .braket import Ket

        input_ = Ket(*args, **kwargs)

        # Apply the gate/circuit
        output = (self|input_).qobj
        probabilities = (output.full()*output.full().conjugate()).real

        # TODO: This could be more time consuming, but we could determine
        # exactly which qubits were left in superpositions
        definite = np.where(probabilities == 1.0)[0]
        if not len(definite):
            warnings.warn(
                'Some qubits were left in superposition; output not '
                'deterministic.')

            cum_prob = probabilities.cumsum()
            rand = random.uniform(0, 1)
            # TODO: Technically this can fail if random.uniform returns exactly
            # 1.0.  It might fine to just generate another random number when
            # that happens, but it is so unlikely that we don't bother for now
            assert rand != 1.0
            result_val = np.where(cum_prob > rand)[0][0]
        else:
            result_val = definite[0]

        result = Ket(result_val, n_qubits=input_.n_qubits)
        result._representation = 'binary'
        return result


class Gate(Operator, GateBase):
    """
    Generic quantum gate instantiated from its matrix elements.

    The first argument must be a unitary matrix.  Other than that, this class
    is identical to (and just a special case of) the `Operator` class.  In fact
    when an `Operator` is instantiated with a matrix that happens to be
    unitary, it is automatically "promoted" to a `Gate`.
    """

    def __init__(self, qobj, name=None, latex_name=None,
                 representation='both', **kwargs):
        super().__init__(
            qobj, name=name, latex_name=latex_name, representation='both',
            **kwargs)
        # Set the dimensionality on the internal qobj to be consistent with
        # the dimensions required for a gate on n qubits
        self._qobj.dims = [[2] * self.n_qubits] * 2


class _CompoundGate(_CompoundOperator, GateBase):
    """Class representing a Gate composed of an expression of other Gates."""

# This is used internally for referencing inputs to multi-qubit gates
# name is the name of the label, and gate refers back to the the actual Gate
# instance for which this is a label
_InputLabel = collections.namedtuple('InputLabel', ['name', 'gate'])


def is_unitary(qobj):
    if qobj.shape[0] != qobj.shape[1]:
        return False

    return np.allclose((qobj * qobj.dag()).tr(), qobj.shape[0])
