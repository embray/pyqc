from itertools import chain, product

import pytest

import numpy as np
from numpy.testing import assert_almost_equal
import qutip

from ..braket import Bra, Ket
from ..operator import Operator


@pytest.mark.parametrize('value', [
    (0,), (1,), (0, 0), (0, 1), (1, 0), (1, 1)
])
def test_ket_from_bits(value):
    k = Ket(*value)
    value_str = ''.join(str(i) for i in value)
    assert k.value == int(value_str, 2)
    assert k.n_qubits == len(value)
    assert repr(k) == '|{0}>'.format(', '.join(value_str))
    assert k._repr_latex_() == r'$\left|{0}\right>$'.format(value_str)


@pytest.mark.parametrize('value', [
    (0,), (1,), (0, 0), (0, 1), (1, 0), (1, 1)
])
def test_bra_from_bits(value):
    b = Bra(*value)
    value_str = ''.join(str(i) for i in value)
    assert b.value == int(value_str, 2)
    assert b.n_qubits == len(value)
    assert repr(b) == '<{0}|'.format(', '.join(value_str))
    assert b._repr_latex_() == r'$\left<{0}\right|$'.format(value_str)


@pytest.mark.parametrize('value', range(2, 30))
def test_ket_from_decimal(value):
    k = Ket(value)
    assert k.value == value
    assert k.n_qubits == int(np.floor(np.log2(value)) + 1)
    assert repr(k) == '|{0}>'.format(value)
    assert k._repr_latex_() == r'$\left|{0}\right>$'.format(value)


@pytest.mark.parametrize('value', range(2, 30))
def test_bra_from_decimal(value):
    b = Bra(value)
    assert b.value == value
    assert b.n_qubits == int(np.floor(np.log2(value)) + 1)
    assert repr(b) == '<{0}|'.format(value)
    assert b._repr_latex_() == r'$\left<{0}\right|$'.format(value)


def test_ket_of_n_qubits():
    k = Ket(0, n_qubits=2)
    assert k.value == 0
    assert k.n_qubits == 2
    assert k.dimension == 4
    assert k.qobj.shape == (4, 1)
    assert k.qobj.dims == [[2, 2], [1, 1]]
    assert repr(k) == '|0, 0>'

    k = Ket(4, n_qubits=4)
    assert k.value == 4
    assert k.n_qubits == 4
    assert k.dimension == 16
    assert k.qobj.shape == (16, 1)
    assert k.qobj.dims == [[2, 2, 2, 2], [1, 1, 1, 1]]
    assert repr(k) == '|0, 1, 0, 0>'


@pytest.mark.parametrize('amplitude', (None, 0, 1, 2))
def test_ket_superposition(amplitude):
    """
    Test equal superpositions, including case where amplitude is > 1 but
    otherwise equal in all elements of the superposition.
    """

    # A 'superposition' of the same state should just result in the same state
    if amplitude == 0:
        assert (amplitude | Ket(0)) + (amplitude | Ket(0)) == 0
        return
    elif amplitude is None:
        k = Ket(0) + Ket(0)
    else:
        k = (amplitude | Ket(0)) + (amplitude | Ket(0))

    assert k.value == 0
    assert repr(k) == '|0>'
    assert k._repr_latex_() == r'$\left|0\right>$'

    # But the sum of two different states should give an equal superposition
    # of those states
    if amplitude is None:
        k = Ket(0) + Ket(1)
    else:
        k = (amplitude | Ket(0)) + (amplitude | Ket(1))
    assert k.value == (0, 1)
    assert repr(k) == 'sqrt(1/2)(|0> + |1>)'
    assert k._repr_latex_() == (r'\begin{equation}'
                                r'\frac{\left|0\right> + \left|1\right>}'
                                r'{\sqrt{2}}\end{equation}')
    assert k.qobj == qutip.Qobj([[np.sqrt(0.5)], [np.sqrt(0.5)]])

    # Ditto for multiple qubit states
    if amplitude is None:
        k = Ket(0, 0) + Ket(1, 1)
    else:
        k = (amplitude | Ket(0, 0)) + (amplitude | Ket(1, 1))
    assert k.value == (0, 3)
    assert k.n_qubits == 2
    assert repr(k) == 'sqrt(1/2)(|0, 0> + |1, 1>)'
    assert k._repr_latex_() == (r'\begin{equation}\frac{'
                                r'\left|00\right> + \left|11\right>'
                                r'}{\sqrt{2}}\end{equation}')
    assert k.qobj == qutip.Qobj([[np.sqrt(0.5)], [0], [0], [np.sqrt(0.5)]],
                                dims=[[2, 2], [1, 1]])

    k = Ket(0, 0) + Ket(0, 1) + Ket(1, 0) + Ket(1, 1)
    if amplitude is None:
        k = Ket(0, 0) + Ket(0, 1) + Ket(1, 0) + Ket(1, 1)
    else:
        k = ((amplitude | Ket(0, 0)) + (amplitude | Ket(0, 1)) +
             (amplitude | Ket(1, 0)) + (amplitude | Ket(1, 1)))
    assert k.value == (0, 1, 2, 3)
    assert k.n_qubits == 2
    assert repr(k) == 'sqrt(1/4)(|0, 0> + |0, 1> + |1, 0> + |1, 1>)'
    # TODO: For now this displays sqrt(4) in the denominator; eventually add
    # code to factor that value properly
    assert k._repr_latex_() == (r'\begin{equation}\frac{'
                                r'\left|00\right> + \left|01\right> + '
                                r'\left|10\right> + \left|11\right>'
                                r'}{\sqrt{4}}\end{equation}')
    assert k.qobj == qutip.Qobj([[0.5]] * 4, dims=[[2, 2], [1, 1]])


def test_invalid_add():
    """Test adding a bra to a ket and vice versa; this should fail."""

    with pytest.raises(TypeError):
        Bra(0) + Ket(0)

    with pytest.raises(TypeError):
        Ket(0) + Bra(0)


def test_ket_tensor_product():
    """
    Test that tensor products of ket vectors produces a higher-dimensional
    ket vector.
    """

    t1 = Ket(0) * Ket(0)
    assert isinstance(t1, Ket)
    assert t1.value == 0
    assert t1.n_qubits == 2
    assert t1.qobj.shape == (4, 1)
    assert t1.qobj.dims == [[2, 2], [1, 1]]
    assert t1._repr_latex_() == r'$\left|00\right>$'

    t2 = Ket(2) * Ket(0)
    assert isinstance(t2, Ket)
    assert t2.value == 4
    assert t2.n_qubits == 3
    assert t2.qobj.shape == (8, 1)
    assert t2.qobj.dims == [[2, 2, 2], [1, 1, 1]]
    assert t2._repr_latex_() == r'$\left|4\right>$'

    t3 = Ket(2) * Ket(2)
    assert isinstance(t3, Ket)
    assert t3.value == 10
    assert t3.n_qubits == 4
    assert t3.qobj.shape == (16, 1)
    assert t3.qobj.dims == [[2, 2, 2, 2], [1, 1, 1, 1]]
    assert t3._repr_latex_() == r'$\left|10\right>$'


@pytest.mark.parametrize('value', [0, 1, 2])
def test_ket_negation(value):
    k = -Ket(value)
    assert k.value == value
    assert k != Ket(value)
    assert repr(k) == '-|{0}>'.format(value)
    assert k._repr_latex_() == r'$-\left|{0}\right>$'.format(value)


@pytest.mark.parametrize('value,amplitude', product((0, 1, 2), (0, 1, 2)))
def test_ket_non_unit_amplitude(value, amplitude):
    """
    Test scalar multiplication with Kets; a single Ket is still a unit vector
    by default so multiplying by a scalar with magnitude greater than 1 has
    no visible effect.  But it is still tracked internally for creating inequal
    superpositions.
    """

    k1 = amplitude|Ket(value)
    k2 = (amplitude + 1)|Ket(value)
    k3 = -amplitude|Ket(value)
    k4 = -(amplitude + 1)|Ket(value)

    if amplitude == 0:
        assert k1 == k3 == 0
        return

    assert k1.value == k2.value == k3.value == k4.value == value
    assert k1 == k2  # Kets with the same *sign* should compare equal
    assert k3 == k4
    assert k1 != k3  # But not with different sign
    assert k1 != k4
    assert k2 != k3
    assert k2 != k4

    assert repr(k1) == '|{0}>'.format(value)
    assert repr(k1) == repr(k2)
    assert k1._repr_latex_() == r'$\left|{0}\right>$'.format(value)
    assert k1._repr_latex_() == k2._repr_latex_()

    assert repr(k3) == '-|{0}>'.format(value)
    assert repr(k3) == repr(k4)
    assert k3._repr_latex_() == r'$-\left|{0}\right>$'.format(value)
    assert k3._repr_latex_() == k4._repr_latex_()


def test_ket_subtraction():
    # Subtracting a Ket from itself should result in zero
    assert Ket(0) - Ket(0) == 0

    # Otherwise subtracting a Ket from a different Ket should give a
    # superposition
    k = Ket(0) - Ket(1)
    assert k.value == (0, 1)
    assert repr(k) == 'sqrt(1/2)(|0> - |1>)'
    assert k._repr_latex_() == (r'\begin{equation}'
                                r'\frac{\left|0\right> - \left|1\right>}'
                                r'{\sqrt{2}}\end{equation}')
    assert k.qobj == qutip.Qobj([[np.sqrt(0.5)], [-np.sqrt(0.5)]])

    k = Ket(0) + Ket(1) - Ket(0)
    assert k.value == 1
    assert repr(k) == '|1>'


@pytest.mark.parametrize('bra,ket', chain(
    product(product((0, 1)), product((0, 1))),
    product(product((0, 1), (0, 1)), product((0, 1), (0, 1)))))
def test_bra_ket_multiplication(bra, ket):
    if bra == ket:
        assert Bra(*bra) | Ket(*ket) == 1.0
    else:
        assert Bra(*bra) | Ket(*ket) == 0.0

    # Test out superpositions too
    if len(bra) > 1:
        b = sum((Bra(b) for b in bra[1:]), Bra(bra[0]))
        k = sum((Ket(k) for k in ket[1:]), Ket(ket[0]))
        if sorted(bra) == sorted(ket):
            assert b | k == 1.0
        elif (bra[0] == bra[1]) != (ket[0] == ket[1]):
            assert_almost_equal(b | k, 2**-0.5)
        else:
            assert b | k == 0.0

    with pytest.raises(TypeError):
        Bra(*bra) | Bra(*bra)

    with pytest.raises(TypeError):
        Ket(*ket) | Ket(*ket)


def test_operator_from_outer_product():
    """
    Check that the outer product of a ket with a bra vector produces an
    operator.
    """

    o = Ket(0)|Bra(1)
    assert isinstance(o, Operator)
    assert np.all(o.qobj.full() == [[0, 1], [0, 0]])
    assert o.dimension == 2
    assert o.qobj.shape == (2, 2)
    assert o.qobj.dims == [[2], [2]]


def test_operate_on_ket():
    """
    Test operating on a ket with an either a pyqc or qutip operator to produce
    a new state vector.
    """

    k = Ket(0)

    # An arbitrary operator...
    o = Operator([[1, 2], [3, 4]])

    expected = np.array([[1, 3]]).T / np.linalg.norm([1, 3])

    result = o | k
    assert isinstance(result, Ket)
    assert result == Ket(0) + (3 | Ket(1))
    assert result.qobj.type == 'ket'
    assert result.qobj.shape == (2, 1)
    assert result.qobj.dims == [[2], [1]]
    assert np.allclose(result.qobj.full(), expected)

    result = o.qobj | k
    assert isinstance(result, Ket)
    assert result == Ket(0) + (3 | Ket(1))
    assert result.qobj.type == 'ket'
    assert result.qobj.shape == (2, 1)
    assert result.qobj.dims == [[2], [1]]
    assert np.allclose(result.qobj.full(), expected)

    # Operating on a ket with a ket--this should fail
    with pytest.raises(TypeError) as exc:
        result | k
        assert 'unsupported operand type(s)' in exc.value.args[0]
        assert "of type 'Ket'" in exc.value.args[0]


def test_operate_on_ket_round_to_one():
    """
    Assert that the inner product of a Ket vector and its dual is exactly one,
    and that the inner product of two unrelated basis vectors is zero.
    """

    assert qutip.Qobj([[1.0, 0]]) | Ket(0) == 1.0
    assert qutip.Qobj([[1.0, 0]]) | Ket(1) == 0.0
