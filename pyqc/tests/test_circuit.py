import pytest

from ..braket import Ket
from ..circuit import Circuit, CircuitDefinitionError
from ..gates import I, H, S, T, Y, Z, CNOT, SWAP, ControlledGate, Gate


def test_incomplete_multiqubit():
    """
    All the inputs to a multi-qubit gate should be specified at a given time
    step in the circuit definition.  Test that if this case is handled
    with an appropriate error.
    """

    with pytest.raises(CircuitDefinitionError):
        Circuit.from_wires(
            [H, CNOT.target, H],
            [H, CNOT.target, H])


def test_duplicate_multiqubit():
    """
    Two of the same type of multi-qubit gate should not be used at the same
    time step in a circuit due to the inherent ambiguity of which inputs go
    with which gates.
    """

    with pytest.raises(CircuitDefinitionError):
        Circuit.from_wires(
            [CNOT.target], [CNOT.target],
            [CNOT.control], [CNOT.control])


def test_circuit_gate_equivalence():
    """
    Test that the circuit definition format produces Circuit objects
    equivalent to Gates created from expressions of multiple Gates.
    """

    # Single gate "circuit"
    assert Circuit([(H, 0)]) == H
    assert Circuit([(CNOT, (0, 1))]) == CNOT

    # Arbitrary two qubit gate
    G = H * T
    assert Circuit([(H, 0), (T, 1)]) == G
    # Also try using G directly in a circuit definition
    assert Circuit([(G, (0, 1))]) == G

    # Circuit with implicit swap
    c = Circuit([(CNOT, (1, 0))])
    assert c == SWAP|CNOT|SWAP
    assert c == CNOT(target=0)


TEST_QASM_1 = """
# Simple circuit for a 3-qubit QFT
    def c-S,1,'S'
    def c-T,1,'T'

    qubit   j0
    qubit   j1
    qubit   j2

    h   j0
    c-S j1,j0
    c-T j2,j0
    nop j1
    h   j1
    c-S j2,j1
    h   j2
    swap    j0,j2
"""


def test_circuit_from_qasm():
    """
    Basic test that a simple Circuit object can be constructed from a
    QASM script.
    """

    CS = ControlledGate(S)
    CT = ControlledGate(T)

    c = Circuit([
        (H, 0),
        (CS, (1, 0)),
        (CT, (2, 0)),
        (H, 1),
        (CS, (2, 1)),
        (H, 2),
        (SWAP, (0, 2))
    ])

    c2 = Circuit.from_wires(
        [H, CS.t, CT.t, I,    I, I, SWAP.q0],
        [I, CS.c,    I, H, CS.t, I,       I],
        [I,    I, CT.c, I, CS.c, H, SWAP.q1])

    c3 = Circuit.from_qasm(TEST_QASM_1)

    assert c == c2 == c3
    assert c.format_latex('circuit') == c2.format_latex('circuit')


def test_circuit_with_placeholder_gate():
    """
    This test is adapted from the circuites.ipynb notebook and is a regression
    test for a bug which broke this case.
    """

    sample_qasm = """\
         defbox    U_0,2,0,'U_0'

         qubit     q0
         qubit     q1

         H         q0
         H         q1
         U_0       q0,q1
         H         q0
         H         q1
    """

    with pytest.warns(UserWarning,
                      match=r'Input QASM defines the following gates '
                            r'unrecognized by pyqc:\s*U_0'):
        C = Circuit.from_qasm(sample_qasm)

    # Evaluating the circuit should produce an exception
    with pytest.raises(NotImplementedError,
            match="The gate 'U_0' is currently a placeholder for an "
                  "undefined gate."):
        C | Ket(0, 0)

    assert "The gate 'U_0' is currently a placeholder for an undefined gate."

    # Registering the gate should make it work
    U_0 = Gate(Z * Z, name='U_0', register=True)
    assert C | Ket(0, 0) == Ket(1, 1)

    # Try evaluating again; there was a bug where it would work once after
    # registering the gate, but not a second time...?
    assert C | Ket(0, 0) == Ket(1, 1)

    # If the gate is re-registered, the previous definition of this gate should
    # still be the one used in the circuit
    with pytest.warns(UserWarning, match="Replacing existing Operator 'U_0'"):
        U_0 = Gate(Y * Y, name='U_0', register=True, replace=True)

    assert C | Ket(0, 0) == Ket(1, 1)

    # But a new circuit built from the same QASM should use the new definition
    C2 = Circuit.from_qasm(sample_qasm)
    assert C2 | Ket(0, 0) == -Ket(1, 1)
