import numpy as np

import pytest

from ..braket import Ket
from ..gates import *
from ..gates import GateDefinitionError
from ..operator import Gate


def test_identity():
    assert I|Ket(0) == Ket(0)
    assert I|Ket(1) == Ket(1)
    assert I(2)|Ket(0, 0) == Ket(0, 0)


def test_x_gate():
    assert X|Ket(0) == Ket(1)
    assert X|Ket(1) == Ket(0)


def test_y_gate():
    # TODO: Implement comparison that ignores global phase (though perhaps not
    # the eigenvalues)
    # assert Y | Ket(0) == Ket(0)
    # assert Y | Ket(1) == -Ket(1)

    assert Y|Ket(0) == 1j * Ket(1)
    assert Y|Ket(1) == -1j * Ket(0)
    assert Y|Ket(1) != Ket(0)
    assert Y|Ket(1) != 1j * Ket(0)


def test_z_gate():
    assert Z|Ket(0) == Ket(0)
    assert Z|Ket(1) == -Ket(1)
    assert Z|Ket(1) != Ket(0)


def test_s_gate():
    assert S|Ket(0) == Ket(0)

    # TODO: Again, phase shouldn't matter
    # assert S|Ket(1) == Ket(1)
    assert S|Ket(1) == 1j * Ket(1)

    # But, let's check that the qobj of the state does include the phase factor
    assert np.allclose((S|Ket(1)).qobj.full(), [[0], [1j]])


def test_t_gate():
    assert T|Ket(0) == Ket(0)

    # TODO: Again, phase shouldn't matter
    # assert T|Ket(1) == Ket(1)
    assert T|Ket(1) == ((1 + 1j)/np.sqrt(2)) * Ket(1)

    # But, let's check that the qobj of the state does include the phase factor
    assert np.allclose((T|Ket(1)).qobj.full(), [[0], [(1 + 1j)/np.sqrt(2)]])


def test_h_gate():
    assert H|Ket(0) == Ket(0) + Ket(1)
    # Ensure the relative phase is not ignored
    assert H|Ket(0) != Ket(0) - Ket(1)
    assert H|Ket(1) == Ket(0) - Ket(1)


# Note: Testing of multi-qubit and controlled gates is a bit spotty for now as
# I think about reworking their interface a bit
def test_swap_gate():
    assert SWAP|Ket(0, 0) == Ket(0, 0)
    assert SWAP|Ket(1, 1) == Ket(1, 1)

    assert SWAP|Ket(0, 1) == Ket(1, 0)
    assert SWAP(3)|Ket(0, 0, 1) == Ket(1, 0, 0)

    with pytest.raises(TypeError):
        SWAP|Ket(0)

    with pytest.raises(TypeError):
        SWAP|Ket(1)


def test_swap_gate_2():
    """
    Tests a particular case that came up as a regression where the swapped
    state represents a value that requires a greater *minimum* number of qubits
    to represent that value than the original state.  For example
    |011> -> |110>

    Note: Turns out this test worked all along *with a 64-bit version of
    Numpy*; with 32-bit Numpy there was roundoff error on the log2 function
    with single-precision floats that caused this bug.
    """

    assert SWAP(3)|Ket(0, 1, 1) == Ket(1, 1, 0)
    assert SWAP(3)|Ket(0, 0, 1) == Ket(1, 0, 0)


def test_invalid_control_gates():
    """Test various error conditions in the definitions of controlled gates."""

    with pytest.raises(GateDefinitionError) as exc_info:
       ControlledGate(X, n_control=0)

    assert 'at least 1 control qubit' in str(exc_info.value)

    with pytest.raises(GateDefinitionError) as exc_info:
        ControlledGate(X, n_control=-1)

    assert 'at least 1 control qubit' in str(exc_info.value)

    with pytest.raises(GateDefinitionError) as exc_info:
        ControlledGate(X, n_control=3, n_qubits=3)

    assert ('must operate on at least a 4-qubit state space' in
            str(exc_info.value))

    with pytest.raises(GateDefinitionError) as exc_info:
        ControlledGate(X, target=3)

    assert ('does not leave enough space for the 1-qubit target gate' in
            str(exc_info.value))

    with pytest.raises(GateDefinitionError) as exc_info:
        ControlledGate(X * X, target=2)

    assert ('does not leave enough space for the 2-qubit target gate' in
            str(exc_info.value))

    with pytest.raises(TypeError):
        # Duplicate positions for control qubit 0
        ControlledGate(X, n_control=2, c0=0, control0=0)

    with pytest.raises(GateDefinitionError) as exc_info:
        ControlledGate(X, control=4)

    assert 'Invalid position for control qubit 0' in str(exc_info.value)

    with pytest.raises(GateDefinitionError) as exc_info:
        ControlledGate(X, n_control=2, c0=0, c1=0)

    assert ('position for control qubit 1 overlaps with the position for '
            'control qubit 0' in str(exc_info.value))

    with pytest.raises(GateDefinitionError) as exc_info:
        # TODO: If the position of the control qubit is specified and *not* the
        # position of the target qubit, maybe the target should be
        # automatically determined (just when the target is specified but not
        # the control, the control is automatically moved)
        ControlledGate(X, control=1)

    assert ('control qubit 0 overlaps with the position of the target '
            'gate' in str(exc_info.value))

    with pytest.raises(TypeError):
        ControlledGate(X, target=0, control=1, foobar='baz')


def test_cnot_gate():
    assert CNOT|Ket(0, 0) == Ket(0, 0)
    assert CNOT|Ket(0, 1) == Ket(0, 1)
    assert CNOT|Ket(1, 0) == Ket(1, 1)
    assert CNOT|Ket(1, 1) == Ket(1, 0)

    with pytest.raises(TypeError):
        CNOT|Ket(0)

    with pytest.raises(TypeError):
        CNOT|Ket(1)


def test_reordered_control_inputs():
    """
    Tests controlled gates with the order of their inputs (which input qubits
    are control qubits) reordered from the default)
    """

    # First test that the default does work as expected
    G = ControlledGate(X, n_control=3)
    assert G|Ket(1, 1, 1, 0) == Ket(1, 1, 1, 1)

    G = ControlledGate(X, n_control=3, n_qubits=5)
    assert G|Ket(1, 1, 1, 0, 0) == Ket(1, 1, 1, 0, 1)
    assert G|Ket(1, 1, 1, 1, 0) == Ket(1, 1, 1, 1, 1)

    # Change the position of the target
    G = ControlledGate(X, n_control=3, target=0)
    assert G|Ket(0, 1, 1, 1) == Ket(1, 1, 1, 1)

    G = ControlledGate(X, n_control=3, n_qubits=5, target=0)
    assert G|Ket(0, 1, 1, 1, 0) == Ket(1, 1, 1, 1, 0)
    assert G|Ket(0, 1, 1, 1, 1) == Ket(1, 1, 1, 1, 1)

    G = ControlledGate(X, n_control=3, n_qubits=5, target=1)
    assert G|Ket(1, 0, 1, 1, 0) == Ket(1, 1, 1, 1, 0)
    assert G|Ket(1, 0, 1, 1, 1) == Ket(1, 1, 1, 1, 1)

    # Change the position of a 2-qubit target gate
    G = ControlledGate(X * X, n_control=2, n_qubits=5, target=0)
    assert G|Ket(0, 1, 1, 1, 0) == Ket(1, 0, 1, 1, 0)
    assert G|Ket(1, 0, 1, 1, 1) == Ket(0, 1, 1, 1, 1)

    G = ControlledGate(X * X, n_control=2, n_qubits=5, target=1)
    assert G|Ket(1, 0, 1, 1, 0) == Ket(1, 1, 0, 1, 0)
    assert G|Ket(1, 1, 0, 1, 1) == Ket(1, 0, 1, 1, 1)

    # Move some of the control qubits around too
    G = ControlledGate(X, n_control=2, n_qubits=5, target=2, c0=0, c1=4)
    assert G|Ket(1, 0, 0, 0, 1) == Ket(1, 0, 1, 0, 1)
    assert G|Ket(1, 0, 0, 1, 1) == Ket(1, 0, 1, 1, 1)
    assert G|Ket(1, 1, 0, 0, 1) == Ket(1, 1, 1, 0, 1)
    assert G|Ket(1, 1, 0, 1, 1) == Ket(1, 1, 1, 1, 1)

    G = ControlledGate(X, n_qubits=4, target=2, control=3)
    assert G|Ket(0, 0, 0, 1) == Ket(0, 0, 1, 1)
    assert G|Ket(0, 1, 0, 1) == Ket(0, 1, 1, 1)
    assert G|Ket(1, 0, 0, 1) == Ket(1, 0, 1, 1)
    assert G|Ket(1, 1, 0, 1) == Ket(1, 1, 1, 1)


def test_target_alias():
    """
    Test using 't' as a shorthand for 'target' when reordering inputs to a
    ControlledGate.
    """

    assert CNOT(3, t=1, c=2)|Ket(0, 1, 1) == Ket(0, 0, 1)

    # Using t and target simultaneously should raise an exception; this would
    # be equivalent to providing the target kwarg twice, for example
    with pytest.raises(TypeError):
        CNOT(target=0, t=0, c=1)


def test_gate_equivalence():
    """
    Gates should compare as equivalent if they have the same matrix elements,
    even if they don't have the same names, etc.
    """

    assert I == Gate([[1, 0], [0, 1]])
    assert H == Gate([[np.sqrt(0.5),  np.sqrt(0.5)],
                      [np.sqrt(0.5), -np.sqrt(0.5)]])
    assert CNOT == Gate([[1, 0, 0, 0],
                         [0, 1, 0, 0],
                         [0, 0, 0, 1],
                         [0, 0, 1, 0]])

    # and so on...
