import pytest

from ..operator import Operator, Gate

def test_operator_product():
    o = Operator([[1, 2], [3, 4]], name='O')
    p = Operator([[1, 0], [0, 1]], name='P')

    q = o | p

    # TODO: assert isinstance(q, Operator).  Currently this will fail; see
    # https://bitbucket.org/embray/pyqc/issue/1
    assert (q.qobj.full() == [[1, 2], [3, 4]]).all()

    # TODO: The name might be different if unicode is enabled; test the unicode
    # case as well.
    assert q.name in ['O | P']
    assert q.dimension == 2

    # TODO: Improve plaintext repr of compound operators
    # see https://bitbucket.org/embray/pyqc/issue/2
    # assert repr(q) == 'O | P'
    assert q.format_latex('name') == r'\hat{O}\hat{P}'
    assert (q.format_latex('matrix') ==
            r'\left(\begin{array}{*{11}c}1.0 & 2.0\\3.0 & 4.0\\\end{array}\right)')
    assert (q.format_latex('both') ==
            r'\hat{O}\hat{P} = '
            r'\left(\begin{array}{*{11}c}1.0 & 2.0\\3.0 & 4.0\\\end{array}\right)')


def test_operator_new_from_product():
    o = Operator([[1, 2], [3, 4]], name='O')
    p = Operator([[1, 0], [0, 1]], name='P')

    q = Operator(o | p, name='Q')

    assert isinstance(q, Operator)
    assert (q.qobj.full() == [[1, 2], [3, 4]]).all()
    assert q.name == 'Q'
    assert q.dimension == 2

    # TODO: Improve plaintext repr of compound operators
    # see https://bitbucket.org/embray/pyqc/issue/2
    # assert repr(q) == 'Q'?
    assert q.format_latex('name') == r'\hat{Q}'
    assert (q.format_latex('matrix') ==
            r'\left(\begin{array}{*{11}c}1.0 & 2.0\\3.0 & 4.0\\\end{array}\right)')
    assert (q.format_latex('both') ==
            r'\hat{Q} = \left(\begin{array}{*{11}c}1.0 & 2.0\\3.0 & 4.0\\\end{array}\right)')


def test_operator_tensor_product():
    o = Operator([[1, 2], [3, 4]], name='O')
    p = Operator([[1, 0], [0, 1]], name='P')

    q = o * p

    # TODO: assert isinstance(q, Operator).  Currently this will fail; see
    # https://bitbucket.org/embray/pyqc/issue/1
    assert (q.qobj.full() == [[1, 0, 2, 0],
                              [0, 1, 0, 2],
                              [3, 0, 4, 0],
                              [0, 3, 0, 4]]).all()
    # TODO: Test unicode name
    assert q.name in ['O * P']
    assert q.dimension == 4
    assert q.qobj.shape == (4, 4)
    assert q.qobj.dims == [[2, 2], [2, 2]]

    # TODO: Improve plaintext repr of compound operators
    # see https://bitbucket.org/embray/pyqc/issue/2
    # assert repr(q) == 'O * P'
    assert q.format_latex('name') == r'\hat{O} \otimes \hat{P}'
    assert (q.format_latex('matrix') ==
            r'\left(\begin{array}{*{11}c}'
            r'1.0 & 0.0 & 2.0 & 0.0\\'
            r'0.0 & 1.0 & 0.0 & 2.0\\'
            r'3.0 & 0.0 & 4.0 & 0.0\\'
            r'0.0 & 3.0 & 0.0 & 4.0\\'
            r'\end{array}\right)')
    assert (q.format_latex('both') ==
            r'\hat{O} \otimes \hat{P} = '
            r'\left(\begin{array}{*{11}c}'
            r'1.0 & 0.0 & 2.0 & 0.0\\'
            r'0.0 & 1.0 & 0.0 & 2.0\\'
            r'3.0 & 0.0 & 4.0 & 0.0\\'
            r'0.0 & 3.0 & 0.0 & 4.0\\'
            r'\end{array}\right)')


def test_operator_new_from_tensor_product():
    o = Operator([[1, 2], [3, 4]], name='O')
    p = Operator([[1, 0], [0, 1]], name='P')

    q = Operator(o * p, name='Q')
    assert isinstance(q, Operator)
    assert (q.qobj.full() == [[1, 0, 2, 0],
                              [0, 1, 0, 2],
                              [3, 0, 4, 0],
                              [0, 3, 0, 4]]).all()
    assert q.name == 'Q'
    assert q.dimension == 4
    assert q.qobj.shape == (4, 4)
    assert q.qobj.dims == [[2, 2], [2, 2]]

    # TODO: Improve plaintext repr of compound operators
    # see https://bitbucket.org/embray/pyqc/issue/2
    # assert repr(q) == 'Q' ?
    assert q.format_latex('name') == r'\hat{Q}'
    assert (q.format_latex('matrix') ==
            r'\left(\begin{array}{*{11}c}'
            r'1.0 & 0.0 & 2.0 & 0.0\\'
            r'0.0 & 1.0 & 0.0 & 2.0\\'
            r'3.0 & 0.0 & 4.0 & 0.0\\'
            r'0.0 & 3.0 & 0.0 & 4.0\\'
            r'\end{array}\right)')
    assert (q.format_latex('both') ==
            r'\hat{Q} = '
            r'\left(\begin{array}{*{11}c}'
            r'1.0 & 0.0 & 2.0 & 0.0\\'
            r'0.0 & 1.0 & 0.0 & 2.0\\'
            r'3.0 & 0.0 & 4.0 & 0.0\\'
            r'0.0 & 3.0 & 0.0 & 4.0\\'
            r'\end{array}\right)')


def test_tensor_product_of_products():
    a = Operator([[1, 2], [3, 4]], name='A')
    b = Operator([[1, 0], [0, 1]], name='B')

    o = Operator([[2, 3], [4, 5]], name='O')
    p = Operator([[0, 1], [1, 0]], name='P')

    # TODO: Test that the order of operations is applied correctly even without
    # the parentheses; see
    # https://bitbucket.org/embray/pyqc/issue/4
    q = (a | b) * (o | p)

    assert q.dimension == 4
    assert q.qobj.shape == (4, 4)
    assert q.qobj.dims == [[2, 2], [2, 2]]
    assert (q.qobj.full() == [[ 3,  2,  6,  4],
                              [ 5,  4, 10,  8],
                              [ 9,  6, 12,  8],
                              [15, 12, 20, 16]]).all()

    assert (q.format_latex('expression') ==
            r'\hat{A}\hat{B} \otimes \hat{O}\hat{P}')


def test_product_of_tensor_products():
    a = Operator([[1, 2], [3, 4]], name='A')
    b = Operator([[1, 0], [0, 1]], name='B')

    o = Operator([[2, 3], [4, 5]], name='O')
    p = Operator([[0, 1], [1, 0]], name='P')

    # TODO: Test that the order of operations is applied correctly even without
    # the parentheses; see
    # https://bitbucket.org/embray/pyqc/issue/4
    q = (a * b) | (o * p)

    assert q.dimension == 4
    assert q.qobj.shape == (4, 4)
    assert q.qobj.dims == [[2, 2], [2, 2]]
    assert (q.qobj.full() == [[ 0, 10,  0, 13],
                              [10,  0, 13,  0],
                              [ 0, 22,  0, 29],
                              [22,  0, 29,  0]]).all()
    assert (q.format_latex('expression') ==
            r'(\hat{A} \otimes \hat{B})(\hat{O} \otimes \hat{P})')


def test_instantiate_gate():
    """Test that an `Operator` instantiated with a unitary matrix returns
    a `Gate` and that a non-unitary matrix does not.  Also ensure that
    trying to directly instatiate a `Gate` with a non-unitary fails.
    """

    assert not isinstance(Operator([[1, 2], [3, 4]]), Gate)
    assert isinstance(Operator([[0, -1j], [1j, 0]]), Gate)

    with pytest.raises(ValueError):
        Gate([[1, 2], [3, 4]])
