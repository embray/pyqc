from __future__ import absolute_import

import operator
import os
import shutil
import sys
import subprocess as sp
import tempfile

from collections import deque

import numpy as np
import qutip


EPS = sys.float_info.epsilon


def repr_bin(value, nbits=None):
    """Returns the binary representation of the input value formatted as an
    n-bit string.

    By default the minimum number of bits are used; otherwise it is padded with
    zeros out the correct number of bits.
    """

    if nbits is None:
        return bin(value)[2:]
    else:
        return '{0:0>{nbits}}'.format(bin(value)[2:], nbits=nbits)


def bit_width(n):
    """
    For a given non-negative integer, return the minimum number of bits needed
    for the binary representation of that integer.
    """

    if n == 0:
        return 1

    return int(np.floor(np.log2(n)) + 1)


def round_exactly_one_or_zero(n):
    """
    Given a floating point number that's close to either zero or one +/-
    slight rounding error, round to exactly 1 or exactly 0.
    """

    if n + EPS == 1.0 or n - EPS == 1.0:
        return 1.0
    elif n + EPS == 0.0 or n - EPS == 0.0:
        return 0.0
    else:
        return n


class ExpressionTree(object):
    __slots__ = ['left', 'right', 'value']

    def __init__(self, value, left=None, right=None):
        self.value = value
        self.left = left
        self.right = right

    @property
    def isleaf(self):
        return self.left is None and self.right is None

    def leftmost(self):
        """
        Returns the leftmost operand in the expression (ignoring grouping).
        """

        if self.left is None:
            return self.value
        return self.left.leftmost()

    def rightmost(self):
        """
        Returns the rightmost operand in the expression (ignoring grouping).
        """

        if self.right is None:
            return self.value
        return self.right.rightmost()

    def traverse_preorder(self):
        stack = deque([self])
        while stack:
            node = stack.pop()
            yield node

            if node.right is not None:
                stack.append(node.right)
            if node.left is not None:
                stack.append(node.left)

    def traverse_inorder(self):
        stack = deque()
        node = self
        while stack or node is not None:
            if node is not None:
                stack.append(node)
                node = node.left
            else:
                node = stack.pop()
                yield node
                node = node.right

    def traverse_postorder(self):
        stack = deque()
        node = self
        last = None
        while stack or node is not None:
            if node is not None:
                stack.append(node)
                node = node.left
            else:
                parent = stack[-1]
                if parent.right is not None and last is not parent.right:
                    node = parent.right
                else:
                    stack.pop()
                    yield parent
                    last = parent

    def evaluate(self, operators, getter=None):
        """Evaluate the expression represented by this tree.

        ``Operators`` should be a dictionary mapping operator names
        ('tensor', 'product', etc.) to a function that implements that
        operator for the correct number of operands.

        If given, ``getter`` is a function evaluated on each *leaf* node's
        value before applying the operator between them.  This could be used,
        for example, to operate on an attribute of the node values rather than
        directly on the node values.
        """

        operands = deque()

        for node in self.traverse_postorder():
            if node.isleaf:
                # For a "tree" containing just a single operator at the root
                operands.append(getter(node.value))
            else:
                operator = operators[node.value]
                right = operands.pop()
                left = operands.pop()
                operands.append(operator(left, right))

        return operands.pop()


# I wrote this bit of code for Astropy, but I'm borrowing it here.  Astropy's
# license, which permits this usage, is as follows::
# Copyright (c) 2011-2013, Astropy Developers
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright notice, this
#   list of conditions and the following disclaimer in the documentation and/or
#   other materials provided with the distribution.
# * Neither the name of the Astropy Team nor the names of its contributors may be
#   used to endorse or promote products derived from this software without
#   specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
class lazyproperty(object):
    """
    Works similarly to property(), but computes the value only once.

    This essentially memoizes the value of the property by storing the result
    of its computation in the ``__dict__`` of the object instance.  This is
    useful for computing the value of some property that should otherwise be
    invariant.  For example::

        >>> class LazyTest(object):
        ...     @lazyproperty
        ...     def complicated_property(self):
        ...         print 'Computing the value for complicated_property...'
        ...         return 42
        ...
        >>> lt = LazyTest()
        >>> lt.complicated_property
        Computing the value for complicated_property...
        42
        >>> lt.complicated_property
        42

    If a setter for this property is defined, it will still be possible to
    manually update the value of the property, if that capability is desired.

    Adapted from the recipe at
    http://code.activestate.com/recipes/363602-lazy-property-evaluation
    """

    def __init__(self, fget, fset=None, fdel=None, doc=None):
        self._fget = fget
        self._fset = fset
        self._fdel = fdel
        if doc is None:
            self.__doc__ = fget.__doc__
        else:
            self.__doc__ = doc

    def __get__(self, obj, owner=None):
        if obj is None:
            return self
        key = self._fget.__name__
        if key not in obj.__dict__:
            val = self._fget(obj)
            obj.__dict__[key] = val
            return val
        else:
            return obj.__dict__[key]

    def __set__(self, obj, val):
        obj_dict = obj.__dict__
        key = self._fget.__name__
        if self._fset:
            ret = self._fset(obj, val)
            if ret is not None and obj_dict.get(key) is ret:
                # By returning the value set the setter signals that it took
                # over setting the value in obj.__dict__; this mechanism allows
                # it to override the input value
                return
        obj_dict[key] = val

    def __delete__(self, obj):
        if self._fdel:
            self._fdel(obj)
        key = self._fget.__name__
        if key in obj.__dict__:
            del obj.__dict__[key]

    def getter(self, fget):
        return self.__ter(fget, 0)

    def setter(self, fset):
        return self.__ter(fset, 1)

    def deleter(self, fdel):
        return self.__ter(fdel, 2)

    def __ter(self, f, arg):
        args = [self._fget, self._fset, self._fdel, self.__doc__]
        args[arg] = f
        cls_ns = sys._getframe(1).f_locals
        for k, v in cls_ns.items():
            if v is self:
                property_name = k
                break

        cls_ns[property_name] = lazyproperty(*args)

        return cls_ns[property_name]


# Borrowed these bits from IPython:
# IPython is licensed under the terms of the Modified BSD License (also known as
# New or Revised BSD), as follows:
#
# Copyright (c) 2008-2010, IPython Development Team
# Copyright (c) 2001-2007, Fernando Perez. <fernando.perez@colorado.edu>
# Copyright (c) 2001, Janko Hauser <jhauser@zscout.de>
# Copyright (c) 2001, Nathaniel Gray <n8gray@caltech.edu>
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# Redistributions in binary form must reproduce the above copyright notice, this
# list of conditions and the following disclaimer in the documentation and/or
# other materials provided with the distribution.
#
# Neither the name of the IPython Development Team nor the names of its
# contributors may be used to endorse or promote products derived from this
# software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#         SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
# OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

def latex_to_png_dvipng(s, extra_files=None):
    try:
        find_cmd('latex')
        find_cmd('dvipng')
    except FindCmdError:
        raise
    try:
        workdir = tempfile.mkdtemp()
        tmpfile = os.path.join(workdir, "tmp.tex")
        dvifile = os.path.join(workdir, "tmp.dvi")
        outfile = os.path.join(workdir, "tmp.png")

        if extra_files is not None:
            for filename, data in extra_files:
                with open(os.path.join(workdir, filename), 'wb') as f:
                    f.write(data)

        with open(tmpfile, "w") as f:
            f.writelines(s)

        # TODO: Rather than dump output to devnull; check for error conditions
        # and report errors back to the user
        with open(os.devnull, 'w') as devnull:
            sp.check_call(
                ["latex", "-halt-on-error", tmpfile], cwd=workdir,
                stdout=devnull, stderr=devnull)

            sp.check_call(
                ["dvipng", "-T", "tight", "-D", "120", "-z", "9",
                 "-bg", "transparent", "-o", outfile, dvifile], cwd=workdir,
                stdout=devnull, stderr=devnull)

        with open(outfile, "rb") as f:
            return f.read()
    finally:
        shutil.rmtree(workdir)


class FindCmdError(Exception):
    pass


def find_cmd(cmd):
    """Find absolute path to executable cmd in a cross platform manner.

    This function tries to determine the full path to a command line program
    using `which` on Unix/Linux/OS X and `win32api` on Windows.  Most of the
    time it will use the version that is first on the users `PATH`.

    Warning, don't use this to find IPython command line programs as there
    is a risk you will find the wrong one.  Instead find those using the
    following code and looking for the application itself::

        from IPython.utils.path import get_ipython_module_path
        from IPython.utils.process import pycmd2argv
        argv = pycmd2argv(get_ipython_module_path('IPython.terminal.ipapp'))

    Parameters
    ----------
    cmd : str
        The command line program to look for.
    """
    try:
        path = _find_cmd(cmd).rstrip()
    except OSError:
        raise FindCmdError('command could not be found: %s' % cmd)
    # which returns empty if not found
    if path == '':
        raise FindCmdError('command could not be found: %s' % cmd)
    return os.path.abspath(path)


if sys.platform == 'win32':
    # TODO: Rather than rely on pywin32 just use ctypes to do this
    def _find_cmd(cmd):
        """Find the full path to a .bat or .exe using the win32api module."""
        try:
            from win32api import SearchPath
        except ImportError:
            raise ImportError('you need to have pywin32 installed for this '
                              'to work')
        else:
            PATH = os.environ['PATH']
            extensions = ['.exe', '.com', '.bat', '.py']
            path = None
            for ext in extensions:
                try:
                    path = SearchPath(PATH, cmd, ext)[0]
                except:
                    pass
            if path is None:
                raise OSError("command %r not found" % cmd)
            else:
                return path
else:
    def _find_cmd(cmd):
        """Find the full path to a command using which."""

        path = sp.Popen(['/usr/bin/env', 'which', cmd],
                        stdout=sp.PIPE, stderr=sp.PIPE).communicate()[0]
        return str(path)
