import os
import pkgutil


# Files needed for LaTeX rendering
_LATEX_FILES = ['meter.epsf', 'xyqcirc.tex']


def get_package_data():
    path = os.path.dirname(__file__)
    files = []
    for filename in _LATEX_FILES:
        filepath = os.path.join(path, filename)
        data = pkgutil.get_data(__name__, filename)
        files.append((filename, data))
    return files
