#!/usr/bin/env python
from setuptools import setup, Extension
from Cython.Build import cythonize
import numpy as np

# Most configuration is in setup.cfg but we still need to pass
# ext_modules to setup(); see https://github.com/pypa/setuptools/issues/2220
setup(
    ext_modules=cythonize(
        Extension(
            'pyqc.chp',
            sources=['pyqc/chp.pyx'],
            include_dirs=[np.get_include()],
            define_macros=[('NPY_NO_DEPRECATED_API', 'NPY_1_7_API_VERSION')]
        ),
        compiler_directives={'language_level': '3'}
    )
)
